﻿using AOISystem.Utilities.Flow;
using AOISystem.Utilities.Modules.Syntek.EtherCAT.SlaveModules.Motion;
using MotionSystem.Project.Common;
using System.Collections.Generic;

namespace MotionSystem.Project.Flow
{
    public class AOIHomeFlow : FlowBase
    {
        public AOIHomeFlow()
        {
            //BuildLog += (msg) => { LogHelper.Flow(msg); };

            SetMotorList();
            SetPosList();
        }

        List<CEtherCATMotion> MotorList = new List<CEtherCATMotion>();
        List<double> PosList = new List<double>();

        public override void Flow()
        {
            switch (Step1)
            {
                case "0": //初始化
                    SetPosList();
                    ChangeStep1("3", " ");
                    break;

                case "3":
                    //復歸動作
                    foreach (CEtherCATMotion motor in MotorList)
                    {
                        motor.Home();
                    }

                    this.Timer1.Restart();
                    ChangeStep1("31", "復歸動作");
                    break;

                case "31":
                    if (this.Timer1.ElapsedMilliseconds > 1000)
                    {
                        this.Timer1.Stop();

                        //檢查是否復歸完成
                        int homeDoneCount = 0;
                        foreach (CEtherCATMotion motor in MotorList)
                        {
                            if (!motor.IsHome)
                                break;
                            else
                            {
                                homeDoneCount++;
                                if (MotorList.IndexOf(motor) == MotorList.Count - 1)
                                {
                                    //motor.ResetPos();
                                    ChangeStep1("4", "復歸完成 Home Done" + " Count = " + homeDoneCount);
                                }
                            }
                        }
                    }
                    break;

                case "4":
                    //移動到預備位置
                    for (int i = 0; i < MotorList.Count; i++)
                    {
                        MotorList[i].AbsolueMove(PosList[i]);
                    }

                    this.Timer1.Restart();
                    ChangeStep1("41", "移動到預備位置");
                    break;

                case "41":
                    if (this.Timer1.ElapsedMilliseconds > 5000)
                    {
                        ChangeStep1("4", "重新下達準備位置命令");
                        break;
                    }
                    //檢查是否移動到預備位置
                    foreach (CEtherCATMotion motor in MotorList)
                    {
                        if (!motor.IsReached)
                            break;
                        else if (MotorList.IndexOf(motor) == MotorList.Count - 1)
                        {
                            //motor.ResetPos();
                            ChangeStep1("801", "各軸已移動到預備位置");
                        }
                    }
                    break;

                case "801": //流程成功
                    AOI.WorkData.IsAOIHomeCompleted = true;
                    Stop("AOIHomeFlow Flow Finish");
                    break;
            }
        }

        private void SetMotorList()
        {
            MotorList.Clear();
            MotorList.Add(AOI.Modules.CV1);
        }

        private void SetPosList()
        {
            PosList.Clear();
            PosList.Add(200);
        }
    }
}
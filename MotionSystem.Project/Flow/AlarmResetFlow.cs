﻿using System.Collections.Generic;
using AOISystem.Utilities;
using AOISystem.Utilities.Common;
using AOISystem.Utilities.Flow;
using AOISystem.Utilities.Logging;
using AOISystem.Utilities.Modules;
using MotionSystem.Project.Common;

namespace MotionSystem.Project.Flow
{
    public class AlarmResetFlow : FlowBase
    {
        private int _motorResetCount = 0;

        public AlarmResetFlow()
        {
            BuildLog += (msg) => LogHelper.Flow(msg);
        }

        public override void Flow()
        {
            switch (this.Step1)
            {
                case "0":
                    {
                        //選擇AlarmReset流程
                        if (!AOI.Modules.SystemStatusCheck())
                        {
                            _motorResetCount = 3;
                            ChangeStep1("10");
                        }
                        else if (AOI.WorkData.IsAllHomeCompleted)
                        {
                            ChangeStep1("20");
                        }
                        else
                        {
                            ChangeStep1("30");
                        }
                        break;
                    }
                case "10":
                    {
                        //Motor Reset 流程
                        AOI.Modules.AllMotorResetAlarm();
                        _motorResetCount--;
                        this.Timer1.Restart();
                        ChangeStep1("11");
                        break;
                    }
                case "11":
                    {
                        if (this.Timer1.ElapsedMilliseconds > 1000)
                        {
                            List<ErrorCode> statusResults;
                            if (!AOI.Modules.SystemStatusCheck(out statusResults))
                            {
                                if (_motorResetCount > 0)
                                {
                                    ChangeStep1("10");
                                }
                                else
                                {
                                    this.Timer1.Stop();
                                    Stop();
                                    //160726.1400 修改AlarmResetFlow流程為Alarm暫停動作
                                    AOI.SystemNotify(OperationStatus.Alarm, new SystemNotifyCommand(StopMode.Pause, "Alarm reset error.", statusResults));
                                }
                            }
                            else
                            {
                                ChangeStep1("0");
                            }
                        }
                        break;
                    }
                case "20":
                    {
                        //馬達位置初始化
                        ChangeStep1("21");
                        break;
                    }
                case "21":
                    {
                        //到位判斷
                        {
                            ChangeStep1("30");
                        }
                        break;
                    }
                case "30":
                    {
                        //加判斷
                        AOI.IOs.SetSignalTowerAndRingtone(SignalTowerStatus.None, RingtoneStatus.None);
                        AOI.WorkData.MachineStatus = MachineStatus.Idle;

                        ModulesFactory.FlowControlHelper.GetFlowControl("MachineOperationFlowControl").RestartAll();
                        MonitorLogger.Clear();
                        Stop();
                        break;
                    }
            }
        }
    }
}

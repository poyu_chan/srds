﻿using AOISystem.Utilities;
using AOISystem.Utilities.Flow;
using AOISystem.Utilities.Logging;
using AOISystem.Utilities.Modules.Syntek;
using MotionSystem.Project.Common;
using System;
using System.Collections.Generic;

namespace MotionSystem.Project.Flow
{
    public class MainFlow : FlowBase
    {
        private string _isRunningSubFlowItems = null;

        public MainFlow()
        {
            WaitPaused += (msg) =>
            {
            };
            Failing += (msg) =>
            {
                LogHelper.Debug(this.Name + " Failing : " + "Desc = " + this.GetFlowInfo());
            };
            BuildLog += (msg) => LogHelper.Flow(msg);
        }

        public override void Flow()
        {
            if (this.IsPauseRequested)
            {
                if (this.Step1 == "9999" /* && this.Step2 == "9999" && this.Step3 == "9999"*/)
                {
                    Stop("暫停流程");
                    return;
                }
            }

            try
            {
                //AOI動作流程
                if (!AOI.SystemStatus.ByPassAOIFlow &&
                    !AOI.FlowControlHelper.GetFlowControl("OtherFlow").IsRunning("ClearOffFlow"))
                {
                    AOI_ActionFlow();
                    //Sensor狀態監控與在籍轉換
                }
                else
                {
                    //AOI空跑動作流程
                    AOIFreeRun_ActionFlow();
                }
            }
            catch (Exception ex)
            {
                LogHelper.Exception(ex);
                //160726.1400 修改AOIFlow流程內Alarm為暫停動作
                AOI.SystemNotify(OperationStatus.Alarm, new SystemNotifyCommand(StopMode.Pause, string.Format("AOI Flow error raised. Message : {0}\r\nStackTrace : {1}", ex.Message, ex.StackTrace)));
            }
        }

        //AOI動作流程
        private void AOI_ActionFlow()
        {
            switch (Step1)
            {
                case "0":
                    {
                        //初始化流程
                        ChangeStep1("1", "確認Sensor狀態");
                        break;
                    }
                case "1":
                    {
                        //確認Sensor狀態
                        List<string> isOnItems = new List<string>();
                        //if (AOI.IOs.DIWaferInputInPositionCheck)
                        //{
                        //    isOnItems.Add("Loader");
                        //}
                        if (isOnItems.Count > 0)
                        {
                            string itemNames = string.Join(", ", isOnItems);
                            AOI.SystemNotify(OperationStatus.Alarm, new SystemNotifyCommand(StopMode.Pause, string.Format("偵測到站點Sensor({0})上存在Wafer, 請移除後重新啟動", itemNames)));
                            ChangeStep1("9999", "停止流程");
                        }
                        else
                        {
                            ChangeStep1("10", "子流程初始化");
                        }
                        break;
                    }
                case "10":
                    {
                        //子流程初始化
                        //子流程等待機制
                        ChangeStep1("20", "馬達運轉");
                        break;
                    }
                case "20":
                    {
                        AOI.Modules.CV1.ContinuousMove(RotationDirection.CW, 200);
                        ChangeStep1("30", "Sensor狀態監控與在籍轉換流程啟動");
                        break;
                    }
                case "30":
                    {
                        //Sensor狀態監控與在籍轉換流程啟動
                        ChangeStep1("40", "等待流程狀態");
                        break;
                    }
                case "40":
                    {
                        //等待流程狀態
                        //等待流程是否暫停
                        if (this.IsPauseRequested)
                        {
                            this.Timer1.Restart();
                            ChangeStep1("50", "等待AOI子流程結束");
                        }
                        else
                        {
                            //流程內容
                        }
                        break;
                    }
                case "50":
                    {
                        //等待AOI子流程結束
                        ChangeStep1("9999", "停止流程");
                        break;
                    }
                case "9999":
                    {
                        //停止流程
                        break;
                    }
            }
        }

        //AOI空跑動作流程, 不會丟通訊給子機
        private void AOIFreeRun_ActionFlow()
        {
            switch (Step1)
            {
                case "0":
                    {
                        //初始化流程
                        ChangeStep1("20");
                        break;
                    }
                case "20":
                    {
                        AOI.Modules.CV1.ContinuousMove(RotationDirection.CW);
                        ChangeStep1("25", "暫停等待判斷");
                        break;
                    }
                case "25":
                    {
                        //暫停等待判斷
                        if (this.IsPauseRequested)
                        {
                            ChangeStep1("9999", "流程停止");
                            break;
                        }
                        break;
                    }
                case "9999":
                    {
                        AOI.Modules.CV1.Stop(StopType.SlowDown);
                        //停止流程
                        break;
                    }
            }
        }
    }
}
﻿using System.Collections.Generic;
using AOISystem.Utilities;
using AOISystem.Utilities.Common;
using AOISystem.Utilities.Flow;
using AOISystem.Utilities.Logging;
using MotionSystem.Project.Common;

namespace MotionSystem.Project.Flow
{
    public class ClearOffFlow : FlowBase
    {
        private string _tempSensorStatus;
        private bool _record = false;

        public ClearOffFlow()
        {
            Started += (msg) =>
            {
                _tempSensorStatus = string.Empty;
                AOI.FlowControlHelper.GetFlowControl("AutoFlow").Restart("MainFlow");
                this.Timer3.Restart();
            };
            Stopped += (msg) =>
            {
                AOI.FlowControlHelper.GetFlowControl("AutoFlow").Stop("MainFlow");
                AOI.Modules.AllMotorStop(StopType.Emergency);
            };
            Failing += (msg) =>
            {
                AOI.FlowControlHelper.GetFlowControl("AutoFlow").Stop("MainFlow");
                AOI.Modules.AllMotorStop(StopType.Emergency);
            };
            BuildLog += (msg) => LogHelper.Flow(msg);
        }

        public override void Flow()
        {
            if (this.Timer3.ElapsedMilliseconds > AOI.RecipeData.ClearOffFlowContiunanceTime * 1000)
            {
                Stop();
                AOI.SystemNotify(OperationStatus.Alarm, new SystemNotifyCommand(StopMode.Pause, new ErrorCode(7002, "Clear Off Flow Timeout")));
                return;
            }
            switch (this.Step1)
            {
                case "0":
                    {
                        this.Timer1.Restart();
                        ChangeStep1("10", "Sensor觸發判斷");
                        break;
                    }
                case "10":
                    {
                        List<ErrorCode> sensorStatusList = null;
                        bool isON = !CheckStatus.DoCheck(out sensorStatusList, AOI.SystemStatus.CheckSensorOn);
                        string sensorStatus = string.Join("\r\n", sensorStatusList);

                        if (sensorStatus != _tempSensorStatus)
                        {
                            _tempSensorStatus = sensorStatus;
                            this.Timer1.Restart();
                        }
                        else
                        {
                            if (this.Timer1.ElapsedMilliseconds > 5000)
                            {
                                Stop();
                                if (isON)
                                {
                                    AOI.SystemNotify(OperationStatus.Alarm, new SystemNotifyCommand(StopMode.Pause, new ErrorCode(7000)));
                                }
                                else
                                {
                                    AOI.SystemNotify(OperationStatus.Warning, new SystemNotifyCommand("Clear Off 完成", 1, RingtoneStatus.Ringtone4));
                                }
                            }
                        }
                        break;
                    }
            }
        }
    }
}
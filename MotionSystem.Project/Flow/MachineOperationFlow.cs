﻿using AOISystem.Utilities;
using AOISystem.Utilities.Account;
using AOISystem.Utilities.Flow;
using AOISystem.Utilities.Logging;
using MotionSystem.Project.Common;

namespace MotionSystem.Project.Flow
{
    public class MachineOperationFlow : FlowBase
    {
        public MachineOperationFlow()
        {
            BuildLog += (msg) => LogHelper.Flow(msg);
        }

        public override void Flow()
        {
            /*
            if (!AOI.WorkData.IsModulesInitialized)
            {
                return;
            }
            if (AOI.WorkData.IsSystemCommanding)
            {
                return;
            }
            if (AOI.IOs.DIStartButton && !this.Timer1.IsRunning && AOI.WorkData.MachineStatus == MachineStatus.Idle)
            {
                this.Timer1.Restart();
                AOI.WorkData.IsSystemCommanding = true;
                AOI.SystemNotify(OperationStatus.Start);
                AOI.WorkData.IsSystemCommanding = false;
                LogHelper.Operation("{0} Press Start Button", AccountInfoManager.ActiveAccountName);
            }
            if (AOI.IOs.DIStopButton && !this.Timer2.IsRunning)
            {
                this.Timer2.Restart();
                AOI.WorkData.IsSystemCommanding = true;
                AOI.SystemNotify(OperationStatus.Pause);
                AOI.WorkData.IsSystemCommanding = false;
                LogHelper.Operation("{0} Press Pause Button", AccountInfoManager.ActiveAccountName);
            }
            if (AOI.IOs.DIAlarmReset && !this.Timer3.IsRunning && 
                (AOI.WorkData.MachineStatus == MachineStatus.Down))
            {
                this.Timer3.Restart();
                AOI.WorkData.IsSystemCommanding = true;
                AOI.SystemNotify(OperationStatus.AlarmReset);
                AOI.WorkData.IsSystemCommanding = false;
                LogHelper.Operation("{0} Press Alarm Reset Button", AccountInfoManager.ActiveAccountName);
            }

            if (this.Timer1.ElapsedMilliseconds > 5000)
            {
                this.Timer1.Stop();
            }
            if (this.Timer2.ElapsedMilliseconds > 5000)
            {
                this.Timer2.Stop();
            }
            if (this.Timer3.ElapsedMilliseconds > 5000)
            {
                this.Timer3.Stop();
            }

            */

            switch (Step1)
            {
                case "0":

                    //ChangeStep1("1", "");
                    break;

                case "901": //流程失敗
                    Fail("MachineOperationFlow Failure\n" + Description);
                    break;
            }
        }
    }
}
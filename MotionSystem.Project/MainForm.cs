﻿#region - using -

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using AOISystem.Utilities;
using AOISystem.Utilities.Account;
using AOISystem.Utilities.Common;
using AOISystem.Utilities.Component;
using AOISystem.Utilities.Flow;
using AOISystem.Utilities.Forms;
using AOISystem.Utilities.IO;
using AOISystem.Utilities.Logging;
using AOISystem.Utilities.Modules;
using AOISystem.Utilities.Modules.Syntek.EtherCAT.SlaveModules.Motion;
using AOISystem.Utilities.MultiLanguage;
using AOISystem.Utilities.Recipe;
using AOISystem.Utilities.Resources;
using AOISystem.Utilities.Threading;
using MotionSystem.Project.Common;
using MotionSystem.Project.Flow;
using MotionSystem.Project.Forms;

#endregion - using -

namespace MotionSystem.Project
{
    public partial class MainForm : Form
    {
        #region - Private Properties -

        private DateTime _todayDateTime = DateTime.Today;

        private bool _isFormClosing = false;

        private int _minuteCheckIndex = 99;

        private object _pauseKey = new object();

        private int _logOutTimes = 0;
        private Point _lastCursorPos = new Point();

        #endregion - Private Properties -

        #region - Public Constructor -

        public MainForm()
        {
            InitializeComponent();

            //避免設計模式底下偷跑
            if (ProcessInfo.IsDesignMode())
            {
                return;
            }

            ModulesFactory.Components = this.components;

            if (!AOI.Initialize())
            {
                MessageBox.Show("系統初始化失敗");
                return;
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            //避免設計模式底下偷跑
            if (ProcessInfo.IsDesignMode())
            {
                return;
            }

            //this.Visible = true;
            this.Activate();

            if (InitializeSystemParameter())
            {
                LogHelper.Operation("==================================");
                LogHelper.Operation("程式開啟");

                ExceptionHelper.PrintScreenEvent += new Action(ExceptionHelper_PrintScreenEvent);

                AOI.SystemNotifyEvent += new AOI.SystemNotifyEventHandler(AOI_SystemNotifyEvent);

                RecipeInfoManager.GetInstance().RecipeInfoCollectionChanged += new RecipeInfoManager.RecipeInfoCollectionChangedEventHandler(MainForm_RecipeInfoCollectionChangedEvent);
                RecipeInfoManager.GetInstance().RecipeInfoSelectedIndexChanged += new RecipeInfoManager.RecipeInfoSelectedIndexChangedEventHandler(MainForm_RecipeInfoSelectedIndexChangedEvent);
                RecipeInfoManager.GetInstance().RecipeInfoCopyChanged += new RecipeInfoManager.RecipeInfoCopyChangedEventHandler(MainForm_RecipeInfoCopyChangedEvent);
                RecipeInfoManager.GetInstance().ErrorRaised += new RecipeInfoManager.ErrorRaisedEventHandler(MainForm_ErrorRaised);
                this.recipeEditorControl.SelectedRecipeInfoEvent += new RecipeEditorControl.SelectedRecipeInfoEventHandler(recipeEditorControl_SelectedRecipeInfoEvent);

                //AccountInfoManager.IsTestMode = true;
                AccountInfoManager.AccountInfoLogInOutCallback += new Action<string, AccountLevel>(AccountInfoManager_AccountInfoLogInOutCallback);
                AccountInfoManager.LogOut();

                InitializeControlLayout();

                //RecipeInfoManager.GetInstance().InitializeConfiguration(1);
                if (RecipeInfoManager.GetInstance().InitializeConfiguration(AOI.SystemData.DefaultRecipeID))
                {
                    GeneratorIOSwitchControls();
                }
                else
                {
                    MessageBox.Show("請確認Recipe建置是否正確在重啟程式");
                }

                InitializeFlowControl();

                AOI.IOs.RegisterSystemLedStatus(this.ledRun, this.ledWarning, this.ledDown);

                AOI.WorkData.MachineStatus = MachineStatus.Idle;

                this.tmrSystem.Start();

                ConnectCommunication();

                LogHelper.Operation("程式載入結束");
            }
            else
            {
                this.Close();
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                //修改關閉程式提醒中文訊息
                if (MessageBox.Show(ResourceHelper.Language.GetString("QuitApplicationMsg"),
                    "", MessageBoxButtons.YesNo) == DialogResult.No)
                {
                    e.Cancel = true;
                    return;
                }

                LogHelper.Operation("按下關閉程式按鈕");
                _isFormClosing = true;
                this.tmrSystem.Stop();

                if (AOI.IsInitialized)
                {
                    AOI.FlowControlHelper.StopAll("Appliction Closing.");
                    AOI.Modules.AllMotorStop(StopType.Emergency);
                    DisconnectCommunication();

                    if (AOI.WorkData.IsModulesInitialized)
                    {
                        //AOI.IOs.DOStartLamp = false;
                        //AOI.IOs.DOStopLamp = false;
                        //AOI.IOs.DOAlarmReset = false;
                    }

                    AOI.FlowControlHelper.Dispose();
                    ModulesFactory.FlowControlHelper.Dispose();

                    LogHelper.Operation("程式關閉完成");
                    LogHelper.Operation("==================================");
                }
            }
            catch (Exception ex)
            {
                NotifyLogger.Post("Caught Exception：'{0}'", ex.Message);
                LogHelper.Operation("程式關閉失敗");
                LogHelper.Operation("==================================");
                LogHelper.Exception(ex);
            }
        }

        private void ExceptionHelper_PrintScreenEvent()
        {
            Task.Factory.StartNew(() =>
            {
                //等待訊息視窗彈出
                Thread.Sleep(1000);

                PrintScreen();
            });
        }

        #endregion - Public Constructor -

        #region - InitializeSystem Parameters -

        private bool InitializeSystemParameter()
        {
            bool sucess = true;
            try
            {
                AOI.SystemData.Load();

                Assembly assembly = Assembly.GetExecutingAssembly();
                AssemblyName assemblyName = assembly.GetName();

                //設定Logger
                Log.SetLogFile(AOI.SystemData.DataSavePath, assemblyName.Name, "log", AOI.SystemData.LogSaveDays);
                //Log.RegisterPresenter(this.hLogger);
                NotifyLogger.InitializeConfiguration(SynchronizationContext.Current);
                MonitorLogger.InitializeConfiguration(SynchronizationContext.Current);
                //FlowLogger
                LogHelper.Initialize(AOI.SystemData.DataSavePath, AOI.SystemData.LogSaveDays);

                this.lblVersion.Text = string.Format("Version : {0}", assemblyName.Version);
                this.txtVersion.Text = assemblyName.Version.ToString();
                LogHelper.Debug(AOI.GetVersionInfo());
                DailyWorkProcess();

#if TEST    
                AccountInfoManager.IsTestMode = true;
#endif
            }
            catch
            {
                sucess = false;
                MessageBox.Show("InitializeConfiguration System Parameter Error.");
            }
            return sucess;
        }

        private void DailyWorkProcess()
        {
            try
            {
                string[] deletedDirectories = PathHelper.DeleteOverdueDirectory(AOI.SystemData.DataSavePath,
                    "yyyyMMdd", AOI.SystemData.DataSaveDays, new string[] { "LOG" });
                foreach (string directory in deletedDirectories)
                {
                    LogHelper.Debug("Path : {0} had deleted.", directory);
                }
            }
            catch (Exception ex)
            {
                NotifyLogger.Post("Caught Exception：'{0}'", ex.Message);
                LogHelper.Exception(ex);
            }
        }

        private void InitializeControlLayout()
        {
            // 將 TabControl 的索引標籤隱藏
            this.tabControl.Appearance = TabAppearance.Buttons; //TabAppearance.Buttons;
            this.tabControl.SizeMode = TabSizeMode.Fixed;
            this.tabControl.ItemSize = new Size(0, 1);

            //this.MaximizeBox = false;
            //this.MinimizeBox = false;
            //this.ControlBox = false;
            this.WindowState = FormWindowState.Maximized;

            ChangeControlBackColorToDarkColor();

            //更新系統語言資訊
            Language language = LanguageManager.GetSystemLanguage();
            this.tsmiTraditionalChinese.Checked = language == Language.TraditionalChinese;
            this.tsmiEnglish.Checked = language == Language.English;
        }

        private void InitializeFlowControl()
        {
            //監控機台操作紐流程, 不納入FlowControlHelper共同管理
            FlowControl machineOperationFlowControl = ModulesFactory.FlowControlHelper.GetFlowControl("MachineOperationFlowControl");
            machineOperationFlowControl.AddFlowBase(new MachineOperationFlow());
            machineOperationFlowControl.StartAll();

            FlowControl homeFlowControl = AOI.FlowControlHelper.GetFlowControl("Home");
            homeFlowControl.AddFlowBase(new AOIHomeFlow());

            FlowControl autoFlowControl = AOI.FlowControlHelper.GetFlowControl("AutoFlow");
            autoFlowControl.AddFlowBase(new MainFlow() { IsWaitingPauseFlow = true, WaitPauseTimeout = 25000, IsInitializeAfterFailRaised = true });

            FlowControl otherFlowControl = AOI.FlowControlHelper.GetFlowControl("OtherFlow");
            otherFlowControl.AddFlowBase(new AlarmResetFlow());
            otherFlowControl.AddFlowBase(new ClearOffFlow() { IsInitializeAfterFailRaised = true });
        }

        #endregion - InitializeSystem Parameters -

        #region - Private Methods-

        //紀錄ErrorCode訊息
        private string NotifyErrorCode(ErrorType errorType, ErrorCode errorCode)
        {
            DateTime dateTime = DateTime.Now;
            ErrorCode errorCode2 = AOI.ErrorCodeHelper.GetErrorCode(errorType, errorCode.Code);

            if (errorCode2.Code == ErrorCode.UnknowCode)
            {
                errorCode2 = errorCode;
                LogHelper.Exception("找不到ErrorCode : {0}", errorCode);
            }
            else
            {
                if (errorCode.Message != ErrorCode.UnknowMessage)
                {
                    errorCode2.Message = errorCode.Message;
                }
            }

            string folderPath = string.Format(@"{0}\{1}", AOI.SystemData.DataSavePath, dateTime.ToString("yyyyMMdd"));
            string fileName = "ErrorCodeHistory.csv";
            string filePath = string.Format(@"{0}\{1}", folderPath, fileName);
            if (!File.Exists(filePath))
            {
                SimpleCsvHelper.AddData(folderPath, fileName, "{0},{1},{2},{3},{4},{5}", "Time", "Type", "Code", "Message", "Description", "Remark");
            }
            //v1.0.6.15 修改格式 //v1.0.6.14 修改時間格式
            SimpleCsvHelper.AddData(folderPath, fileName, "{0},{1},{2},{3},{4},{5}", dateTime.ToString("yyyy/MM/dd HH:mm:ss"), errorCode2.ErrorType, errorCode2.Code.ToString(), errorCode2.Message, errorCode2.Description, errorCode2.Remark);

            this.tabMessage.SelectedTab = this.tpInfoformation;
            ListViewItem listViewItem = new ListViewItem();
            listViewItem.Text = dateTime.ToString("yyyy.MM.dd HH:mm:ss");
            listViewItem.SubItems.Add(errorCode2.ErrorType.ToString());
            listViewItem.SubItems.Add(errorCode2.Code.ToString());
            listViewItem.SubItems.Add(errorCode2.Message);
            listViewItem.SubItems.Add(errorCode2.Description);
            listViewItem.SubItems.Add(errorCode2.Remark);
            this.lsvErrorMessage.Items.Insert(0, listViewItem);

            if (this.lsvErrorMessage.Items.Count > 10)
            {
                this.lsvErrorMessage.Items.RemoveAt(this.lsvErrorMessage.Items.Count - 1);
            }

            return errorCode2.ToString();
        }

        private void StartLot()
        {
            AOI.RecipeData.Load();
        }

        private void CheckPCStatus()
        {
            double cupRate, ramRate, diskRate;
            CheckCPULoading(out cupRate);
            CheckRAMLoading(out ramRate);
            CheckDISKLoading(out diskRate);
            LogHelper.Debug("CPU {0:F2}% | RAM {1:F2}% | DISK {2:F2}%", cupRate, ramRate, diskRate);

            if (AOI.SystemData.AvailableRAMMinFreeSpace != 0)
            {
                double availableFreeSpace = PerformanceInfo.GetTotalMemoryInMiB() / 1024;
                if (availableFreeSpace < AOI.SystemData.AvailableRAMMinFreeSpace)
                {
                    AOI.SystemNotify(OperationStatus.Alarm, new SystemNotifyCommand(StopMode.Pause, "RAMFreeSpaceFull"));
                }
            }
            if (AOI.SystemData.AvailableDISKMinFreeSpace != 0)
            {
                double availableFreeSpace = DriveHelper.GetAvailableFreeSpace(AOI.SystemData.DataSavePath);
                if (availableFreeSpace < AOI.SystemData.AvailableDISKMinFreeSpace)
                {
                    AOI.SystemNotify(OperationStatus.Alarm, new SystemNotifyCommand(StopMode.Pause, "DISKFreeSpaceFull"));
                }
            }
        }

        private void CheckCPULoading(out double cupRate)
        {
            cupRate = ComputerUtility.GetInstance().GetUsedCupPercentage();
            this.prgCPU.Value = (int)cupRate;
        }

        private void CheckRAMLoading(out double ramRate)
        {
            ramRate = PerformanceInfo.GetPhysicalOccupiedMemoryInPercent();
            this.prgRAM.Value = (int)ramRate;
        }

        private void CheckDISKLoading(out double diskRate)
        {
            diskRate = DriveHelper.GetUsedSpaceRatio(AOI.SystemData.DataSavePath);
            this.prgDISK.Value = (int)diskRate;
        }

        public void PrintScreen()
        {
            try
            {
                int width = Screen.PrimaryScreen.Bounds.Width;
                int height = Screen.PrimaryScreen.Bounds.Height;
                Bitmap screenshot = new Bitmap(width, height, PixelFormat.Format24bppRgb);
                Graphics graph = Graphics.FromImage(screenshot);
                graph.CopyFromScreen(0, 0, 0, 0, new Size(width, height), CopyPixelOperation.SourceCopy);

                DateTime dateTime = DateTime.Now;
                string foldPath = string.Format(@"{0}\{1}\PrintScreen", SystemDefine.SYSTEM_DATA_FOLDER_PATH, dateTime.ToString("yyyyMMdd"));
                if (AOI.SystemData != null)
                {
                    foldPath = string.Format(@"{0}\{1}\PrintScreen", AOI.SystemData.DataSavePath, dateTime.ToString("yyyyMMdd"));
                }
                if (!Directory.Exists(foldPath))
                {
                    Directory.CreateDirectory(foldPath);
                }
                screenshot.Save(string.Format(@"{0}\{1}.png", foldPath, dateTime.ToString("yyyyMMdd_HHmmss")), ImageFormat.Png);

                screenshot.Dispose();
                graph.Dispose();
            }
            catch (Exception ex)
            {
                LogHelper.Exception(ex);
            }
        }
        #endregion - Private Methods-

        #region - Event Callback Methods -

        private void tmrSystem_Tick(object sender, EventArgs e)
        {
            this.lblNowTime.Text = string.Format("NowTime : {0}", DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss"));
            this.txtNowTime.Text = DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss");

            //換天時執行
            if (DateTime.Today > _todayDateTime)
            {
                _todayDateTime = DateTime.Today;
                DailyWorkProcess();
            }

            if (_minuteCheckIndex != (DateTime.Now - _todayDateTime).Minutes)
            {
                _minuteCheckIndex = (DateTime.Now - _todayDateTime).Minutes;
                CheckPCStatus();
            }

            if (AOI.WorkData.IsModulesInitialized)
            {
                AOI.IOs.RefreshMachineLampStatus();

                BindingIOListState();
            }
        }

        private void tmrLogOut_Tick(object sender, EventArgs e)
        {
            //v1.0.170206.1400 加入滑鼠移動重置自動登出時間
            Point currentCursorPos = WinAPI.GetCursorPos();
            if (currentCursorPos != _lastCursorPos)
            {
                _lastCursorPos = currentCursorPos;
                _logOutTimes = 0;
            }
            else
            {
                _logOutTimes++;
                if (_logOutTimes >= 600)
                {
                    tmrLogOut.Enabled = false;
                    _logOutTimes = 0;
                    AccountInfoManager.LogOut(true);
                }
            }
        }

        #endregion - Event Callback Methods -

        #region - Event Operation Methods -

        private void btnMain_Click(object sender, EventArgs e)
        {
            this.tabControl.SelectedTab = this.tpMain;
            SwitchToolBarControlBackColor(this.btnMain);
        }

        private void btnIO_Click(object sender, EventArgs e)
        {
            if (AccountInfoManager.TestPermission(AccountLevel.Engineer))
            {
                this.tabControl.SelectedTab = this.tpIO;
                SwitchToolBarControlBackColor(this.btnIO);
            }
        }

        private void btnMotion_Click(object sender, EventArgs e)
        {
            if (AccountInfoManager.TestPermission(AccountLevel.Engineer))
            {
                this.tabControl.SelectedTab = this.tpMotion;
                SwitchToolBarControlBackColor(this.btnMotion);
                SystemRecipeData(tabMotion, DataTypeEnum.System, ActionTyepEnum.Load, GetSystemRecipeData(DataTypeEnum.System));
            }
        }

        private void btnDatabase_Click(object sender, EventArgs e)
        {
            this.tabControl.SelectedTab = this.tpDatabase;
            SwitchToolBarControlBackColor(this.btnDatabase);
        }

        private void btnRecipe_Click(object sender, EventArgs e)
        {
            if (AccountInfoManager.TestPermission(AccountLevel.Engineer))
            {
                this.tabControl.SelectedTab = this.tpRecipe;
                SwitchToolBarControlBackColor(this.btnRecipe);
                SystemRecipeData(tabRecipe, DataTypeEnum.Recipe, ActionTyepEnum.Load, GetSystemRecipeData(DataTypeEnum.Recipe));
            }
        }

        private void btnSystem_Click(object sender, EventArgs e)
        {
            if (AccountInfoManager.TestPermission(AccountLevel.Developer))
            {
                this.tabControl.SelectedTab = this.tpSystem;
                SwitchToolBarControlBackColor(this.btnSystem);
                SystemRecipeData(flpSystem, DataTypeEnum.System, ActionTyepEnum.Load, GetSystemRecipeData(DataTypeEnum.System));
            }
        }

        private void btnTest_Click(object sender, EventArgs e)
        {
            if (AccountInfoManager.TestPermission(AccountLevel.Developer))
            {
                this.tabControl.SelectedTab = this.tpTest;
                SwitchToolBarControlBackColor(this.btnTest);                                  //v1.0.5.0 自定義Bin槽流程
            }
        }

        private void SwitchToolBarControlBackColor(Button button)
        {
            foreach (Control item in flpToolBarSub1.Controls)
            {
                if (item == button)
                {
                    item.BackColor = Color.Yellow;
                }
                else
                {
                    item.BackColor = SystemColors.Control;
                }
            }
        }

        private void tsmiSystemConfiguration_Click(object sender, EventArgs e)
        {
            ParameterXMLForm<SystemData> propertyGridForm = new ParameterXMLForm<SystemData>(AOI.SystemData, "System Configuration");
            propertyGridForm.ShowDialog();
        }

        private void tsmiRecipeConfiguration_Click(object sender, EventArgs e)
        {
            ParameterXMLForm<RecipeData> propertyGridForm = new ParameterXMLForm<RecipeData>(AOI.RecipeData, "Recipe Configuration");
            propertyGridForm.ShowDialog();
        }

        private void tsmiRecipe_Click(object sender, EventArgs e)
        {
            if (AccountInfoManager.TestPermission(AccountLevel.Engineer))
            {
                RecipeEditorForm recipeEditorForm = new RecipeEditorForm();
                if (recipeEditorForm.ShowDialog() == DialogResult.OK)
                {
                }
            }
        }

        private void tsmiTraditionalChinese_Click(object sender, EventArgs e)
        {
            LanguageManager.SetSystemLanguage(Language.TraditionalChinese);
            MessageBox.Show("設定成功請重啟程式");
        }

        private void tsmiEnglish_Click(object sender, EventArgs e)
        {
            LanguageManager.SetSystemLanguage(Language.English);
            MessageBox.Show("設定成功請重啟程式");
        }

        private void tsmiAbout_Click(object sender, EventArgs e)
        {
            AboutForm aboutForm = new AboutForm();
            aboutForm.Show();
        }

        private void btnByPassConfiguration_Click(object sender, EventArgs e)
        {
            ParameterINIForm propertyGridForm = new ParameterINIForm(AOI.SystemStatus, "By Pass Setting");
            propertyGridForm.Show();
        }

        private void btnMutiLanguage_Click(object sender, EventArgs e)
        {
            AOI.SystemNotify(OperationStatus.Alarm, new SystemNotifyCommand(StopMode.Pause, new ErrorCode(1400, "馬達異常")));
        }

        #endregion - Event Operation Methods -

        #region - Account LogInOut Methods -

        private void AccountInfoManager_AccountInfoLogInOutCallback(string name, AccountLevel level)
        {
            bool AutoLogOut = true;
            this.lblAccountName.Text = string.Format("Account ID : {0}", name == string.Empty ? "No User" : name);
            this.lblAccountLevel.Text = string.Format("Account Level : {0}", level);
            this.txtUserID.Text = string.Format("{0}\t[{1}]", name == string.Empty ? "No User" : name, level);

            if (level == AccountLevel.Developer)
            {
                ControlsVisible(true, true, true, true);
            }
            else if (level == AccountLevel.Administrator)
            {
                ControlsVisible(false, true, true, true);
            }
            else if (level == AccountLevel.Engineer)
            {
                ControlsVisible(false, false, true, true);
            }
            else
            {
                AutoLogOut = false;
                ControlsVisible(false, false, false, true);
            }

            //160802.0920 新增自動登出功能
            if (AutoLogOut)
                tmrLogOut.Enabled = true;
        }

        private void tspmiLogIn_Click(object sender, EventArgs e)
        {
            AccountInfoManager.LogIn();
        }

        private void tspmiLogOut_Click(object sender, EventArgs e)
        {
            AccountInfoManager.LogOut(true);
        }

        private void tspmiAccountEditor_Click(object sender, EventArgs e)
        {
            AccountInfoManager.AccountEditor();
        }

        private void ControlsVisible(bool developerVisible, bool administratorVisible, bool engineerVisible, bool operatorVisible)
        {
            //developerVisible
            this.grpDeveloperTools.Visible = developerVisible;
            this.btnTest.Visible = developerVisible;
            //engineerVisible
            this.tabManualFlowControl.Visible = engineerVisible;

            if (developerVisible)
            {
                if (!this.tabManualFlowControl.Controls.Contains(tpTools))
                {
                    this.tabManualFlowControl.Controls.Add(tpTools);
                }
            }
            else
            {
                if (this.tabManualFlowControl.Controls.Contains(tpTools))
                {
                    this.tabManualFlowControl.Controls.Remove(tpTools);
                }
            }

            //160802.0920 新增自動登出功能
            if (!developerVisible && !administratorVisible & !engineerVisible)
            {
                this.tabControl.SelectedTab = this.tpMain;
                SwitchToolBarControlBackColor(this.btnMain);
            }
        }

        #endregion - Account LogInOut Methods -

        #region - Main Operation Methods -

        private void btnSystemConfiguration_Click(object sender, EventArgs e)
        {
            ParameterXMLForm<SystemData> propertyGridForm = new ParameterXMLForm<SystemData>(AOI.SystemData, "System Configuration");
            propertyGridForm.ShowDialog();
        }

        private void btnStartFlow_Click(object sender, EventArgs e)
        {
            AOI.SystemNotify(OperationStatus.Start);
            btnLoadRecipeFlow.Enabled = false;
            LogHelper.Operation("按下Start按鈕");
        }

        private void btnStopFlow_Click(object sender, EventArgs e)
        {
            //v1.0.3.5 卡控工程師權限
            if (AccountInfoManager.TestPermission(AccountLevel.Engineer))
            {
                if (MessageBox.Show("請確認是否停止程式流程!!", "重要", MessageBoxButtons.OKCancel) == System.Windows.Forms.DialogResult.OK)
                {
                    if (MessageBox.Show("按下確定按鈕停止程式流程!!", "重要", MessageBoxButtons.OKCancel) == System.Windows.Forms.DialogResult.OK)
                    {
                        AOI.WorkData.IsNeedHome = true;
                        AOI.SystemNotify(OperationStatus.Stop);
                        btnLoadRecipeFlow.Enabled = true;
                        btnHome.Enabled = true;
                        btnStartFlow.Enabled = true;
                        LogHelper.Operation("按下Stop按鈕");
                    }
                }
            }
        }

        Font MuteFont;
        private void btnMute_Click(object sender, EventArgs e)
        {
            if (this.btnMute.Text == "MUTE")
            {
                this.btnMute.Text = "UNMUTE";
                MuteFont = new Font(this.btnMute.Font.FontFamily, 14);
                this.btnMute.Font = MuteFont;
                ColorToDarkColor(this.btnMute, SystemColors.Highlight);
                AOI.IOs.SetRingtone(true);
            }
            else
            {
                this.btnMute.Text = "MUTE";
                MuteFont = new Font(this.btnMute.Font.FontFamily, 14);
                this.btnMute.Font = MuteFont;
                this.btnMute.BackColor = SystemColors.Highlight;
                AOI.IOs.SetRingtone(false);
            }
        }

        private void btnHome_Click(object sender, EventArgs e)
        {
            if (AOI.WorkData.MachineStatus == MachineStatus.Down)
            {
                MessageBox.Show("Machine is error. Please reset alarm and retry it.");
                return;
            }
            else if (AOI.FlowControlHelper.IsRunningAllFlowControl())
            {
                if (MessageBox.Show("程式流程執行中，確定要停止流程!", "警告", MessageBoxButtons.OKCancel) != DialogResult.OK)
                {
                    return;
                }
            }

            LogHelper.Operation("按下Home按鈕");
            btnHome.Enabled = false;        //160628.0850 修改Home按鈕使用時機

            //初始化流程
            AOI.SystemNotify(OperationStatus.Stop);
            Thread.Sleep(100);
            List<ErrorCode> statusResults;
            bool result = CheckStatus.DoCheck(
                out statusResults,
                AOI.SystemStatus.CheckMotionIsBusy,
                AOI.SystemStatus.CheckSensorOn,
                AOI.SystemStatus.CheckMachineStatus
                );
            if (!result)
            {
                MonitorLogger.Post(statusResults);
                AOI.RecordAllFlowBaseInfo("Home request dump");

                btnHome.Enabled = true;
                return;
            }
            if (!CheckStatus.DoCheck(out statusResults, AOI.SystemStatus.CheckSensorOn))
            {
                if (MessageBox.Show("請檢查後旋轉站是否有Wafer, 是否繼續Home流程?",
                    "", MessageBoxButtons.YesNo) == DialogResult.No)
                {
                    return;
                }
            }

            AOI.WorkData.IsAllHomeCompleted = false;
            ColorToDarkColor(this.btnHome, Color.Yellow);

            this.Enabled = false;
            AllHomeForm allHomeForm = new AllHomeForm();
            if (allHomeForm.ShowDialog() == DialogResult.OK)
            {
                this.btnHome.BackColor = Color.Yellow;
                btnLoadRecipeFlow.Enabled = true;
                AOI.WorkData.IsNeedHome = false;
            }
            this.Enabled = true;

            btnStartFlow.Enabled = true;
        }

        private void btnPauseFlow_Click(object sender, EventArgs e)
        {
            LogHelper.Operation("按下Pause按鈕");
            AOI.SystemNotify(OperationStatus.Pause);
        }

        private void btnAlarmReset_Click(object sender, EventArgs e)
        {
            AOI.SystemNotify(OperationStatus.AlarmReset);
        }

        private void btnLoadRecipeFlow_Click(object sender, EventArgs e)
        {
            if (AOI.WorkData.MachineStatus == MachineStatus.Down)
            {
                MessageBox.Show("Machine is error. Please reset alarm and retry it.");
                return;
            }
            LogHelper.Operation("按下LoadRecipe按鈕");
            LoadRecipeForm loadRecipForm = new LoadRecipeForm();
            if (loadRecipForm.ShowDialog() == DialogResult.OK)
            {
            }
        }

        private void ChangeControlBackColorToDarkColor()
        {
            ColorToDarkColor(this.btnStartFlow, Color.Lime);
            //ColorToDarkColor(this.btnPauseFlow, Color.Orange);
            //ColorToDarkColor(this.btnAlarmReset, Color.Red);
            //ColorToDarkColor(this.btnMute, SystemColors.Highlight);
            //ColorToDarkColor(this.btnStopFlow, SystemColors.Control);
            ColorToDarkColor(this.btnHome, Color.Yellow);
        }

        private void ColorToDarkColor(Control control, Color color)
        {
            double factor = 0.5;
            control.BackColor = Color.FromArgb((int)( factor * color.R ), (int)( factor * color.G ), (int)( factor * color.B ));
        }

        #endregion - Main Operation Methods -

        #region - SystemNotify Methods -

        private void AOI_SystemNotifyEvent(OperationStatus operationStatus, SystemNotifyCommand command)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new AOI.SystemNotifyEventHandler(AOI_SystemNotifyEvent), operationStatus, command);
                return;
            }
            LogHelper.Operation("{0} Invoke", operationStatus);
            switch (operationStatus)
            {
                case OperationStatus.Initialize:
                    SystemNotifyInitialize();
                    break;
                case OperationStatus.Start:
                    SystemNotifyStart();
                    break;
                case OperationStatus.Pause:
                    SystemNotifyPause();
                    break;
                case OperationStatus.Stop:
                    SystemNotifyStop();
                    break;
                case OperationStatus.Warning:
                    SystemNotifyWarning(command);
                    break;
                case OperationStatus.Alarm:
                    SystemNotifyAlarm(command);
                    break;
                case OperationStatus.AlarmReset:
                    SystemNotifyAlarmReset();
                    break;
                case OperationStatus.ClearOff:
                    SystemNotifyClearOff();
                    break;
                default:
                    throw new NotImplementedException(string.Format("{0} doesn't be implemented.", operationStatus));
            }
        }

        private void SystemNotifyInitialize()
        {
            AOI.FlowControlHelper.InitializeAll();
        }

        private void SystemNotifyStart()
        {
            if (AOI.WorkData.MachineStatus == MachineStatus.Down)
            {
                MessageBox.Show("Machine is error. Please reset alarm and retry it.");
                return;
            }
            else if (AOI.WorkData.IsNeedHome)
            {
                MessageBox.Show("請執行整機復歸動作");
                return;
            }
            else if (!AOI.WorkData.IsLoadRecipe)
            {
                List<string> status = new List<string>();
                //不使用手動載入Recipe檔案功能
                if (!AOI.SystemData.UseManualSelectRecipe)
                {
                    LogHelper.Operation("Auto Run New Lot");
                    LogHelper.Operation("Auto Run Recipe : {0:D3}_{1}", txtRecipeNo, txtRecipeID);

                    AOI.WorkData.IsLoadRecipe = true;

                    StartLot();
                }
            }
            //v1.0.2.5 Start前加入ByPassAutoJudge確認
            if (AOI.SystemStatus.ByPassAutoJudge)
            {
                if (MessageBox.Show("目前為分類槽強判測試流程，請確認是否繼續操作!", "警告", MessageBoxButtons.OKCancel) != DialogResult.OK)
                {
                    return;
                }
            }

            List<ErrorCode> statusResults;
            bool result = CheckStatus.DoCheck(
                out statusResults,
                AOI.SystemStatus.CheckAllHome,
                AOI.SystemStatus.CheckMotionIsBusy,
                AOI.SystemStatus.CheckSensorOn,
                AOI.SystemStatus.CheckMachineStatus,
                AOI.SystemStatus.CheckCommunicationStatus,
                AOI.SystemStatus.CheckBindingRecipeChange
                );
            if (!result)
            {
                MonitorLogger.Post(statusResults);
                AOI.RecordAllFlowBaseInfo("SystemNotifyStart dump");
                return;
            }

            AOI.WorkData.MachineStatus = MachineStatus.Run;
            AOI.IOs.SetSignalTowerAndRingtone(SignalTowerStatus.Green, RingtoneStatus.None);
            this.btnStartFlow.BackColor = Color.Lime;

            AOI.FlowControlHelper.GetFlowControl("AutoFlow").StartAll("AOI Flow Start");
        }

        private void SystemNotifyPause()
        {
            lock (_pauseKey)
            {
                if (AOI.WorkData.IsPauseFlowRunning)
                {
                    //正在執行暫停流程中
                    return;
                }
                AOI.WorkData.IsPauseFlowRunning = true;
            }

            AOI.IOs.SetSignalTowerAndRingtone(SignalTowerStatus.Yellow, RingtoneStatus.None);
            ColorToDarkColor(this.btnStartFlow, Color.Lime);

            AOI.FlowControlHelper.PauseAll("Pasue Flow");

            string isRunningItems = string.Empty;
            Task.Factory.StartNew(() =>
            {
                try
                {
                    MonitorLogger.Post("暫停流程開始");

                    Stopwatch sw = Stopwatch.StartNew();
                    if (!SpinWait.SpinUntil(() =>
                        !AOI.FlowControlHelper.IsRunningAllFlowControl(out isRunningItems) ||
                        AOI.WorkData.IsStopFlowRunning,
                        (int)( AOI.RecipeData.PauseFlowTimeOut * 1000 )))
                    {
                        LogHelper.Flow("PauseAll Flow Timeout, IsRunningItems : \r\n" + isRunningItems);
                        MonitorLogger.Post("PauseAll Flow Timeout, IsRunningItems : \r\n" + isRunningItems);
                    }
                    sw.Stop();
                    LogHelper.Flow("PauseAll Flow TACT : {0}", sw.ElapsedMilliseconds);
                }
                catch (Exception ex)
                {
                    LogHelper.Exception("SystemNotifyPause Error");
                    LogHelper.Exception(ex);
                }
                finally
                {
                    AOI.RecordAllFlowBaseInfo("after pause flow dump");

                    MonitorLogger.Post("暫停流程結束");
                    //等待停止流程
                    SpinWait.SpinUntil(() => !AOI.WorkData.IsStopFlowRunning, 3000);
                    if (AOI.WorkData.MachineStatus != MachineStatus.Down)
                    {
                        AOI.WorkData.MachineStatus = MachineStatus.Idle;
                    }
                    AOI.Modules.AllMotorStop(StopType.CmdWait);;

                    lock (_pauseKey)
                    {
                        AOI.WorkData.IsPauseFlowRunning = false;
                    }
                }
            });
        }

        private void SystemNotifyStop()
        {
            AOI.WorkData.IsStopFlowRunning = true;
            AOI.WorkData.IsLoadRecipe = false;
            AOI.IOs.SetSignalTowerAndRingtone(SignalTowerStatus.Red, RingtoneStatus.None);
            ColorToDarkColor(this.btnStartFlow, Color.Lime);
            AOI.FlowControlHelper.StopAll("Stop Flow");
            AOI.RecordAllFlowBaseInfo("after stop flow dump");
            if (AOI.WorkData.MachineStatus != MachineStatus.Down)
            {
                AOI.WorkData.MachineStatus = MachineStatus.Idle;
            }
            AOI.Modules.AllMotorStop(StopType.Emergency);
            AOI.WorkData.IsStopFlowRunning = false;
        }

        private void SystemNotifyWarning(SystemNotifyCommand command)
        {
            SystemNotifyCommandAction(ErrorType.Warning, command);
        }

        private void SystemNotifyAlarm(SystemNotifyCommand command)
        {
            AOI.RecordAllFlowBaseInfo("alarm raised dump");

            if (command.StopMode == StopMode.Pause)
            {
                SystemNotifyPause();
            }
            else if (command.StopMode == StopMode.Stop)
            {
                SystemNotifyStop();
            }
            else
            {
                SystemNotifyPause();
                //throw new ArgumentException("SystemNotifyAlarm SystemNotifyCommand.StopMode can't set None.");
            }
            AOI.WorkData.MachineStatus = MachineStatus.Down;

            SystemNotifyCommandAction(ErrorType.Alarm, command);
        }

        private void SystemNotifyCommandAction(ErrorType errorType, SystemNotifyCommand command)
        {
            string msg = GetErrorMessage(errorType, command);
            if (this.btnMute.Text == "UNMUTE")
            {
                this.btnMute.Text = "MUTE";
                this.btnMute.BackColor = SystemColors.Highlight;
            }
            AOI.IOs.SetSignalTowerAndRingtone(
                errorType == ErrorType.Alarm ? SignalTowerStatus.Red : SignalTowerStatus.Yellow,
                command.RingtoneStatus,
                command.RingtoneContinueTime);
            LogHelper.Flow(msg);
            if (errorType == ErrorType.Alarm)
            {
                LogHelper.Alarm(msg);
            }
            else
            {
                LogHelper.Warning(msg);
            }

            MonitorLogger.Post(msg);
        }

        private string GetErrorMessage(ErrorType errorType, SystemNotifyCommand command)
        {
            string msg = string.Empty;
            if (!string.IsNullOrEmpty(command.Message))
            {
                msg += command.Message;
            }
            if (command.ErrorCodes != null)
            {
                for (int i = 0; i < command.ErrorCodes.Count; i++)
                {
                    if (i > 0)
                    {
                        msg += "\r\n";
                    }
                    msg += NotifyErrorCode(errorType, command.ErrorCodes[i]);
                }
            }
            return msg;
        }

        private void SystemNotifyAlarmReset()
        {
            this.btnMute.Text = "MUTE";
            this.btnMute.BackColor = SystemColors.Highlight;
            AOI.IOs.SetRingtone(false);
            if (AOI.WorkData.MachineStatus != MachineStatus.Down)
            {
                MessageBox.Show("目前系統狀態正常, 無須清除異常");
                return;
            }
            if (AOI.FlowControlHelper.GetFlowControl("OtherFlow").IsRunning("AlarmResetFlow"))
            {
                MessageBox.Show("異常重置流程正常執行, 請等待一段時間後再重新嘗試");
                return;
            }
            string isRunningItems = string.Empty;
            if (AOI.FlowControlHelper.IsRunningAllFlowControl(out isRunningItems))
            {
                MessageBox.Show("Machine is running.\r\n" + isRunningItems);
                return;
            };
            AOI.FlowControlHelper.GetFlowControl("OtherFlow").Restart("AlarmResetFlow");

            //160628.0850 修改Home按鈕使用時機
            btnHome.Enabled = !AOI.WorkData.IsAllHomeCompleted;
        }

        private void SystemNotifyClearOff()
        {
            List<ErrorCode> statusResults;
            bool result = CheckStatus.DoCheck(
                out statusResults,
                AOI.SystemStatus.CheckMachineStatus
                );
            if (!result)
            {
                MonitorLogger.Post(statusResults);
                AOI.RecordAllFlowBaseInfo("SystemNotifyClearOff request dump");
                return;
            }
            AOI.FlowControlHelper.GetFlowControl("OtherFlow").Restart("ClearOffFlow");
        }

        #endregion - SystemNotify Methods -

        #region - Communication Methods -

        private void ConnectCommunication()
        {

        }

        private void DisconnectCommunication()
        {

        }

        #endregion - Communication Methods -

        #region - I / O Operation Methods -

        private void GeneratorIOSwitchControls()
        {
            Dictionary<string, string> ioList = LoadIOListFromCsv();
            Type type = typeof(IOController);
            PropertyInfo[] properties = type.GetProperties(BindingFlags.Instance | BindingFlags.Public);
            foreach (var property in properties)
            {
                DescriptionAttribute descriptionAttribute = (DescriptionAttribute)Attribute.GetCustomAttribute(property, typeof(DescriptionAttribute));
                IONumberAttribute ioNumberAttribute = (IONumberAttribute)Attribute.GetCustomAttribute(property, typeof(IONumberAttribute));
                if (ioList.Count > 0 && !ioList.ContainsKey(ioNumberAttribute.IONumber))
                {
                    throw new ArgumentException(string.Format("Unable to find {0} IO name definition from IO list.", ioNumberAttribute.IONumber));
                }
                if (property.Name.Contains("DI"))
                {
                    IOSwitchInput ioSwitchInput = new IOSwitchInput();
                    ioSwitchInput.Name = property.Name;
                    ioSwitchInput.IONumber = ioNumberAttribute.IONumber;
                    ioSwitchInput.IONumberSize = 75;
                    if (ioList.Count > 0)
                    {
                        ioSwitchInput.Text = ioList[ioNumberAttribute.IONumber];
                    }
                    else
                    {
                        ioSwitchInput.Text = descriptionAttribute.Description;
                    }
                    ioSwitchInput.Size = new Size(600, ioSwitchInput.Size.Height);
                    this.flpIOInput.Controls.Add(ioSwitchInput);
                }
                if (property.Name.Contains("DO"))
                {
                    IOSwitchOutput ioSwitchOutput = new IOSwitchOutput();
                    ioSwitchOutput.Name = property.Name;
                    ioSwitchOutput.IONumber = ioNumberAttribute.IONumber;
                    ioSwitchOutput.IONumberSize = 75;
                    if (ioList.Count > 0)
                    {
                        ioSwitchOutput.Text = ioList[ioNumberAttribute.IONumber];
                    }
                    else
                    {
                        ioSwitchOutput.Text = descriptionAttribute.Description;
                    }
                    ioSwitchOutput.Size = new Size(600, ioSwitchOutput.Size.Height);
                    ioSwitchOutput.TestPermission = true;
                    ioSwitchOutput.AccountLevel = AccountLevel.Engineer;
                    ioSwitchOutput.SwitchStatusChenged += new EventHandler(ioSwitchOutput_SwitchStatusChenged);
                    this.flpIOOutput.Controls.Add(ioSwitchOutput);
                }
            }
        }

        private Dictionary<string, string> LoadIOListFromCsv()
        {
            Dictionary<string, string> ioList = new Dictionary<string, string>();
            try
            {
                string filePath = string.Format(@"{0}\{1}", SystemDefine.SYSTEM_DATA_FOLDER_PATH, "IOList.csv");
                if (!File.Exists(filePath))
                {
                    return ioList;
                }
                string cultureName = Thread.CurrentThread.CurrentUICulture.ToString();
                List<string> csvStrings = SimpleCsvHelper.ReadCsvToList(filePath);
                List<string> headers = csvStrings[0].Split(',').ToList();
                int cultureIndex = headers.FindIndex(x => x == cultureName);
                if (cultureIndex > 0)
                {
                    for (int i = 1; i < csvStrings.Count; i++)
                    {
                        string[] subItems = csvStrings[i].Split(',');
                        ioList.Add(subItems[0], subItems[cultureIndex]);
                    }
                }
                return ioList;
            }
            catch (Exception ex)
            {
                LogHelper.Exception(ex);
                NotifyLogger.Post(ex);
                return ioList;
            }
        }

        private void BindingIOListState()
        {
            Type type = typeof(IOController);

            foreach (Control control in this.flpIOInput.Controls)
            {
                InvokeControlProperty(AOI.IOs, type, control);
            }
            foreach (Control control in this.flpIOOutput.Controls)
            {
                InvokeControlProperty(AOI.IOs, type, control);
            }
        }

        private void InvokeControlProperty(object target, Type type, Control control)
        {
            if (typeof(AOISystem.Utilities.Forms.IOSwitchInput) == control.GetType())
            {
                IOSwitchInput ioSwitchInput = (IOSwitchInput)control;
                bool handshakeState = (bool)type.InvokeMember(ioSwitchInput.Name, BindingFlags.GetProperty, null, target, null);
                if (handshakeState != ioSwitchInput.On)
                {
                    ioSwitchInput.On = handshakeState;
                }
            }
            else if (typeof(AOISystem.Utilities.Forms.IOSwitchOutput) == control.GetType())
            {
                IOSwitchOutput ioSwitchOutput = (IOSwitchOutput)control;
                bool handshakeState = (bool)type.InvokeMember(ioSwitchOutput.Name, BindingFlags.GetProperty, null, target, null);
                if (handshakeState != ioSwitchOutput.On)
                {
                    ioSwitchOutput.On = handshakeState;
                }
            }
        }

        private void ioSwitchOutput_SwitchStatusChenged(object sender, EventArgs e)
        {
            IOSwitchOutput ioSwitchOutput = (IOSwitchOutput)sender;
            Type type = typeof(IOController);
            type.InvokeMember(ioSwitchOutput.Name, BindingFlags.SetProperty, null, AOI.IOs, new object[] { ioSwitchOutput.On });
        }

        #endregion - I / O Operation Methods -

        #region - Motion Operation Methods -

        private void ShowMotorConfiguration_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            string motorName = btn.Name.Substring(3);
            if (string.IsNullOrEmpty(motorName))
            {
                return;
            }

            PropertyInfo propertyInfo = typeof(Modules).GetProperty(motorName);
            if (propertyInfo != null)
            {
                CEtherCATMotion iMotion = null;
                iMotion = (CEtherCATMotion)propertyInfo.GetValue(AOI.Modules, null);
                if (iMotion != null)
                {
                    iMotion.ConfigurationShow();
                }
            }
        }

        private void btnSystemMotionPositionSave_Click(object sender, EventArgs e)
        {
            #region 系統點位存檔
            if (SystemRecipeData(tabMotion, DataTypeEnum.System, ActionTyepEnum.Save, GetSystemRecipeData(DataTypeEnum.System)))
                MessageBox.Show("存檔完成!");
            #endregion
        }

        private void btnSystemMotionPositionRestore_Click(object sender, EventArgs e)
        {
            SystemMotionPositionRestore();
        }

        private void SystemMotionPositionRestore()
        {
            #region 系統點位回復
            SystemRecipeData(tabMotion, DataTypeEnum.System, ActionTyepEnum.Load, GetSystemRecipeData(DataTypeEnum.System));
            #endregion
        }

        #endregion - Motion Operation Methods -

        #region - Recipe Methods -

        private void MainForm_RecipeInfoCollectionChangedEvent(RecipeInfoCollection recipeInfoCollection)
        {
        }

        private void MainForm_RecipeInfoSelectedIndexChangedEvent(RecipeInfo recipeInfo)
        {
            LogHelper.Operation("切換Recipe : {0}", recipeInfo);
            AOI.SystemData.DefaultRecipeID = recipeInfo.RecipeID;
            AOI.SystemData.Save();
            this.lblRecipeNo.Text = string.Format("Recipe No : {0}", recipeInfo.RecipeNo);
            this.lblRecipeID.Text = string.Format("Recipe ID : {0}", recipeInfo.RecipeID);
            this.txtRecipeNo.Text = recipeInfo.RecipeNo.ToString("D3");
            this.txtRecipeID.Text = recipeInfo.RecipeID;
            this.txtDescription.Text = recipeInfo.Description;

            AOI.RecipeData.CurrentRecipe = ObjectCopier.Clone<RecipeInfo>(recipeInfo); ;
            AOI.RecipeData.Load();

            SystemMotionPositionRestore();

            SystemRecipeData(tabRecipe, DataTypeEnum.Recipe, ActionTyepEnum.Load, GetSystemRecipeData(DataTypeEnum.Recipe));

            AOI.SystemStatus.Load(AOI.RecipeData.CurrentRecipe.GetRecipePath());

            AOI.SystemNotify(OperationStatus.Initialize);
        }

        private void MainForm_RecipeInfoCopyChangedEvent(RecipeInfo oldRecipeInfo, RecipeInfo newRecipeInfo)
        {
        }

        private void MainForm_ErrorRaised(object sender, int errorCode, string errorMsg)
        {
            MonitorLogger.Post(errorMsg);
            LogHelper.Exception(errorMsg);
        }

        private void recipeEditorControl_SelectedRecipeInfoEvent(RecipeInfo recipeInfo)
        {
            RecipeData recipeData = new RecipeData();
            recipeData.CurrentRecipe = this.recipeEditorControl.SelectedRecipeInfo;
            recipeData.Load();

            SystemRecipeData(tabRecipe, DataTypeEnum.Recipe, ActionTyepEnum.Load, GetSystemRecipeData(DataTypeEnum.Recipe), recipeData);
        }

        private void btnSaveSystemData_Click(object sender, EventArgs e)
        {
            if (SystemRecipeData(flpSystem, DataTypeEnum.System, ActionTyepEnum.Save, GetSystemRecipeData(DataTypeEnum.System)))
                MessageBox.Show("存檔完成!");
        }

        private void btnRecipeSave_Click(object sender, EventArgs e)
        {
            RecipeData recipeData = new RecipeData();
            recipeData.CurrentRecipe = this.recipeEditorControl.SelectedRecipeInfo;
            recipeData.Load();
            if (SystemRecipeData(tabRecipe, DataTypeEnum.Recipe, ActionTyepEnum.Save, GetSystemRecipeData(DataTypeEnum.Recipe), recipeData))
            {
                if (AOI.RecipeData.CurrentRecipe.RecipeID == recipeData.CurrentRecipe.RecipeID)
                    AOI.RecipeData.Load();
                MessageBox.Show("Recipe 存檔完成");
            }
            else
            {
                MessageBox.Show("Recipe 存檔失敗");
            }
        }

        private void btnRecipeRestore_Click(object sender, EventArgs e)
        {
            RecipeData recipeData = new RecipeData();
            recipeData.CurrentRecipe = this.recipeEditorControl.SelectedRecipeInfo;
            recipeData.Load();
            SystemRecipeData(tabRecipe, DataTypeEnum.Recipe, ActionTyepEnum.Load, GetSystemRecipeData(DataTypeEnum.Recipe), recipeData);
        }

        public enum DataTypeEnum { System, Recipe };

        public enum ActionTyepEnum { Load, Save };

        private bool SystemRecipeData(Control ctl, DataTypeEnum dataType, ActionTyepEnum actionType, object aoiInstance, object selectedInstance = null)
        {
            object dataInstance = null;
            string propertyName = string.Empty;

            dataInstance = ( selectedInstance != null ) ? selectedInstance : aoiInstance;

            try
            {
                foreach (Control item in ctl.Controls)
                {
                    if (ctl.HasChildren == true)
                    {
                        SystemRecipeData(item, dataType, actionType, aoiInstance, selectedInstance);
                    }

                    if (item is MotionPositionControl)
                    {
                        MotionPositionControl motionPositionControl = item as MotionPositionControl;

                        if (actionType == ActionTyepEnum.Load)
                        {
                            if (selectedInstance != null) motionPositionControl.Restore(selectedInstance);
                            else motionPositionControl.Restore();
                        }
                        else motionPositionControl.Save(selectedInstance);
                    }
                    else
                    {
                        if (item.Tag == null || string.IsNullOrEmpty(item.Tag.ToString()))
                        {
                            continue;
                        }

                        propertyName = item.Tag.ToString();
                        PropertyInfo propertyInfo = null;

                        if (dataType == DataTypeEnum.Recipe)
                            propertyInfo = typeof(RecipeData).GetProperty(propertyName);
                        else
                            propertyInfo = typeof(SystemData).GetProperty(propertyName);

                        if (propertyInfo == null)
                            throw new Exception();

                        if (item is RecipeTextControl)
                        {
                            RecipeTextControl recipeTextControl = item as RecipeTextControl;
                            if (actionType == ActionTyepEnum.Load) recipeTextControl.Value = (double)propertyInfo.GetValue(dataInstance, null);
                            else propertyInfo.SetValue(dataInstance, recipeTextControl.Value, null);
                        }
                        else if (item is RecipeBooleanControl)
                        {
                            RecipeBooleanControl recipeBoolControl = item as RecipeBooleanControl;
                            if (actionType == ActionTyepEnum.Load) recipeBoolControl.Value = (bool)propertyInfo.GetValue(dataInstance, null);
                            else propertyInfo.SetValue(dataInstance, recipeBoolControl.Value, null);
                        }
                        else if (item is RecipeNumericUpDownControl)
                        {
                            RecipeNumericUpDownControl recipeNumericUpDownControl = item as RecipeNumericUpDownControl;
                            if (actionType == ActionTyepEnum.Load) recipeNumericUpDownControl.Value = Convert.ToDouble(propertyInfo.GetValue(dataInstance, null));
                            else PropertyHelper.PropertyInfoSetValue(dataInstance, propertyInfo, recipeNumericUpDownControl.Value);
                        }
                        else if (item is RecipeListControl)
                        {
                            RecipeListControl recipeListControl = item as RecipeListControl;
                            if (actionType == ActionTyepEnum.Load) recipeListControl.ListValue = (string)propertyInfo.GetValue(dataInstance, null);
                            else propertyInfo.SetValue(dataInstance, recipeListControl.ListValue, null);
                        }
                        else if (item is RecipeLongTextControl)
                        {
                            RecipeLongTextControl recipeLongTextControl = item as RecipeLongTextControl;
                            if (actionType == ActionTyepEnum.Load) recipeLongTextControl.Value = (string)propertyInfo.GetValue(dataInstance, null);
                            else propertyInfo.SetValue(dataInstance, recipeLongTextControl.Value, null);
                        }
                        else if (item is RecipePathControl)
                        {
                            RecipePathControl recipePathControl = item as RecipePathControl;
                            if (actionType == ActionTyepEnum.Load) recipePathControl.Value = (string)propertyInfo.GetValue(dataInstance, null);
                            else propertyInfo.SetValue(dataInstance, recipePathControl.Value, null);
                        }
                    }
                }

                if (actionType == ActionTyepEnum.Save)
                {
                    if (dataType == DataTypeEnum.Recipe)
                    {
                        RecipeData recipe = (RecipeData)dataInstance;
                        recipe.Save();
                    }
                    else
                    {
                        SystemData system = (SystemData)dataInstance;
                        system.Save();
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                string errorMsg = string.Format("SystemRecipeData PropertyName {0} Error.\r\nMessage : {1}\r\nStackTrace : {2}\r\n", propertyName, ex.Message, ex.StackTrace);
                MessageBox.Show(errorMsg);
                LogHelper.Exception(errorMsg);
                return false;
            }
        }

        private object GetSystemRecipeData(DataTypeEnum dataType)
        {
            switch (dataType)
            {
                default:
                case DataTypeEnum.System:
                    AOI.SystemData.Load();
                    return AOI.SystemData;
                case DataTypeEnum.Recipe:
                    AOI.RecipeData.Load();
                    return AOI.RecipeData;
            }
        }

        #endregion - Recipe Methods -
    }
}

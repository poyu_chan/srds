﻿
namespace MotionSystem.Project.Common.StatusItems
{
    public class CheckBindingRecipeChange : CheckStatusBase
    {
        public override void DoCheck()
        {
            if (!AOI.SystemStatus.ByPassCheckBindingRecipeChange)
            {
                if (!AOI.WorkData.IsRecipeReadyAOI1)
                {
                    AddStatus(6003, "AOI1 PC Recipe切換失敗");
                }
            }
        }
    }
}

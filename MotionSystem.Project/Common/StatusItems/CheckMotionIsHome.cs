﻿using System.Reflection;
using AOISystem.Utilities.Modules;
using AOISystem.Utilities.Modules.Syntek.EtherCAT.SlaveModules.Motion;

namespace MotionSystem.Project.Common.StatusItems
{
    public class CheckMotionIsHome : CheckStatusBase
    {
        public override void DoCheck()
        {
            if (!AOI.SystemStatus.ByPassCheckMotionIsHome)
            {
                PropertyInfo[] propertyInfos = typeof(Modules).GetProperties();
                foreach (var propertyInfo in propertyInfos)
                {
                    object instance = propertyInfo.GetValue(AOI.Modules, null);
                    if (instance is CEtherCATMotion)
                    {
                        CEtherCATMotion iMotion = instance as CEtherCATMotion;
                        if (!iMotion.IsHome)
                        {
                            AddStatus("Check Motion {0} Home", propertyInfo.Name);
                        }
                    }
                }
            }
        }
    }
}

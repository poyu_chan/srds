﻿using AOISystem.Utilities;
using AOISystem.Utilities.Collections;
using AOISystem.Utilities.IO;
using System;
using System.Collections.Generic;
using System.IO;

namespace MotionSystem.Project.Common.StatusItems
{
    public class CheckSafeDoor : CheckStatusBase
    {

        public static SerializableDictionary<string, string> dicSetting = new SerializableDictionary<string, string>();

        string filePath = string.Empty;
        string propertyName = string.Empty;
        int errorCode = 0;
        bool isLoad = false;

        public CheckSafeDoor()
        {
            filePath = string.Format(@"{0}\{1}", SystemDefine.SYSTEM_DATA_FOLDER_PATH, SystemDefine.SAFEDOOR_SETTINGS_FILE_NAME);
            LoadFile();
        }

        private bool LoadFile()
        {
            if (!File.Exists(filePath))
            {
                return false;
            }

            dicSetting = XmlFile.DeserializeFromFile<SerializableDictionary<string, string>>(filePath);
            return true;
        }

        public override void DoCheck()
        {
            bool _safeDoorEnable;
            //v1.0.6.38 改為跑片中才偵測安全門
            if (!AOI.SystemStatus.ByPassCheckSafeDoor && AOI.WorkData.MachineStatus == MachineStatus.Run)
            {
                foreach (KeyValuePair<string, string> item in dicSetting)
                {
                    _safeDoorEnable = bool.Parse(item.Value.Split('_')[0]);

                    if (_safeDoorEnable)
                    {
                        propertyName = string.Format("DI{0}{1}{2}", item.Key.Split('_')[0], item.Key.Split('_')[1], item.Key.Split('_')[2]);
                        errorCode = int.Parse(item.Value.Split('_')[1]);

                        try
                        {
                        if (!AOI.IOs.GetIO(propertyName))
                        {
                            AddStatus(errorCode);
                            }
                        }
                        catch (Exception ex)
                        {
                            AddStatus("CheckSafeDoor找不到Key : {0}", propertyName);
                        }
                    }
                }
            }
        }
    }
}

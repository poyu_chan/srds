﻿
namespace MotionSystem.Project.Common.StatusItems
{
    public class CheckAllHome : CheckStatusBase
    {
        public override void DoCheck()
        {
            if (!AOI.SystemStatus.ByPassCheckAllHome)
            {
                if (!AOI.WorkData.IsAllHomeCompleted)
                {
                    AddStatus(5000, "全機復歸未完成");
                }
            }
        }
    }
}

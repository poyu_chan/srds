﻿using AOISystem.Utilities.Modules.Syntek.EtherCAT.SlaveModules.Motion;
using System.Reflection;

namespace MotionSystem.Project.Common.StatusItems
{
    public class CheckMotionIsBusy : CheckStatusBase
    {
        public override void DoCheck()
        {
            if (!AOI.SystemStatus.ByPassCheckMotionIsBusy)
            {
                PropertyInfo[] propertyInfos = typeof(Modules).GetProperties();
                foreach (var propertyInfo in propertyInfos)
                {
                    object instance = propertyInfo.GetValue(AOI.Modules, null);
                    if (instance is CEtherCATMotion)
                    {
                        CEtherCATMotion iMotion = instance as CEtherCATMotion;
                        if (iMotion.IsBusy)
                        {
                            AddStatus("Check Motion {0} IsBusy", propertyInfo.Name);
                        }
                    }
                }
            }
        }
    }
}

﻿using AOISystem.Utilities.Modules.Syntek.EtherCAT.SlaveModules.Motion;
using System.Reflection;
using System.Threading;

namespace MotionSystem.Project.Common.StatusItems
{
    public class CheckMotionAlarm : CheckStatusBase
    {
        public override void DoCheck()
        {
            if (!AOI.SystemStatus.ByPassCheckMotionAlarm)
            {
                bool firstDelay = true;
                PropertyInfo[] propertyInfos = typeof(Modules).GetProperties();
                foreach (var propertyInfo in propertyInfos)
                {
                    object instance = propertyInfo.GetValue(AOI.Modules, null);
                    if (instance is CEtherCATMotion)
                    {
                        CEtherCATMotion iMotion = instance as CEtherCATMotion;
                        if (iMotion.IsAlarm)
                        {
                            //v1.0.6.39 修正多顆馬達同時錯誤時等待時間過久問題
                            if (firstDelay)
                            {
                                firstDelay = false;
                                Thread.Sleep(AOI.SystemData.MotionErrorCodeDelayTime);
                            }
                            string errorCode = iMotion.GetErrorCode();
                            if (errorCode == "0.0")
                            {
                                AddStatus("Check Motion {0} Alarm", propertyInfo.Name);
                            }
                            else
                            {
                                //v1.0.6.16 修正馬達驅動器異常時無異常代碼錯誤
                                AddStatus(1400, string.Format("Check Motion {0} Alarm [ErrorCode : {1}]", propertyInfo.Name, errorCode));
                            }
                        }
                    }
                }
            }
        }
    }
}

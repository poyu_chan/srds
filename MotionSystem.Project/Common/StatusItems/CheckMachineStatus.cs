﻿using AOISystem.Utilities;

namespace MotionSystem.Project.Common.StatusItems
{
    public class CheckMachineStatus : CheckStatusBase
    {
        public override void DoCheck()
        {
            if (!AOI.SystemStatus.ByPassCheckMachineStatus)
            {
                if (AOI.WorkData.MachineStatus == MachineStatus.Down)
                {
                    AddStatus("系統尚存在異常，請排除異常後重新操作");
                }

                string isRunningItems = string.Empty;
                if (AOI.FlowControlHelper.IsRunningAllFlowControl(out isRunningItems))
                {
                    AddStatus("Machine is running.\r\n" + isRunningItems);
                    return;
                };

                if (AOI.WorkData.IsPauseFlowRunning)
                {
                    AddStatus("Pause Flow Is Running");
                }
            }
        }
    }
}

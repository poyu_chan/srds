﻿
namespace MotionSystem.Project.Common.StatusItems
{
    public class CheckAirDetect : CheckStatusBase
    {
        public override void DoCheck()
        {
            if (!AOI.SystemStatus.ByPassCheckAirDetect)
            {
                if (!AOI.IOs.DICDASourcePressureSensor)
                {
                    AddStatus(1200, "空壓異常");
                }
            }
        }
    }
}

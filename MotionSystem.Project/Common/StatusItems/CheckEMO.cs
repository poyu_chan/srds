﻿
namespace MotionSystem.Project.Common.StatusItems
{
    public class CheckEMO : CheckStatusBase
    {
        public override void DoCheck()
        {
            if (!AOI.SystemStatus.ByPassCheckEMO)
            {
                if (!AOI.IOs.DIEmergencyStopSensor)
                {
                    AddStatus(1003, "Loader右前方EMO已按壓");
                }
            }
        }
    }
}

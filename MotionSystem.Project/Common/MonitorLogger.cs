﻿using AOISystem.Utilities.Common;
using AOISystem.Utilities.Logging;
using MotionSystem.Project.Forms;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Forms;

namespace MotionSystem.Project.Common
{
    public static class MonitorLogger
    {
        public static SynchronizationContext _synchronizationContext;

        public static void InitializeConfiguration(SynchronizationContext synchronizationContext)
        {
            _synchronizationContext = synchronizationContext;
        }

        public static void SynchronizedMethod(Action action)
        {
            if (_synchronizationContext != null)
            {
                _synchronizationContext.Send((o) =>
                {
                    action();
                }, null);
            }
            else
            {
                action();
            }
        }

        public static void Post(string format, params object[] arg)
        {
            Post(string.Format(format, arg));
        }

        public static void Post(string message, bool showForm = true)
        {
            LogHelper.Monitor(message);
            SynchronizedMethod(() => 
            {
                string writeString = string.Format("{0:yyyy.MM.dd HH:mm:ss} | {1}", DateTime.Now, message);
                if (showForm) ShowMonitor();
                MonitorForm.GetInstance().NotifyMessage(writeString);
            });
        }

        public static void Post(Exception e)
        {
            if (e.InnerException != null)
            {
                if (e.InnerException is AggregateException)
                {
                    AggregateException aggEx = (AggregateException)e.InnerException;
                    foreach (Exception ex in aggEx.InnerExceptions)
                    {
                        Post(ex);
                    }
                }
                else
                {
                    Post(e.InnerException.Message);
                    Post(e.InnerException.StackTrace);
                }
            }
            else
            {
                Post(e.Message);
                Post(e.StackTrace);
            }
        }

        public static void Post(List<string> messageList)
        {
            foreach (string message in messageList)
            {
                Post(message);
            }
        }

        public static void Post(List<ErrorCode> messageList)
        {
            foreach (ErrorCode message in messageList)
            {
                Post(message.ToString());
            }
        }

        public static void Clear()
        {
            SynchronizedMethod(() =>
            {
                ShowMonitor();
                MonitorForm.GetInstance().ClearMessage();
            });
        }

        public static void ShowMonitor()
        {
            SynchronizedMethod(() =>
            {
                MonitorForm msgForm = null;
                bool wExist = CheckFormIsOpen("MonitorForm");

                if (!wExist)
                {
                    msgForm = MonitorForm.GetInstance();
                    msgForm.Name = "MonitorForm";
                    msgForm.StartPosition = FormStartPosition.CenterScreen;
                    msgForm.Show();
                }
                else
                {
                    msgForm = (MonitorForm)Application.OpenForms["MonitorForm"];
                    if (msgForm.WindowState == FormWindowState.Minimized)
                    {
                        msgForm.WindowState = FormWindowState.Normal;
                    }
                    msgForm.Activate();
                }
            });
        }

        public static void CloseMonitor()
        {
            SynchronizedMethod(() =>
            {
                if (CheckFormIsOpen("MonitorForm"))
                {
                    MonitorForm msgForm = (MonitorForm)Application.OpenForms["MonitorForm"];
                    msgForm.FormClose();
                }
            });
        }

        //判斷窗口是否已經打開
        private static bool CheckFormIsOpen(string Forms)
        {
            bool bResult = false;
            foreach (Form frm in Application.OpenForms)
            {
                if (frm.Name == Forms)
                {
                    bResult = true;
                    break;
                }
            }
            return bResult;
        }

        private static void _lopper_LogMessageRecorded(DateTime date, string logCategory, string logText)
        {
            string message = string.Format("{0} | {1} | {2}", date.ToString("yyyy.MM.dd HH:mm:ss.fff"), logCategory, logText);
            if (Log.ShowLog != null)
            {
                Log.ShowLog(message);   
            }
        }
    }
}

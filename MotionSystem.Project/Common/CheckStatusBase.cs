﻿using AOISystem.Utilities.Common;

namespace MotionSystem.Project.Common
{
    public class CheckStatusBase
    {
        private CheckStatusContainer _checkStatusContainer;

        protected CheckStatusBase()
        {
        }

        public void BlindingContainer(CheckStatusContainer checkStatusContainer)
        {
            _checkStatusContainer = checkStatusContainer;
        }

        /// <summary>
        /// 狀態確認方法, 須覆寫
        /// </summary>
        public virtual void DoCheck()
        {
        }

        /// <summary>
        /// 狀態結果新增
        /// </summary>
        /// <param name="errorCode">錯誤碼</param>
        protected void AddStatus(ErrorCode errorCode)
        {
            _checkStatusContainer.StatusResults.Add(errorCode);
        }

        /// <summary>
        /// 狀態結果新增
        /// </summary>
        /// <param name="code">錯誤碼</param>
        /// <param name="message">錯誤訊息</param>
        protected void AddStatus(int code, string message)
        {
            ErrorCode errorCode = new ErrorCode(code, message);
            AddStatus(errorCode);
        }

        /// <summary>
        /// 狀態結果新增
        /// </summary>
        /// <param name="code">錯誤碼</param>
        protected void AddStatus(int code)
        {
            ErrorCode errorCode = AOI.ErrorCodeHelper.GetErrorCode(ErrorType.Alarm, code);
            AddStatus(errorCode);
        }

        /// <summary>
        /// 狀態結果新增
        /// </summary>
        /// <param name="status">結果字串長度大於0為False</param>
        protected void AddStatus(string format, params object[] args)
        {
            ErrorCode errorCode = new ErrorCode(-1, string.Format(format, args));
            AddStatus(errorCode);
        }
    }
}

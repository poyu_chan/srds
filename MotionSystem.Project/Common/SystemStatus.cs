﻿using AOISystem.Utilities;
using AOISystem.Utilities.Common;
using AOISystem.Utilities.Component;
using MotionSystem.Project.Common.StatusItems;
using System.ComponentModel;

namespace MotionSystem.Project.Common
{
    public class SystemStatus : ParameterINI
    {
        #region - Constructor -

        public SystemStatus()
            : base(SystemDefine.SYSTEM_DATA_FOLDER_PATH)
        { 
        }

        #endregion - Constructor -
        
        #region - By Pass Properties -

        public bool ByPassCheckEMO { get; set; }
        public bool ByPassCheckSafeDoor { get; set; }
        public bool ByPassCheckMotionAlarm { get; set; }
        public bool ByPassCheckMotionIsBusy { get; set; }
        public bool ByPassCheckMotionIsHome { get; set; }
        public bool ByPassCheckAirDetect { get; set; }
        public bool ByPassCheckAllHome { get; set; }
        public bool ByPassCheckWaferCheckerError { get; set; }
        public bool ByPassCheckSensorOn { get; set; }
        public bool ByPassCheckMachineStatus { get; set; }
        public bool ByPassCheckCommunicationStatus { get; set; }

        [Browsable(true), Category("AOI設定"), Description("是否忽略AOI流程")]
        public bool ByPassAOIFlow { get; set; }

        /// <summary>是否忽略使用自動影像辨識結果</summary>
        [Browsable(true), Category("判定設定"), Description("是否忽略使用自動影像辨識結果"), NonMember()]
        public bool ByPassAutoJudge { get; set; }

        /// <summary>是否忽略檢測EdgeL1站</summary>
        [Browsable(true), Category("檢測設定"), Description("是否忽略檢測EdgeL1站")]
        public bool ByPassInspectEdgeL1 { get; set; }

        /// <summary>是否忽略檢測EdgeR1站</summary>
        [Browsable(true), Category("檢測設定"), Description("是否忽略檢測EdgeR1站")]
        public bool ByPassInspectEdgeR1 { get; set; }

        /// <summary>是否忽略檢測EdgeL2站</summary>
        [Browsable(true), Category("檢測設定"), Description("是否忽略檢測EdgeL2站")]
        public bool ByPassInspectEdgeL2 { get; set; }

        /// <summary>是否忽略檢測EdgeR2站</summary>
        [Browsable(true), Category("檢測設定"), Description("是否忽略檢測EdgeR2站")]
        public bool ByPassInspectEdgeR2 { get; set; }

        /// <summary>是否忽略綁定Recipe切換</summary>
        [Browsable(true), Category("通訊設定"), Description("是否忽略綁定Recipe切換")]
        public bool ByPassCheckBindingRecipeChange { get; set; }

        #endregion - By Pass Properties -

        #region - Check Items -

        //loop check

        public CheckEMO CheckEMO = new CheckEMO();
        public CheckSafeDoor CheckSafeDoor = new CheckSafeDoor();
        public CheckMotionAlarm CheckMotionAlarm = new CheckMotionAlarm();
        public CheckAirDetect CheckAirDetect = new CheckAirDetect();
        public CheckSensorOn CheckSensorOn = new CheckSensorOn();

        //general check

        public CheckAllHome CheckAllHome = new CheckAllHome();
        public CheckMachineStatus CheckMachineStatus = new CheckMachineStatus();
        public CheckMotionIsBusy CheckMotionIsBusy = new CheckMotionIsBusy();
        public CheckMotionIsHome CheckMotionIsHome = new CheckMotionIsHome();
        public CheckCommunicationStatus CheckCommunicationStatus = new CheckCommunicationStatus();
        public CheckBindingRecipeChange CheckBindingRecipeChange = new CheckBindingRecipeChange();
        #endregion - Check Items -
    }
}

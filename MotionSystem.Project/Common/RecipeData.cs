﻿using AOISystem.Utilities.Common;
using AOISystem.Utilities.Recipe;
using System;
using System.ComponentModel;
using System.Xml.Serialization;

namespace MotionSystem.Project.Common
{
    [Serializable]
    public class RecipeData : ParameterXML<RecipeData>
    {
        public RecipeData()
        {
        }

        private RecipeInfo _currentRecipe;
        [Browsable(true), Category("Recipe Info"), Description("Current Recipe Info"), XmlIgnore()]
        public RecipeInfo CurrentRecipe
        {
            get { return _currentRecipe; }
            set 
            { 
                _currentRecipe = value;
                this.FolderPath = _currentRecipe.GetRecipePath();
            }
        }

        #region Other

        [Browsable(true), Category("WaferChecker Setting"), Description("在籍單站容許誤差距離(mm)")]
        public double ExtraMaxDistance { get; set; }

        [Browsable(true), Category("WaferChecker Setting"), Description("在籍單站容許存活時間(S)")]
        public double ExtraSurvivalTime { get; set; }

        [Browsable(true), Category("Flow Setting"), Description("清片動作執行時間(S)")]
        public double ClearOffFlowContiunanceTime { get; set; }

        [Browsable(true), Category("Flow Setting"), Description("暫停流程逾時(S)")]
        public double PauseFlowTimeOut { get; set; }

        [Browsable(true), Category("UI Setting"), Description("Inspect CalResult Display Number")]
        public int InspectResultDisplayNumber { get; set; }

        #endregion Other

        public T GetProperty<T>(string propertyName)
        {
            return (T)this.GetType().GetProperty(propertyName).GetValue(this, null);
        }
    }

}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net.NetworkInformation;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Windows.Forms;
using AOISystem.Utilities;
using AOISystem.Utilities.Common;
using AOISystem.Utilities.Flow;
using AOISystem.Utilities.IO;
using AOISystem.Utilities.Logging;
using MotionSystem.Project.Resources;

namespace MotionSystem.Project.Common
{
    public class AOI
    {
        //初始化是否成功
        public static bool IsInitialized = false;
        //系統參數
        public static SystemData SystemData = null;
        //目前Recipe資料
        public static RecipeData RecipeData = null;
        //系統狀態, IO狀態
        public static SystemStatus SystemStatus = null;
        //工作狀態, 程式旗標
        public static WorkData WorkData = null;
        //模組管理
        public static Modules Modules = null;
        //IO控制
        public static IOController IOs = null;
        //流程控制管理
        public static FlowControlHelper FlowControlHelper = null;
        //Error Code管理
        public static ErrorCodeHelper ErrorCodeHelper = null;
        //多國語言
        public static ResourceManager MultiLanguage = null;

        public static bool Initialize()
        {
            try
            {
                SystemData = new SystemData();
                RecipeData = new RecipeData();
                SystemStatus = new SystemStatus();
                WorkData = new WorkData();
                Modules = Modules.GetInstance();
                IOs = new IOController(Modules.DI1, Modules.DO1);
                FlowControlHelper = new FlowControlHelper();
                ErrorCodeHelper = new ErrorCodeHelper(SystemDefine.SYSTEM_DATA_FOLDER_PATH);
                MultiLanguage = new ResourceManager(typeof(MultiLanguage));
                IsInitialized = true;
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\r\n" + ex.StackTrace);
                Log.Exception(ex);
                return false;
            }
        }

        #region - SystemNotify Methods -

        public delegate void SystemNotifyEventHandler(OperationStatus operationStatus, SystemNotifyCommand command);
        public static event SystemNotifyEventHandler SystemNotifyEvent;

        public static void SystemNotify(OperationStatus operationStatus, SystemNotifyCommand command = null)
        {
            AOI.WorkData.OperationStatus = operationStatus;
            if (command == null)
            {
                command = new SystemNotifyCommand();
            }
            command.OperationStatus = operationStatus;
            OnSystemNotifyEvent(operationStatus, command);
        }

        private static void OnSystemNotifyEvent(OperationStatus operationStatus, SystemNotifyCommand command)
        {
            if (SystemNotifyEvent != null)
            {
                SystemNotifyEvent(operationStatus, command);
            }
        }

        #endregion - SystemNotify Methods -

        //取得參考組件版本訊息
        public static string GetVersionInfo()
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendLine("==========系統組件==========");
            Assembly assembly = Assembly.GetExecutingAssembly();
            FileVersionInfo fileVersionInfo = FileVersionInfo.GetVersionInfo(assembly.Location);
            stringBuilder.AppendLine(string.Format("{0} : Version={1}, FileVersion={2}", assembly.ManifestModule, assembly.GetName().Version, fileVersionInfo.FileVersion));
            string[] referencedAssemblies = new string[] 
            { 
                "AOISystem.SPC.dll",
                "AOISystem.Utilities.dll",
                "CsvHelper.dll",
                "TcpSocketControl.dll",
                "AOISystem.Communication.dll"
            };
            foreach (string asmName in referencedAssemblies)
            {
                if (File.Exists(string.Format(@"{0}\{1}", Application.StartupPath, asmName)))
                {
                    Assembly refAssembly = Assembly.LoadFrom(asmName);
                    FileVersionInfo refFileVersionInfo = FileVersionInfo.GetVersionInfo(refAssembly.Location);
                    stringBuilder.AppendLine(string.Format("{0} : Version={1}, FileVersion={2}", refAssembly.ManifestModule, refAssembly.GetName().Version, refFileVersionInfo.FileVersion));
                }
            }
            return stringBuilder.ToString();
        }

        //紀錄流程項目狀態
        public static void RecordAllFlowBaseInfo(string description = "dump")
        {
            DateTime dateTime = DateTime.Now;
            string folderPath = string.Format(@"{0}\{1}", AOI.SystemData.DataSavePath, dateTime.ToString("yyyyMMdd"));
            string fileName = "FlowBaseInfoDump.csv";
            string filePath = string.Format(@"{0}\{1}", folderPath, fileName);
            if (!File.Exists(filePath))
            {
                SimpleCsvHelper.AddData(folderPath, fileName, "{0},{1},{2},{3},{4},{5},{6},{7},{8}", "Time", "Name", "IsRunning", "Status", "Step1", "Step2", "Step3", "Description", "OtherMessage");
            }
            SimpleCsvHelper.AddData(folderPath, fileName, "{0},{1},{2},{3},{4},{5},{6},{7},{8}", dateTime.ToString("yyyy.MM.dd HH:mm:ss"), description, "TRUE", "Initialize", "-1", "-1", "-1", "", "");
            List<string> flowBaseInfos = FlowControlHelper.GetAllFlowBaseInfo();
            foreach (var item in flowBaseInfos)
            {
                SimpleCsvHelper.AddData(folderPath, fileName, item);
            }
        }

        /// <summary>
        /// 判斷主機是否為RTX系統
        /// </summary>
        /// <returns></returns>
        private static bool CheckIsRTXSystem()
        {
            bool isRTX = false;
            foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
            {
                if (nic.Description.Contains("RTX"))
                {
                    isRTX = true;
                    break;
                }
            }
            return isRTX;
        }
    }
}

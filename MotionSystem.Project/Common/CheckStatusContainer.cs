﻿using AOISystem.Utilities.Common;
using System.Collections.Generic;

namespace MotionSystem.Project.Common
{
    public class CheckStatusContainer
    {
        private List<CheckStatusBase> _checkStatusBaseCollection;

        public CheckStatusContainer()
        {
            _checkStatusBaseCollection = new List<CheckStatusBase>();
            this.StatusResults = new List<ErrorCode>();
        }

        /// <summary>
        /// 存放系統狀態結果, 字串長度大於0為False
        /// </summary>
        public List<ErrorCode> StatusResults { get; set; }

        /// <summary>
        /// 加入狀態確認項目
        /// </summary>
        /// <param name="checkStatusBase"></param>
        public void Add(CheckStatusBase checkStatusBase)
        {
            _checkStatusBaseCollection.Add(checkStatusBase);
        }

        /// <summary>
        /// 確認狀態
        /// </summary>
        public void DoCheck()
        {
            foreach (CheckStatusBase item in _checkStatusBaseCollection)
            {
                item.DoCheck();
            }
        }
    }
}

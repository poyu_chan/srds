﻿using AOISystem.Utilities;
using System.ComponentModel;

namespace MotionSystem.Project.Common
{
    public class WorkData
    {
        public WorkData()
        {
            Initialize();
        }

        public void Initialize()
        {
            MachineStatus = MachineStatus.Down;
            OperationStatus = OperationStatus.Initialize;
            IsModulesInitialized = false;
            IsAllHomeCompleted = false;
            IsLoadRecipe = false;
            IsRecipeReadyAOI1 = false;
            IsNeedHome = true;
        }

        /// <summary> 機台狀態 </summary> 
        [Category("Flow Status"), Description("Machine Status")]
        public MachineStatus MachineStatus { get; set; }

        /// <summary> 操作狀態 </summary> 
        [Category("Machine Status"), Description("Operation Status")]
        public OperationStatus OperationStatus { get; set; }

        /// <summary> Modules是否初始化完畢 </summary> 
        [Category("Machine Status"), Description("Modules Initialized Status")]
        public bool IsModulesInitialized { get; set; }

        /// <summary> 是否正在執行系統命令 </summary> 
        [Category("Machine Status"), Description("Is System Commanding")]
        public bool IsSystemCommanding { get; set; }

        /// <summary> 全機復歸是否完成 </summary> 
        [Category("Machine Status"), Description("All Home Completed")]
        public bool IsAllHomeCompleted
        {
            get { return this.IsAOIHomeCompleted; }
            set 
            {
                this.IsAOIHomeCompleted = value;
            }
        }

        /// <summary>是否需要執行復歸動作</summary>
        [Category("Machine Status"), Description("Loader Home Completed")]
        public bool IsNeedHome { get; set; }

        /// <summary> AOI復歸是否完成 </summary> 
        [Category("Machine Status"), Description("AOI Home Completed")]
        public bool IsAOIHomeCompleted { get; set; }

        /// <summary> 機台是否暫停流程執行中 </summary> 
        [Category("Machine Status"), Description("Pause Flow Is Running")]
        public bool IsPauseFlowRunning { get; set; }

        /// <summary> 機台是否停止流程執行中 </summary> 
        [Category("Machine Status"), Description("Stop Flow Is Running")]
        public bool IsStopFlowRunning { get; set; }

        /// <summary> 機台是否載入Recipe </summary> 
        [Category("Machine Status"), Description("機台是否載入Recipe")]
        public bool IsLoadRecipe { get; set; }

        /// <summary> AOI1 是否載入Recipe </summary> 
        [Category("Subsystem Status"), Description("AOI1 是否載入Recipe")]
        public bool IsRecipeReadyAOI1 { get; set; }
    }
}

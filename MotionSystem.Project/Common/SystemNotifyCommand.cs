﻿using AOISystem.Utilities;
using AOISystem.Utilities.Common;
using System.Collections.Generic;

namespace MotionSystem.Project.Common
{
    public class SystemNotifyCommand
    {
        public SystemNotifyCommand()
        {
        }

        public SystemNotifyCommand(StopMode stopMode, string message, List<ErrorCode> errorCodes, int ringtoneContinueTime = -1, RingtoneStatus ringtoneStatus = RingtoneStatus.Ringtone1)
        {
            this.StopMode = stopMode;
            this.Message = message;
            this.ErrorCodes = errorCodes;
            this.RingtoneContinueTime = ringtoneContinueTime;
            this.RingtoneStatus = ringtoneStatus;
        }

        public SystemNotifyCommand(StopMode stopMode, string message, ErrorCode errorCode, int ringtoneContinueTime = -1, RingtoneStatus ringtoneStatus = RingtoneStatus.Ringtone1)
            : this(stopMode, message, new List<ErrorCode>() { errorCode }, ringtoneContinueTime, ringtoneStatus)
        {
        }

        public SystemNotifyCommand(StopMode stopMode, string message, int ringtoneContinueTime = -1, RingtoneStatus ringtoneStatus = RingtoneStatus.Ringtone1)
        {
            this.StopMode = stopMode;
            this.Message = message;
            this.ErrorCodes = null;
            this.RingtoneContinueTime = ringtoneContinueTime;
            this.RingtoneStatus = ringtoneStatus;
        }

        public SystemNotifyCommand(StopMode stopMode, List<ErrorCode> errorCodes, int ringtoneContinueTime = -1, RingtoneStatus ringtoneStatus = RingtoneStatus.Ringtone1)
            : this(stopMode, string.Empty, errorCodes, ringtoneContinueTime, ringtoneStatus)
        {
        }

        public SystemNotifyCommand(StopMode stopMode, ErrorCode errorCode, int ringtoneContinueTime = -1, RingtoneStatus ringtoneStatus = RingtoneStatus.Ringtone1)
            : this(stopMode, null, errorCode, ringtoneContinueTime, ringtoneStatus)
        {
        }

        public SystemNotifyCommand(string message, int ringtoneContinueTime = -1, RingtoneStatus ringtoneStatus = RingtoneStatus.Ringtone1)
            : this(StopMode.None, message, ringtoneContinueTime, ringtoneStatus)
        {
        }

        public SystemNotifyCommand(List<ErrorCode> errorCodes, int ringtoneContinueTime = -1, RingtoneStatus ringtoneStatus = RingtoneStatus.Ringtone1)
            : this(StopMode.None, string.Empty, errorCodes, ringtoneContinueTime, ringtoneStatus)
        {
        }

        public SystemNotifyCommand(ErrorCode errorCode, int ringtoneContinueTime = -1, RingtoneStatus ringtoneStatus = RingtoneStatus.Ringtone1)
            : this(new List<ErrorCode>() { errorCode }, ringtoneContinueTime, ringtoneStatus)
        {
        }

        public OperationStatus OperationStatus { get; set; }

        public StopMode StopMode { get; set; }

        public string Message { get; set; }

        public List<ErrorCode> ErrorCodes { get; set; }

        public int RingtoneContinueTime { get; set; }

        public RingtoneStatus RingtoneStatus { get; set; }
    }
}

﻿using AOISystem.Utilities;
using AOISystem.Utilities.Common;
using AOISystem.Utilities.Flow;
using AOISystem.Utilities.Logging;
using AOISystem.Utilities.Modules;
using AOISystem.Utilities.Modules.Syntek;
using AOISystem.Utilities.Modules.Syntek.EtherCAT.MasterCard;
using AOISystem.Utilities.Modules.Syntek.EtherCAT.SlaveModules.IO;
using AOISystem.Utilities.Modules.Syntek.EtherCAT.SlaveModules.Motion;
using AOISystem.Utilities.Resources;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Windows.Forms;

namespace MotionSystem.Project.Common
{
    public sealed class Modules
    {
        private static Modules instance = null;
        private Dictionary<string, double> commands = new Dictionary<string, double>();
        private Dictionary<string, double> encoders = new Dictionary<string, double>();

        private Modules()
        {
            if (ProcessInfo.IsDesignMode())
            {
                return;
            }
#if !TEST
            InitializeModules();

            RegisterErrorRaisedEvent();

            if (AOI.WorkData.IsModulesInitialized)
            {
                AllMotorCommandEncoderRecord();

                AllMotorResetAlarm();

                AllMotorMoveMode(6);

                AllMotorServoOn();

                AllMotorCommandEncoderRecord2();

                //v1.0.6.37 將狀態掃描加入流程控制
                FlowBase modulesScanStatusFlow = new FlowBase("ModulesScanStatusFlow", ScanStatus);
                ModulesFactory.FlowControlHelper.GetFlowControl("MachineOperationFlowControl").AddFlowBase(modulesScanStatusFlow);
                modulesScanStatusFlow.Start();
            }
            else
            {
                MessageBox.Show(new ErrorCode(1000, "系統模組初始化異常").ToString());
            }
#endif
        }

        #region - Module Instances -

        /// <summary>AOI CV1</summary>
        public CEtherCATMotion CV1 { get; set; }

        /// <summary>數位輸入 #1</summary>
        public CEtherCATDI6022 DI1 { get; set; }

        /// <summary>數位輸出 #1</summary>
        public CEtherCATDO7062 DO1 { get; set; }

        #endregion - Module Instances -

        public static Modules GetInstance()
        {
            if (instance == null)
            {
                instance = new Modules();
            }
            return instance;
        }

        private void InitializeModules()
        {
            try
            {
                this.CV1 = new CEtherCATMotion(SystemDefine.MODULES_DATA_FOLDER_PATH, "CV1");
                this.DI1 = new CEtherCATDI6022(SystemDefine.MODULES_DATA_FOLDER_PATH, "DI1");
                this.DO1 = new CEtherCATDO7062(SystemDefine.MODULES_DATA_FOLDER_PATH, "DO1");

                AOI.WorkData.IsModulesInitialized = true;
            }
            catch (Exception ex)
            {
                AOI.WorkData.IsModulesInitialized = false;
                EtherCATInitializationForm.GetInstance().CloseForm();
                MessageBox.Show(ex.Message, ResourceHelper.Language.GetString("SystemHint"), MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void RegisterErrorRaisedEvent()
        {
            if (AOI.WorkData.IsModulesInitialized)
            {
                PropertyInfo[] propertyInfos = typeof(Modules).GetProperties();
                foreach (var propertyInfo in propertyInfos)
                {
                    object instance = propertyInfo.GetValue(this, null);
                    if (instance is ModulesBase)
                    {
                        ModulesBase modulesBase = instance as ModulesBase;
                        modulesBase.ErrorRaised += new ModulesBase.ErrorRaisedEventHandler(modulesBase_ErrorRaised);
                    }
                }
            }
        }

        private void modulesBase_ErrorRaised(object sender, int errorCode, string errorMsg)
        {
            ModulesBase modulesBase = (ModulesBase)sender;
            string message = string.Format("Device : {0} ErrorRaised : {1}", modulesBase.DeviceName, errorMsg);
            LogHelper.Exception(message);
            MonitorLogger.Post(message);
        }

        private void ScanStatus(FlowVar flowVar)
            {
                if (AOI.WorkData.MachineStatus != MachineStatus.Down)
                {
                    List<ErrorCode> statusResults;
                    if (!SystemStatusCheck(out statusResults))
                    {
                        AOI.WorkData.MachineStatus = MachineStatus.Down;
                        AOI.SystemNotify(OperationStatus.Alarm, new SystemNotifyCommand(StopMode.Pause, statusResults));
                    }
                }
        }

        public bool SystemStatusCheck()
        {
            List<ErrorCode> statusResults;
            return SystemStatusCheck(out statusResults);
        }

        public bool SystemStatusCheck(out List<ErrorCode> statusResults)
        {
            //v1.0.6.23 新增持續偵測疊片檢知訊號

            return CheckStatus.DoCheck(
                        out statusResults,
                        AOI.SystemStatus.CheckEMO,
                        AOI.SystemStatus.CheckSafeDoor,
                        AOI.SystemStatus.CheckMotionAlarm,
                        AOI.SystemStatus.CheckAirDetect);
        }

        public void AllMotorStop(StopType type, bool isStopTask = true)
        {
            if (AOI.WorkData.IsModulesInitialized)
            {
                PropertyInfo[] propertyInfos = typeof(Modules).GetProperties();
                foreach (var propertyInfo in propertyInfos)
                {
                    object instance = propertyInfo.GetValue(this, null);
                    if (instance is CEtherCATMotion)
                    {
                        CEtherCATMotion iMotion = instance as CEtherCATMotion;
                        iMotion.Stop(type, isStopTask);
                    }
                }
            }
        }

        public void AllMotorServoOn()
        {
            PropertyInfo[] propertyInfos = typeof(Modules).GetProperties();
            foreach (var propertyInfo in propertyInfos)
            {
                object instance = propertyInfo.GetValue(this, null);
                if (instance != null && instance is CEtherCATMotion)
                {
                    CEtherCATMotion iMotion = instance as CEtherCATMotion;
                    iMotion.ServoOn(CmdStatus.ON);
                }
            }
        }

        public void AllMotorServoOFF()
        {
            PropertyInfo[] propertyInfos = typeof(Modules).GetProperties();
            foreach (var propertyInfo in propertyInfos)
            {
                object instance = propertyInfo.GetValue(this, null);
                if (instance != null && instance is CEtherCATMotion)
                {
                    CEtherCATMotion iMotion = instance as CEtherCATMotion;
                    iMotion.ServoOn(CmdStatus.OFF);
                }
            }
        }

        public void AllMotorMoveMode(ushort opMode)
        {
            PropertyInfo[] propertyInfos = typeof(Modules).GetProperties();
            foreach (var propertyInfo in propertyInfos)
            {
                object instance = propertyInfo.GetValue(this, null);
                if (instance != null && instance is CEtherCATMotion)
                {
                    CEtherCATMotion iMotion = instance as CEtherCATMotion;
                    iMotion.SetMoveMode(opMode);
                }
            }
        }

        public void AllMotorResetAlarm()
        {
            if (AOI.WorkData.IsModulesInitialized)
            {
                Stopwatch sw = Stopwatch.StartNew();
                PropertyInfo[] propertyInfos = typeof(Modules).GetProperties();
                foreach (var propertyInfo in propertyInfos)
                {
                    object instance = propertyInfo.GetValue(this, null);
                    if (instance is CEtherCATMotion)
                    {
                        sw.Restart();
                        CEtherCATMotion iMotion = instance as CEtherCATMotion;
                        if (iMotion.IsAlarm)
                        {
                            iMotion.ResetAlarm();
                        }
                        sw.Stop();
                        Console.WriteLine("ResetAlarm {0} {1}", iMotion.DeviceName, sw.ElapsedMilliseconds);
                    }
                }
            }
        }

        public void AllMotorCommandEncoderRecord()
        {
            PropertyInfo[] propertyInfos = typeof(Modules).GetProperties();
            foreach (var propertyInfo in propertyInfos)
            {
                object instance = propertyInfo.GetValue(this, null);
                if (instance != null && instance is CEtherCATMotion)
                {
                    CEtherCATMotion iMotion = instance as CEtherCATMotion;
                    commands[iMotion.DeviceName] = iMotion.Position;
                    encoders[iMotion.DeviceName] = iMotion.Encoder;
                }
            }
        }

        public void AllMotorCommandEncoderRecord2()
        {
            PropertyInfo[] propertyInfos = typeof(Modules).GetProperties();
            foreach (var propertyInfo in propertyInfos)
            {
                object instance = propertyInfo.GetValue(this, null);
                if (instance != null && instance is CEtherCATMotion)
                {
                    CEtherCATMotion iMotion = instance as CEtherCATMotion;
                    Console.WriteLine("{0} Encoder {1}", iMotion.DeviceName, iMotion.Encoder - encoders[iMotion.DeviceName]);
                }
            }
        }

        public void AllCnvMotorStop()
        {
            AOI.Modules.CV1.Stop(StopType.Emergency);
        }

        public CEtherCATMotion GetMotor(string propertyName)
        {
            return (CEtherCATMotion)this.GetType().GetProperty(propertyName).GetValue(this, null);
        }

        public void CnvMotorMove(RotationDirection rotationDirection)
        {
            AOI.Modules.CV1.ContinuousMove(rotationDirection, 200);
        }

        public void AllCnvsMotorDoHome()
        {
            AOI.Modules.CV1.Home();
        }

        public bool AllCnvsMotorIsHome()
        {
            bool isHome = true;
            isHome &= AOI.Modules.CV1.IsHome;
            return isHome;
        }

        public void AllCnvsMotorResetPosition()
        {
            AOI.Modules.CV1.ResetPos();
        }

        public void SetMotionStrEndVelZero()
        {
            //v1.0.6.27 將馬達加減速設置為0
            PropertyInfo[] propertyInfos = typeof(Modules).GetProperties();
            foreach (var propertyInfo in propertyInfos)
            {
                object instance = propertyInfo.GetValue(this, null);
                if (instance != null && instance is CEtherCATMotion)
                {
                    CEtherCATMotion iMotion = instance as CEtherCATMotion;
                    if (iMotion.MotionModel == MotionModel.Panasonic_MINAS_A5B)
                    {
                        iMotion.AxisPara.StrVel = 0;
                        iMotion.AxisPara.EndVel = 0;
                        iMotion.AxisPara.Save();   
                    }
                }
            }
        }
    }
}

﻿using AOISystem.Utilities;
using AOISystem.Utilities.Common;
using System;
using System.ComponentModel;
using System.IO;

namespace MotionSystem.Project.Common
{
    [Serializable]
    public class SystemData : ParameterXML<SystemData>
    {
        public SystemData()
            : base(SystemDefine.SYSTEM_DATA_FOLDER_PATH)
        {
            this.DefaultRecipeID = "DefaultRecipeID";
            this.DataSavePath = @"D:\Default_Data";
            this.DataSaveDays = 30;
            this.LogSaveDays = 90;
            this.AvailableRAMMinFreeSpace = 0;
            this.AvailableDISKMinFreeSpace = 0;
            this.ContinueNGJudgeCountMax = 0;
            CheckPath(SystemDefine.SYSTEM_DATA_FOLDER_PATH);
            this.MotionErrorCodeDelayTime = 1000;
        }

        [Browsable(true), Category("System Setting"), Description("DefaultRecipeID")]
        public string DefaultRecipeID { get; set; }

        [Browsable(true), Category("System Setting"), Description("DataSavePath")]
        public string DataSavePath { get; set; }

        [Browsable(true), Category("System Setting"), Description("DataSaveDays")]
        public int DataSaveDays { get; set; }

        [Browsable(true), Category("System Setting"), Description("LogSaveDays")]
        public int LogSaveDays { get; set; }

        [Browsable(true), Category("System Setting"), Description("AlarmRingtoneStatus")]
        public RingtoneStatus AlarmRingtoneStatus { get; set; }

        [Browsable(true), Category("System Setting"), Description("WarningRingtoneStatus")]
        public RingtoneStatus WarningRingtoneStatus { get; set; }

        /// <summary> RAM最小可用空間 </summary>
        [Browsable(true), Category("System Operate"), Description("Available RAM Min Free Space(GB)")]
        public double AvailableRAMMinFreeSpace { get; set; }

        /// <summary> DISK最小可用空間 </summary>
        [Browsable(true), Category("System Operate"), Description("Available DISK Min Free Space(GB)")]
        public double AvailableDISKMinFreeSpace { get; set; }

        /// <summary>連續NG判定確認</summary>
        [Browsable(true), Category("System Operate"), Description("連續NG判定確認")]
        public int ContinueNGJudgeCountMax { get; set; }

        /// <summary>廠別名稱</summary>
        [Browsable(true), Category("System Setting"), Description("廠別名稱")]
        public string FabName { get; set; }

        /// <summary>機台名稱</summary>
        [Browsable(true), Category("System Setting"), Description("機台名稱")]
        public string MachineID { get; set; }

        /// <summary>是否每次手動載入Recipe檔案</summary>
        [Browsable(true), Category("System Setting"), Description("是否每次手動載入Recipe檔案")]
        public bool UseManualSelectRecipe { get; set; }

        /// <summary>馬達ErrorCode取得延遲時間(ms)</summary>
        [Browsable(true), Category("Other Setting"), Description("馬達ErrorCode取得延遲時間(ms)")]
        public int MotionErrorCodeDelayTime { get; set; }

        public double GetPosition(string propertyName)
        {
            return (double)this.GetType().GetProperty(propertyName).GetValue(this, null);
        }

        public bool GetBooleanProperty(string propertyName)
        {
            return (bool)this.GetType().GetProperty(propertyName).GetValue(this, null);
        }
            
        private void CheckPath(string path)
        {
            if (!File.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        }
    }
}

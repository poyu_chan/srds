﻿using AOISystem.Utilities.Common;
using System;
using System.Collections.Generic;

namespace MotionSystem.Project.Common
{
    public class CheckStatus
    {
        public delegate void CheckStatusRaisedEventHandler(List<ErrorCode> statusResults);

        public static event CheckStatusRaisedEventHandler CheckStatusRaised;

        /// <summary>
        /// 確認狀態內容
        /// </summary>
        /// <param name="parameters">狀態參數陣列</param>
        /// <returns>正常狀態為True</returns>
        public static bool DoCheck(params CheckStatusBase[] parameters)
        {
            List<ErrorCode> statusResults;
            return DoCheck(false, out statusResults, parameters);
        }

        /// <summary>
        /// 確認狀態內容
        /// </summary>
        /// <param name="statusResults">狀態結果</param>
        /// <param name="parameters">狀態參數陣列</param>
        /// <returns>正常狀態為True</returns>
        public static bool DoCheck(out List<ErrorCode> statusResults, params CheckStatusBase[] parameters)
        {
            return DoCheck(false, out statusResults, parameters);
        }

        /// <summary>
        /// 確認狀態內容
        /// </summary>
        /// <param name="isLoggingStatusResult">是否引發狀態結果紀錄事件</param>
        /// <param name="parameters">狀態參數陣列</param>
        /// <returns>正常狀態為True</returns>
        public static bool DoCheck(bool isLoggingStatusResult, params CheckStatusBase[] parameters)
        {
            List<ErrorCode> statusResults;
            return DoCheck(isLoggingStatusResult, out statusResults, parameters);
        }

        /// <summary>
        /// 確認狀態內容
        /// </summary>
        /// <param name="isLoggingStatusResult">是否引發狀態結果紀錄事件</param>
        /// <param name="statusResults">狀態結果</param>
        /// <param name="parameters">狀態參數陣列</param>
        /// <returns>正常狀態為True</returns>
        private static bool DoCheck(bool isLoggingStatusResult, out List<ErrorCode> statusResults, params CheckStatusBase[] parameters)
        {
            if (parameters.Length == 0)
            {
                throw new ArgumentNullException("CheckStatusBase Parameter Null");
            }
            CheckStatusContainer checkStatusContainer = new CheckStatusContainer();
            foreach (CheckStatusBase parameter in parameters)
            {
                parameter.BlindingContainer(checkStatusContainer);
                checkStatusContainer.Add(parameter);
            }
            checkStatusContainer.DoCheck();
            return CheckResult(isLoggingStatusResult, checkStatusContainer, out statusResults);
        }

        private static bool CheckResult(bool isLoggingStatusResult, CheckStatusContainer checkStatusContainer, out List<ErrorCode> statusResults)
        {
            statusResults = checkStatusContainer.StatusResults;
            bool checkResult = statusResults.Count == 0;
            if (!checkResult)
            {
                if (isLoggingStatusResult)
                {
                    if (CheckStatusRaised != null)
                    {
                        CheckStatusRaised(statusResults);
                    }
                }
            }
            return checkResult;
        }
    }
}

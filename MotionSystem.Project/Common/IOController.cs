﻿using AOISystem.Utilities;
using AOISystem.Utilities.Component;
using AOISystem.Utilities.Forms;
using AOISystem.Utilities.Modules.Syntek.EtherCAT.SlaveModules.IO;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;

namespace MotionSystem.Project.Common
{
    public enum SignalTowerStatus { None, Green, Yellow, Red }

    public enum RingtoneStatus { None, Ringtone1, Ringtone2, Ringtone3, Ringtone4 }

    // 縮=OFF, 伸=ON
    public enum CylinderStatus { OFF = 0, ON = 1 }

    // 破真空=OFF, 吸真空=ON
    public enum VacuumStatus { OFF = 0, ON = 1 }

    public enum MotorStatus { ForwardRotation, ReversalRotation, AlarmReset, Stop }

    // 吹氣啟動=ON, 吹氣關閉=OFF    //160822.1030 v1.0.2.9 
    public enum BlowStatus { OFF = 0, ON = 1 }

    public class IOController
    {
        #region feild

        private CEtherCATDI6022 _di1;
        private CEtherCATDI6022 _di2;
        private CEtherCATDO7062 _do1;
        private CEtherCATDO7062 _do2;

        private LedRectangle ledRun = new LedRectangle();
        private LedRectangle ledWarning = new LedRectangle();
        private LedRectangle ledDown = new LedRectangle();

        private bool _isMute = false;

        #endregion feild

        #region construct

        public IOController(CEtherCATDI6022 di1, CEtherCATDO7062 do1)
        {
            if (ProcessInfo.IsDesignMode())
            {
                return;
            }
            _di1 = di1;
            _do1 = do1;
        }

        #endregion construct

        #region DI Properties

        /// <summary>X0000 緊急停止</summary>
        [Browsable(true), Category("DI"), IONumber("X0000"), Description("緊急停止")]
        public bool DIEmergencyStopSensor { get { return _di1.GetInput(0); } }

        /// <summary>X0001 空壓偵測</summary>
        [Browsable(true), Category("DI"), IONumber("X0001"), Description("空壓偵測")]
        public bool DICDASourcePressureSensor { get { return _di1.GetInput(1); } }

        /// <summary>X0002 切片機1定位檢知</summary>
        [Browsable(true), Category("DI"), IONumber("X0002"), Description("切片機1定位檢知")]
        public bool DIRobotToSR1InPositionSensor { get { return _di1.GetInput(2); } }

        /// <summary>X0003 切片機2定位檢知</summary>
        [Browsable(true), Category("DI"), IONumber("X0003"), Description("切片機2定位檢知")]
        public bool DIRobotToSR2InPositionSensor { get { return _di1.GetInput(3); } }

        /// <summary>X0004 儲存站定位檢知</summary>
        [Browsable(true), Category("DI"), IONumber("X0004"), Description("儲存站定位檢知")]
        public bool DIRobotToStockerInPositionSensor { get { return _di1.GetInput(4); } }

        /// <summary>X0005 手臂在席檢知</summary>
        [Browsable(true), Category("DI"), IONumber("X0005"), Description("手臂在席檢知")]
        public bool DIRobotArmInPositionSensor { get { return _di1.GetInput(5); } }

        /// <summary>X0006 手臂夾持氣缸夾持檢知</summary>
        [Browsable(true), Category("DI"), IONumber("X0006"), Description("手臂夾持氣缸夾持檢知")]
        public bool DIRobotArmGripperCylinderONSensor { get { return _di1.GetInput(6); } }

        /// <summary>X0007 手臂夾持氣缸放開檢知</summary>
        [Browsable(true), Category("DI"), IONumber("X0007"), Description("手臂夾持氣缸放開檢知")]
        public bool DIRobotArmGripperCylinderOFFSensor { get { return _di1.GetInput(7); } }

        /// <summary>X0008 手臂站盛水桶滿水位檢知</summary>
        [Browsable(true), Category("DI"), IONumber("X0008"), Description("手臂站盛水桶滿水位檢知")]
        public bool DIRobotStaionWaterTankFullWaterLevelSensor { get { return _di1.GetInput(8); } }

        /// <summary>X0016 儲存站在席檢知</summary>
        [Browsable(true), Category("DI"), IONumber("X0016"), Description("儲存站在席檢知")]
        public bool DIStockerInPositionSensor { get { return _di2.GetInput(0); } }

        /// <summary>X0017 儲存站搬移檢知</summary>
        [Browsable(true), Category("DI"), IONumber("X0017"), Description("儲存站搬移檢知")]
        public bool DIStockerTransferringSensor { get { return _di2.GetInput(1); } }

        /// <summary>X0018 儲存站入料按鈕</summary>
        [Browsable(true), Category("DI"), IONumber("X0018"), Description("儲存站入料按鈕")]
        public bool DIStockerLoadInButton { get { return _di2.GetInput(2); } }

        /// <summary>X0019 儲存站前門氣缸開門檢知</summary>
        [Browsable(true), Category("DI"), IONumber("X0019"), Description("儲存站前門氣缸開門檢知")]
        public bool DIStockerFrontDoorCylinderONSensor { get { return _di2.GetInput(3); } }

        /// <summary>X0020 儲存站前門氣缸關門檢知</summary>
        [Browsable(true), Category("DI"), IONumber("X0020"), Description("儲存站前門氣缸關門檢知")]
        public bool DIStockerFrontDoorCylinderOFFSensor { get { return _di2.GetInput(4); } }

        /// <summary>X0021 儲存站後門氣缸開門檢知</summary>
        [Browsable(true), Category("DI"), IONumber("X0021"), Description("儲存站後門氣缸開門檢知")]
        public bool DIStockerRearDoorCylinderONSensor { get { return _di2.GetInput(5); } }

        /// <summary>X0022 儲存站後門氣缸關門檢知</summary>
        [Browsable(true), Category("DI"), IONumber("X0022"), Description("儲存站後門氣缸關門檢知")]
        public bool DIStockerRearDoorCylinderOFFSensor { get { return _di2.GetInput(6); } }

        /// <summary>X0023 儲存站盛水桶上液位檢知</summary>
        [Browsable(true), Category("DI"), IONumber("X0023"), Description("儲存站盛水桶上液位檢知")]
        public bool DIStockerStaionWaterTankFullWaterLevelSensor { get { return _di2.GetInput(7); } }

        /// <summary>X0024 儲存站盛水桶下液位檢知</summary>
        [Browsable(true), Category("DI"), IONumber("X0024"), Description("儲存站盛水桶下液位檢知")]
        public bool DIStockerStaionWaterTankLowWaterLevelSensor { get { return _di2.GetInput(8); } }

        #endregion DI Properties

        #region DO Properties

        private bool _doSignalTowerGreen;
        /// <summary>Y0000 三色燈 綠</summary>
        [Browsable(true), Category("DO"), IONumber("Y0000"), Description("三色燈 綠")]
        public bool DOSignalTowerGreen { set { _do1.SetOutput(0, value); _doSignalTowerGreen = value; } get { return _doSignalTowerGreen; } }

        private bool _doSignalTowerYellow;
        /// <summary>Y0001 三色燈 黃</summary>
        [Browsable(true), Category("DO"), IONumber("Y0001"), Description("三色燈 黃")]
        public bool DOSignalTowerYellow { set { _do1.SetOutput(1, value); _doSignalTowerYellow = value; } get { return _doSignalTowerYellow; } }

        private bool _doSignalTowerRed;
        /// <summary>Y0002 三色燈 紅</summary>
        [Browsable(true), Category("DO"), IONumber("Y0002"), Description("三色燈 紅")]
        public bool DOSignalTowerRed { set { _do1.SetOutput(2, value); _doSignalTowerRed = value; } get { return _doSignalTowerRed; } }

        private bool _doSignalPhone1;
        /// <summary>Y0003 音樂警報 1</summary>
        [Browsable(true), Category("DO"), IONumber("Y0003"), Description("音樂警報 1")]
        public bool DOSignalPhone1 { set { _do1.SetOutput(3, value); _doSignalPhone1 = value; } get { return _doSignalPhone1; } }

        private bool _doSignalPhone2;
        /// <summary>Y0004 音樂警報 2</summary>
        [Browsable(true), Category("DO"), IONumber("Y0004"), Description("音樂警報 2")]
        public bool DOSignalPhone2 { set { _do1.SetOutput(4, value); _doSignalPhone2 = value; } get { return _doSignalPhone2; } }

        private bool _doSignalPhone3;
        /// <summary>Y0005 音樂警報 3</summary>
        [Browsable(true), Category("DO"), IONumber("Y0005"), Description("音樂警報 3")]
        public bool DOSignalPhone3 { set { _do1.SetOutput(5, value); _doSignalPhone3 = value; } get { return _doSignalPhone3; } }

        private bool _doSignalPhone4;
        /// <summary>Y0006 音樂警報 4</summary>
        [Browsable(true), Category("DO"), IONumber("Y0006"), Description("音樂警報 4")]
        public bool DOSignalPhone4 { set { _do1.SetOutput(6, value); _doSignalPhone4 = value; } get { return _doSignalPhone4; } }

        private bool _doRobotArmGripperONSolenoidValve;
        /// <summary>Y0017 手臂夾爪電磁閥啟動</summary>
        [Browsable(true), Category("DO"), IONumber("Y0017"), Description("手臂夾爪電磁閥啟動")]
        public bool DORobotArmGripperONSolenoidValve { set { _do2.SetOutput(1, value); _doRobotArmGripperONSolenoidValve = value; } get { return _doRobotArmGripperONSolenoidValve; } }

        private bool _doRobotArmGripperOFFSolenoidValve;
        /// <summary>Y0018 手臂夾爪電磁閥關閉</summary>
        [Browsable(true), Category("DO"), IONumber("Y0018"), Description("手臂夾爪電磁閥關閉")]
        public bool DORobotArmGripperOFFSolenoidValve { set { _do2.SetOutput(2, value); _doRobotArmGripperOFFSolenoidValve = value; } get { return _doRobotArmGripperOFFSolenoidValve; } }

        private bool _doRobotArmSpareSolenoidValve;
        /// <summary>Y0020 手臂夾爪備用電磁閥</summary>
        [Browsable(true), Category("DO"), IONumber("Y0020"), Description("手臂夾爪備用電磁閥")]
        public bool DORobotArmSpareSolenoidValve { set { _do2.SetOutput(4, value); _doRobotArmSpareSolenoidValve = value; } get { return _doRobotArmSpareSolenoidValve; } }

        private bool _doRobotStaionTankDrainSolenoidValve;
        /// <summary>Y0021 手臂站盛水桶排水電磁閥</summary>
        [Browsable(true), Category("DO"), IONumber("Y0021"), Description("手臂站盛水桶排水電磁閥")]
        public bool DORobotStaionTankDrainSolenoidValve { set { _do2.SetOutput(5, value); _doRobotStaionTankDrainSolenoidValve = value; } get { return _doRobotStaionTankDrainSolenoidValve; } }

        private bool _doStockerFrontDoorONSolenoidValve;
        /// <summary>Y0024 儲存站前門開門電磁閥啟動</summary>
        [Browsable(true), Category("DO"), IONumber("Y0024"), Description("儲存站前門開門電磁閥啟動")]
        public bool DOStockerFrontDoorONSolenoidValve { set { _do2.SetOutput(8, value); _doStockerFrontDoorONSolenoidValve = value; } get { return _doStockerFrontDoorONSolenoidValve; } }

        private bool _doStockerFrontDoorOFFSolenoidValve;
        /// <summary>Y0025 儲存站前門開門電磁閥關閉</summary>
        [Browsable(true), Category("DO"), IONumber("Y0025"), Description("儲存站前門開門電磁閥關閉")]
        public bool DOStockerFrontDoorOFFSolenoidValve { set { _do2.SetOutput(9, value); _doStockerFrontDoorOFFSolenoidValve = value; } get { return _doStockerFrontDoorOFFSolenoidValve; } }

        private bool _doStockerRearDoorONSolenoidValve;
        /// <summary>Y0026 儲存站後門開門電磁閥啟動</summary>
        [Browsable(true), Category("DO"), IONumber("Y0026"), Description("儲存站後門開門電磁閥啟動")]
        public bool DOStockerRearDoorONSolenoidValve { set { _do2.SetOutput(10, value); _doStockerRearDoorONSolenoidValve = value; } get { return _doStockerRearDoorONSolenoidValve; } }

        private bool _doStockerRearDoorOFFSolenoidValve;
        /// <summary>Y0027 儲存站後門開門電磁閥關閉</summary>
        [Browsable(true), Category("DO"), IONumber("Y0027"), Description("儲存站後門開門電磁閥關閉")]
        public bool DOStockerRearDoorOFFSolenoidValve { set { _do2.SetOutput(11, value); _doStockerRearDoorOFFSolenoidValve = value; } get { return _doStockerRearDoorOFFSolenoidValve; } }

        private bool _doWaterSpraySolenoidValve;
        /// <summary>Y0028 噴水控制電磁閥</summary>
        [Browsable(true), Category("DO"), IONumber("Y0028"), Description("噴水控制電磁閥")]
        public bool DOWaterSpraySolenoidValve { set { _do2.SetOutput(12, value); _doWaterSpraySolenoidValve = value; } get { return _doWaterSpraySolenoidValve; } }

        private bool _doStockerLoadInButtonLight;
        /// <summary>Y0029 儲存站入料按鈕燈</summary>
        [Browsable(true), Category("DO"), IONumber("Y0029"), Description("儲存站入料按鈕燈")]
        public bool DOStockerLoadInButtonLight { set { _do2.SetOutput(13, value); _doStockerLoadInButtonLight = value; } get { return _doStockerLoadInButtonLight; } }

        #endregion DO Properties

        #region private method

        #endregion private method

        #region public method

        /// <summary>
        /// 刷新機台操作燈號狀態
        /// </summary>
        /// <param name="operationStatus"></param>
        public void RefreshMachineLampStatus()
        {
            //this.DOStartLamp = AOI.WorkData.MachineStatus == MachineStatus.Idle;
            //this.DOStopLamp = AOI.WorkData.MachineStatus == MachineStatus.Run;
            //this.DOAlarmReset = AOI.WorkData.MachineStatus == MachineStatus.Down;
        }

        /// <summary>
        /// 綁定主視窗訊號燈
        /// </summary>
        /// <param name="ledRun"></param>
        /// <param name="ledWarning"></param>
        /// <param name="ledDown"></param>
        public void RegisterSystemLedStatus(LedRectangle ledRun, LedRectangle ledWarning, LedRectangle ledDown)
        {
            this.ledRun = ledRun;
            this.ledWarning = ledWarning;
            this.ledDown = ledDown;
        }

        /// <summary>
        /// 設置三色燈及鈴聲狀態
        /// </summary>
        /// <param name="signalTowerStatus"></param>
        /// <param name="ringtoneStatus"></param>
        /// <param name="waitSecondsToCloseOff">等待幾秒後自動關閉</param>
        public void SetSignalTowerAndRingtone(SignalTowerStatus signalTowerStatus, RingtoneStatus ringtoneStatus, int waitSecondsToCloseOff = -1)
        {
            if (!AOI.WorkData.IsModulesInitialized)
            {
                return;
            }
            SetSignalTower(signalTowerStatus);
            //RingtoneStatus tempRingtoneStatus = GetRingtone();
            SetRingtone(ringtoneStatus);
            if (waitSecondsToCloseOff != -1)
            {
                Task.Factory.StartNew(() =>
                {
                    SpinWait.SpinUntil(() => false, waitSecondsToCloseOff * 1000);
                    SetRingtone(RingtoneStatus.None);
                });
            }
        }

        /// <summary>
        /// 設置三色燈狀態
        /// </summary>
        /// <param name="signalTowerStatus"></param>
        public void SetSignalTower(SignalTowerStatus signalTowerStatus)
        {
            if (!AOI.WorkData.IsModulesInitialized)
            {
                return;
            }
            switch (signalTowerStatus)
            {
                case SignalTowerStatus.None:
                    this.DOSignalTowerGreen = false;
                    this.DOSignalTowerYellow = false;
                    this.DOSignalTowerRed = false;
                    break;
                case SignalTowerStatus.Green:
                    this.DOSignalTowerGreen = true;
                    this.DOSignalTowerYellow = false;
                    this.DOSignalTowerRed = false;
                    break;
                case SignalTowerStatus.Yellow:
                    this.DOSignalTowerGreen = false;
                    this.DOSignalTowerYellow = true;
                    this.DOSignalTowerRed = false;
                    break;
                case SignalTowerStatus.Red:
                    this.DOSignalTowerGreen = false;
                    this.DOSignalTowerYellow = false;
                    this.DOSignalTowerRed = true;
                    break;
            }
        }

        /// <summary>
        /// 設置鈴聲
        /// </summary>
        /// <param name="ringtoneStatus"></param>
        public void SetRingtone(bool isMute)
        {
            if (!AOI.WorkData.IsModulesInitialized)
            {
                return;
            }
            _isMute = isMute;
            this.DOSignalPhone1 = _doSignalPhone1;
            this.DOSignalPhone2 = _doSignalPhone2;
            this.DOSignalPhone3 = _doSignalPhone3;
            this.DOSignalPhone4 = _doSignalPhone4;
        }

        /// <summary>
        /// 設置鈴聲
        /// </summary>
        /// <param name="ringtoneStatus"></param>
        public void SetRingtone(RingtoneStatus ringtoneStatus)
        {
            switch (ringtoneStatus)
            {
                case RingtoneStatus.None:
                    this.DOSignalPhone1 = false;
                    this.DOSignalPhone2 = false;
                    this.DOSignalPhone3 = false;
                    this.DOSignalPhone4 = false;
                    break;
                case RingtoneStatus.Ringtone1:
                    this.DOSignalPhone1 = true;
                    this.DOSignalPhone2 = false;
                    this.DOSignalPhone3 = false;
                    this.DOSignalPhone4 = false;
                    break;
                case RingtoneStatus.Ringtone2:
                    this.DOSignalPhone1 = false;
                    this.DOSignalPhone2 = true;
                    this.DOSignalPhone3 = false;
                    this.DOSignalPhone4 = false;
                    break;
                case RingtoneStatus.Ringtone3:
                    this.DOSignalPhone1 = false;
                    this.DOSignalPhone2 = false;
                    this.DOSignalPhone3 = true;
                    this.DOSignalPhone4 = false;
                    break;
                case RingtoneStatus.Ringtone4:
                    this.DOSignalPhone1 = false;
                    this.DOSignalPhone2 = false;
                    this.DOSignalPhone3 = false;
                    this.DOSignalPhone4 = true;
                    break;
            }
        }

        public RingtoneStatus GetRingtone()
        {
            RingtoneStatus ringtoneStatus = RingtoneStatus.None;
            if (this.DOSignalPhone1)
            {
                ringtoneStatus = RingtoneStatus.Ringtone1;
            }
            else if (this.DOSignalPhone2)
            {
                ringtoneStatus = RingtoneStatus.Ringtone2;
            }
            else if (this.DOSignalPhone3)
            {
                ringtoneStatus = RingtoneStatus.Ringtone3;
            }
            else if (this.DOSignalPhone4)
            {
                ringtoneStatus = RingtoneStatus.Ringtone4;
            }
            return ringtoneStatus;
        }

        /// <summary>
        /// 得到IO狀態由屬性名稱
        /// </summary>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public bool GetIO(string propertyName)
        {
            return (bool)this.GetType().GetProperty(propertyName).GetValue(this, null);
        }

        /// <summary>
        /// 設定IO狀態由屬性名稱
        /// </summary>
        /// <param name="propertyName"></param>
        /// <param name="value"></param>
        public void SetIO(string propertyName, bool value)
        {
            this.GetType().GetProperty(propertyName).SetValue(this, value, null);
        }

        #endregion public method
    }
}

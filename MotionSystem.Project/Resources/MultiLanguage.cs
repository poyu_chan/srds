﻿using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace MotionSystem.Project.Resources
{
    public class MultiLanguage
    {
        public MultiLanguage()
        {
        }

        public static void Apply(Form form)
        {
            if (form == null)
                throw new ArgumentNullException("form");

            ComponentResourceManager Manager = null;
            try
            {
                Manager = new ComponentResourceManager(typeof(MultiLanguage));

                foreach (Control Ctrl in form.Controls)
                {
                    ApplyLanguage(Ctrl, Manager);
                }
            }
            finally
            {
                Manager = null;
            }
        }

        private static void ApplyLanguage(Control Ctrl, ComponentResourceManager Manager)
        {
            if (Ctrl is MenuStrip)
            {
                MenuStrip menu = Ctrl as MenuStrip;
                Manager.ApplyResources(Ctrl, Ctrl.Name);

                foreach (ToolStripItem item in menu.Items)
                {
                    ApplyLanguage(item, Manager);
                }
            }
            else
            {
                Manager.ApplyResources(Ctrl, Ctrl.Name);
                foreach (Control item in Ctrl.Controls)
                {
                    if (item is TextBox)
                    {
                    }
                    else
                    {
                        ApplyLanguage(item, Manager);
                    }
                }
            }
        }

        private static void ApplyLanguage(ToolStripItem Ctrl, ComponentResourceManager Manager)
        {
            if (!(Ctrl is System.Windows.Forms.ToolStripSeparator))
            {
                Manager.ApplyResources(Ctrl, Ctrl.Name);
                ToolStripMenuItem menu = Ctrl as ToolStripMenuItem;
                foreach (ToolStripItem item in menu.DropDownItems)
                {
                    ApplyLanguage(item, Manager);
                }
            }
        }
    }
}

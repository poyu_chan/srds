﻿using AOISystem.Utilities;
using AOISystem.Utilities.Common;
using AOISystem.Utilities.MultiLanguage;
using System;
using System.IO;
using System.Security.Permissions;
using System.Threading;
using System.Windows.Forms;

namespace MotionSystem.Project
{
    static class Program
    {
        //1.0.160811.0800 修改程式多開appKey值為"WIS.Project"
        static string appKey = "WIS.Project";

        /// <summary>
        /// 應用程式的主要進入點。
        /// </summary>
        [STAThread]
        // Starts the application. 
        [SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.ControlAppDomain)]
        static void Main()
        {
            using (Mutex m = new Mutex(false, "Global\\" + appKey))
            {
                if (!m.WaitOne(0, false))
                {
                    MessageBox.Show("程式已開啟，請勿重複開啟程式");
                    return;
                }
                //設定資料夾設置為程式根目錄
                //SystemDefine.InitializeApplicationDataFolderPath(Application.StartupPath);
                //1.0.160811.0800 新增參數路徑確認
                if (!Directory.Exists(SystemDefine.APPLICATION_DATA_FOLDER_PATH))
                {
                    MessageBox.Show(string.Format("設定資料夾不存在，請確認 {0} 是否存在", SystemDefine.APPLICATION_DATA_FOLDER_PATH));
                    return;
                }
                if (!Directory.Exists(SystemDefine.SYSTEM_DATA_FOLDER_PATH))
                {
                    MessageBox.Show(string.Format("Systen資料夾不存在，請確認 {0} 是否存在", SystemDefine.SYSTEM_DATA_FOLDER_PATH));
                    return;
                }
                if (!Directory.Exists(SystemDefine.RECIPE_DATA_FOLDER_PATH))
                {
                    MessageBox.Show(string.Format("Recipe資料夾不存在，請確認 {0} 是否存在", SystemDefine.RECIPE_DATA_FOLDER_PATH));
                    return;
                }

                //v1.0.8.4 確認系統時間，避免BIOS時間還原未設定正確時間
                DateTime dtCheck = new DateTime(2018, 1, 1);
                if (dtCheck > DateTime.Now)
                {
                    MessageBox.Show(string.Format("目前系統時間為{0}，請校正後再重新啟動", DateTime.Now));
                    return;
                }

                // Add the event handler for handling UI thread exceptions to the event.
                Application.ThreadException += new ThreadExceptionEventHandler(ExceptionHelper.ExceptionHandler_UIThreadException);

                // Set the unhandled exception mode to force all Windows Forms errors to go through
                // our handler.
                Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);

                // Add the event handler for handling non-UI thread exceptions to the event. 
                AppDomain.CurrentDomain.UnhandledException +=
                    new UnhandledExceptionEventHandler(ExceptionHelper.CurrentDomain_UnhandledException);

                //設定程式當前使用語言
                LanguageManager.ApplyFromSystemLanguage();

                // Runs the application.
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new MainForm());
            }
        }
    }
}

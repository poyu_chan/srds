﻿namespace MotionSystem.Project.Forms
{
    partial class ModulesIMotionControl
    {
        /// <summary> 
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 元件設計工具產生的程式碼

        /// <summary> 
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器
        /// 修改這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.grbIMotionPositionControl = new System.Windows.Forms.GroupBox();
            this.btnOpen = new System.Windows.Forms.Button();
            this.cbxIMotionList = new System.Windows.Forms.ComboBox();
            this.grbIMotionPositionControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbIMotionPositionControl
            // 
            this.grbIMotionPositionControl.Controls.Add(this.btnOpen);
            this.grbIMotionPositionControl.Controls.Add(this.cbxIMotionList);
            this.grbIMotionPositionControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grbIMotionPositionControl.ForeColor = System.Drawing.Color.Blue;
            this.grbIMotionPositionControl.Location = new System.Drawing.Point(0, 0);
            this.grbIMotionPositionControl.Name = "grbIMotionPositionControl";
            this.grbIMotionPositionControl.Size = new System.Drawing.Size(285, 69);
            this.grbIMotionPositionControl.TabIndex = 0;
            this.grbIMotionPositionControl.TabStop = false;
            this.grbIMotionPositionControl.Text = "Modules IMotion Control";
            // 
            // btnOpen
            // 
            this.btnOpen.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnOpen.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnOpen.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold);
            this.btnOpen.ForeColor = System.Drawing.Color.Black;
            this.btnOpen.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnOpen.Location = new System.Drawing.Point(190, 23);
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.Size = new System.Drawing.Size(80, 35);
            this.btnOpen.TabIndex = 194;
            this.btnOpen.Text = "Open";
            this.btnOpen.UseVisualStyleBackColor = false;
            this.btnOpen.Click += new System.EventHandler(this.btnOpen_Click);
            // 
            // cbxIMotionList
            // 
            this.cbxIMotionList.FormattingEnabled = true;
            this.cbxIMotionList.Location = new System.Drawing.Point(6, 30);
            this.cbxIMotionList.Name = "cbxIMotionList";
            this.cbxIMotionList.Size = new System.Drawing.Size(178, 20);
            this.cbxIMotionList.TabIndex = 193;
            // 
            // ModulesIMotionControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.grbIMotionPositionControl);
            this.Name = "ModulesIMotionControl";
            this.Size = new System.Drawing.Size(285, 69);
            this.Load += new System.EventHandler(this.ModulesIMotionControl_Load);
            this.grbIMotionPositionControl.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grbIMotionPositionControl;
        private System.Windows.Forms.Button btnOpen;
        private System.Windows.Forms.ComboBox cbxIMotionList;

    }
}

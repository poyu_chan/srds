﻿using System;
using System.IO;
using System.Text;
using System.Windows.Forms;
using AOISystem.Utilities.Logging;
using AOISystem.Utilities.Recipe;
using MotionSystem.Project.Common;

namespace MotionSystem.Project.Forms
{
    public partial class LoadRecipeForm : Form
    {
        #region - Private Methods -

        #endregion - Private Methods -

        #region - Constructor -

        public LoadRecipeForm()
        {
            InitializeComponent();

            RecipeInfoManager.GetInstance().RecipeInfoSelectedIndexChanged += new RecipeInfoManager.RecipeInfoSelectedIndexChangedEventHandler(MainForm_RecipeInfoSelectedIndexChangedEvent);
        }

        private void RecipEditorForm_Load(object sender, EventArgs e)
        {
        }

        private void LoadRecipeForm_FormClosing(object sender, FormClosingEventArgs e)
        {

        }

        #endregion - Constructor -

        #region - Event Methods -

        private void MainForm_RecipeInfoSelectedIndexChangedEvent(RecipeInfo recipeInfo)
        {

        }

        private void recipeEditorControl_SelectedRecipeInfoEvent(RecipeInfo recipeInfo)
        {
            RecipeData recipeData = new RecipeData();
            recipeData.CurrentRecipe = this.recipeEditorControl.SelectedRecipeInfo;
            recipeData.Load();

            this.pgRecipe.SelectedObject = recipeData;
        }
        #endregion - Event Methods -

        public static string RandomBarcode()
        {
            Random r = new Random();
            StringBuilder sb = new StringBuilder("FAKEID");

            for (int i = 0; i < 6; i++)
            {
                sb.Append(r.Next(1, 9).ToString());
            }
            return sb.ToString();
        }

        public static bool CheckLotID(string lotID)
        {
            return (lotID.Length == 12) ? true : false;
        }

        #region - Private Methods -

        #endregion - Private Methods -




    }
}

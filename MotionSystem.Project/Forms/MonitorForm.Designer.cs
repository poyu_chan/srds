﻿namespace MotionSystem.Project.Forms
{
    partial class MonitorForm
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器
        /// 修改這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tpWaferCheckerInformation = new System.Windows.Forms.TabPage();
            this.hLogger = new AOISystem.Utilities.Logging.HLogger();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tlpOperation = new System.Windows.Forms.TableLayoutPanel();
            this.btnClearOff = new AOISystem.Utilities.Forms.DelayTimeButton();
            this.btnExit = new AOISystem.Utilities.Forms.DelayTimeButton();
            this.btnAlarmReset = new AOISystem.Utilities.Forms.DelayTimeButton();
            this.tabControl.SuspendLayout();
            this.tpWaferCheckerInformation.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tlpOperation.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tpWaferCheckerInformation);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(3, 3);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(1024, 341);
            this.tabControl.TabIndex = 3;
            // 
            // tpWaferCheckerInformation
            // 
            this.tpWaferCheckerInformation.Controls.Add(this.hLogger);
            this.tpWaferCheckerInformation.Location = new System.Drawing.Point(4, 22);
            this.tpWaferCheckerInformation.Name = "tpWaferCheckerInformation";
            this.tpWaferCheckerInformation.Padding = new System.Windows.Forms.Padding(3);
            this.tpWaferCheckerInformation.Size = new System.Drawing.Size(1016, 315);
            this.tpWaferCheckerInformation.TabIndex = 0;
            this.tpWaferCheckerInformation.Text = "Information";
            this.tpWaferCheckerInformation.UseVisualStyleBackColor = true;
            // 
            // hLogger
            // 
            this.hLogger.Dock = System.Windows.Forms.DockStyle.Fill;
            this.hLogger.Location = new System.Drawing.Point(3, 3);
            this.hLogger.Name = "hLogger";
            this.hLogger.Size = new System.Drawing.Size(1010, 309);
            this.hLogger.TabIndex = 0;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.tabControl, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tlpOperation, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1030, 407);
            this.tableLayoutPanel1.TabIndex = 4;
            // 
            // tlpOperation
            // 
            this.tlpOperation.ColumnCount = 5;
            this.tlpOperation.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tlpOperation.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tlpOperation.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tlpOperation.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tlpOperation.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tlpOperation.Controls.Add(this.btnClearOff, 3, 0);
            this.tlpOperation.Controls.Add(this.btnExit, 4, 0);
            this.tlpOperation.Controls.Add(this.btnAlarmReset, 2, 0);
            this.tlpOperation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpOperation.Location = new System.Drawing.Point(3, 350);
            this.tlpOperation.Name = "tlpOperation";
            this.tlpOperation.RowCount = 1;
            this.tlpOperation.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpOperation.Size = new System.Drawing.Size(1024, 54);
            this.tlpOperation.TabIndex = 4;
            // 
            // btnClearOff
            // 
            this.btnClearOff.AccountLevel = AOISystem.Utilities.Account.AccountLevel.Engineer;
            this.btnClearOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnClearOff.DelayTime = 0D;
            this.btnClearOff.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnClearOff.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold);
            this.btnClearOff.ForeColor = System.Drawing.Color.Black;
            this.btnClearOff.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnClearOff.Location = new System.Drawing.Point(618, 3);
            this.btnClearOff.Margin = new System.Windows.Forms.Padding(6, 3, 6, 3);
            this.btnClearOff.Name = "btnClearOff";
            this.btnClearOff.Size = new System.Drawing.Size(192, 48);
            this.btnClearOff.TabIndex = 130;
            this.btnClearOff.TestPermission = false;
            this.btnClearOff.Text = "Clear Off";
            this.btnClearOff.UseVisualStyleBackColor = false;
            this.btnClearOff.Click += new System.EventHandler(this.btnClearOff_Click);
            // 
            // btnExit
            // 
            this.btnExit.AccountLevel = AOISystem.Utilities.Account.AccountLevel.Engineer;
            this.btnExit.BackColor = System.Drawing.Color.Yellow;
            this.btnExit.DelayTime = 0D;
            this.btnExit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnExit.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold);
            this.btnExit.ForeColor = System.Drawing.Color.Black;
            this.btnExit.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnExit.Location = new System.Drawing.Point(822, 3);
            this.btnExit.Margin = new System.Windows.Forms.Padding(6, 3, 6, 3);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(196, 48);
            this.btnExit.TabIndex = 129;
            this.btnExit.TestPermission = false;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnAlarmReset
            // 
            this.btnAlarmReset.AccountLevel = AOISystem.Utilities.Account.AccountLevel.Engineer;
            this.btnAlarmReset.BackColor = System.Drawing.Color.Red;
            this.btnAlarmReset.DelayTime = 0D;
            this.btnAlarmReset.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnAlarmReset.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold);
            this.btnAlarmReset.ForeColor = System.Drawing.Color.Black;
            this.btnAlarmReset.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnAlarmReset.Location = new System.Drawing.Point(414, 3);
            this.btnAlarmReset.Margin = new System.Windows.Forms.Padding(6, 3, 6, 3);
            this.btnAlarmReset.Name = "btnAlarmReset";
            this.btnAlarmReset.Size = new System.Drawing.Size(192, 48);
            this.btnAlarmReset.TabIndex = 128;
            this.btnAlarmReset.TestPermission = false;
            this.btnAlarmReset.Text = "Alarm Reset";
            this.btnAlarmReset.UseVisualStyleBackColor = false;
            this.btnAlarmReset.Click += new System.EventHandler(this.btnAlarmReset_Click);
            // 
            // MonitorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1030, 407);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MonitorForm";
            this.Text = "Monitor Form";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MonitorForm_FormClosing);
            this.Load += new System.EventHandler(this.MonitorForm_Load);
            this.tabControl.ResumeLayout(false);
            this.tpWaferCheckerInformation.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tlpOperation.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tpWaferCheckerInformation;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tlpOperation;
        private AOISystem.Utilities.Forms.DelayTimeButton btnClearOff;
        private AOISystem.Utilities.Forms.DelayTimeButton btnExit;
        private AOISystem.Utilities.Forms.DelayTimeButton btnAlarmReset;
        private AOISystem.Utilities.Logging.HLogger hLogger;
    }
}
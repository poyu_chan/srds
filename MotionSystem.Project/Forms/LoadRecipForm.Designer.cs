﻿namespace MotionSystem.Project.Forms
{
    partial class LoadRecipeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoadRecipeForm));
            this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
            this.recipeEditorControl = new AOISystem.Utilities.Recipe.RecipeEditorControl();
            this.pgRecipe = new System.Windows.Forms.PropertyGrid();
            this.tlpMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // tlpMain
            // 
            this.tlpMain.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.InsetDouble;
            this.tlpMain.ColumnCount = 2;
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 500F));
            this.tlpMain.Controls.Add(this.pgRecipe, 1, 0);
            this.tlpMain.Controls.Add(this.recipeEditorControl, 0, 0);
            this.tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpMain.Location = new System.Drawing.Point(0, 0);
            this.tlpMain.Name = "tlpMain";
            this.tlpMain.RowCount = 1;
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 556F));
            this.tlpMain.Size = new System.Drawing.Size(972, 605);
            this.tlpMain.TabIndex = 0;
            // 
            // recipeEditorControl
            // 
            this.recipeEditorControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.recipeEditorControl.Location = new System.Drawing.Point(6, 6);
            this.recipeEditorControl.Name = "recipeEditorControl";
            this.recipeEditorControl.SelectedRecipeInfo = ((AOISystem.Utilities.Recipe.RecipeInfo)(resources.GetObject("recipeEditorControl.SelectedRecipeInfo")));
            this.recipeEditorControl.Size = new System.Drawing.Size(457, 593);
            this.recipeEditorControl.TabIndex = 27;
            this.recipeEditorControl.SelectedRecipeInfoEvent += new AOISystem.Utilities.Recipe.RecipeEditorControl.SelectedRecipeInfoEventHandler(this.recipeEditorControl_SelectedRecipeInfoEvent);
            // 
            // pgRecipe
            // 
            this.pgRecipe.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pgRecipe.Location = new System.Drawing.Point(472, 6);
            this.pgRecipe.Name = "pgRecipe";
            this.pgRecipe.Size = new System.Drawing.Size(494, 593);
            this.pgRecipe.TabIndex = 32;
            // 
            // LoadRecipeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(972, 605);
            this.Controls.Add(this.tlpMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(988, 643);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(988, 643);
            this.Name = "LoadRecipeForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Load Recip Form";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.LoadRecipeForm_FormClosing);
            this.Load += new System.EventHandler(this.RecipEditorForm_Load);
            this.tlpMain.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private AOISystem.Utilities.Recipe.RecipeEditorControl recipeEditorControl;
        private System.Windows.Forms.PropertyGrid pgRecipe;
    }
}
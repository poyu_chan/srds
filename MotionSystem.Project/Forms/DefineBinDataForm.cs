﻿using AOISystem.Utilities.Collections;
using AOISystem.Utilities.IO;
using Sorting_Rule;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;
using System.Windows.Forms;

namespace MotionSystem.Project.Forms
{
    public partial class DefineBinDataForm : Form
    {
        public DefineBinDataForm()
        {
            InitializeComponent();
        }

        public void SaveToFile(string FilePath)
        {
            SerializableDictionary<string, string> dicDrawer = new SerializableDictionary<string, string>();

            foreach (var item in pnlBinCtl.Controls)
            {
                if (item is BinCtl)
                {
                    BinCtl ctl = (BinCtl)item;
                    dicDrawer.Add(ctl.GetDrawerName(), ctl.GetDrawerData());
                }
            }
            XmlFile.SerializeToFile<SerializableDictionary<string, string>>(FilePath, dicDrawer);
            //File.WriteAllText("Drawer.txt", new JavaScriptSerializer().Serialize(dicDrawer));
        }

        public bool OpenBinDataFile(string FileName)
        {
            try
            {
                Dictionary<string, string> dicBinData = new Dictionary<string, string>();
                dicBinData = XmlFile.DeserializeFromFile<SerializableDictionary<string, string>>(FileName);


                foreach (var item in pnlBinCtl.Controls)
                {
                    if (item is BinCtl)
                    {
                        BinCtl ctl = item as BinCtl;
                        SetCtlData(dicBinData, ctl);
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("載入分類槽資料異常!\n {0}", ex.Message));
                return false;
            }
        }

        private void SetCtlData(Dictionary<string, string> dic, BinCtl ctl)
        {
            string ReportFieldName = "";
            BinTypeEnum BinType = BinTypeEnum.NG;

            string KeyValue = "";
            string[] KeyData = new string[10];

            //回填分類槽資料
            KeyValue = dic[ctl.GetType().GetProperty("DrawerNumber").GetValue(ctl, null).ToString()];
            KeyData = KeyValue.Split(',');
            ReportFieldName = KeyData[0];
            ctl.GetType().GetProperty("ReportFieldName").SetValue(ctl, ReportFieldName, null);

            //回填BinType資料
            BinType = GetBinType(KeyData[1]);
            ctl.GetType().GetProperty("SelectBinType").SetValue(ctl, BinType, null);
        }

        private BinTypeEnum GetBinType(string Type)
        {
            return (Type == "OK") ? BinTypeEnum.OK : BinTypeEnum.NG;
        }
    }
}

﻿using System;
using System.Windows.Forms;
using MotionSystem.Project.Common;

namespace MotionSystem.Project.Forms
{
    public partial class AboutForm : Form
    {
        public AboutForm()
        {
            InitializeComponent();
        }

        private void AboutForm_Load(object sender, EventArgs e)
        {
            this.txtVesionInfo.Text = AOI.GetVersionInfo().ToString();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}

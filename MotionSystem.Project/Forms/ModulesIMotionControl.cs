﻿using AOISystem.Utilities;
using AOISystem.Utilities.Modules.Syntek.EtherCAT.SlaveModules.Motion;
using MotionSystem.Project.Common;
using System;
using System.Reflection;
using System.Windows.Forms;

namespace MotionSystem.Project.Forms
{
    public partial class ModulesIMotionControl : UserControl
    {
        public ModulesIMotionControl()
        {
            InitializeComponent();
        }

        private void ModulesIMotionControl_Load(object sender, EventArgs e)
        {
            if (ProcessInfo.IsDesignMode())
            {
                return;
            }
            PropertyInfo[] propertyInfos = typeof(Modules).GetProperties();
            this.cbxIMotionList.Items.Clear();
            foreach (var item in propertyInfos)
            {
                object instance = item.GetValue(AOI.Modules, null);
                if (instance != null && instance is CEtherCATMotion)
                {
                    this.cbxIMotionList.Items.Add(item.Name);
                }
            }
            if (this.cbxIMotionList.Items.Count > 0)
            {
                this.cbxIMotionList.SelectedIndex = 0;
            }
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.cbxIMotionList.Text))
            {
                return;
            }
            CEtherCATMotion iMotion = null;
            PropertyInfo propertyInfo = typeof(Modules).GetProperty(this.cbxIMotionList.Text);
            if (propertyInfo != null)
            {
                iMotion = (CEtherCATMotion)propertyInfo.GetValue(AOI.Modules, null);
            }
            if (iMotion != null)
            {
                iMotion.ConfigurationShow();
            }
        }
    }
}

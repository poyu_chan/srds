﻿namespace MotionSystem.Project.Forms
{
    partial class DefineBinDataForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.pnlBinCtl = new System.Windows.Forms.Panel();
            this.binCtl15 = new Sorting_Rule.BinCtl();
            this.binCtl11 = new Sorting_Rule.BinCtl();
            this.binCtl12 = new Sorting_Rule.BinCtl();
            this.binCtl13 = new Sorting_Rule.BinCtl();
            this.binCtl14 = new Sorting_Rule.BinCtl();
            this.binCtl7 = new Sorting_Rule.BinCtl();
            this.binCtl8 = new Sorting_Rule.BinCtl();
            this.binCtl9 = new Sorting_Rule.BinCtl();
            this.binCtl10 = new Sorting_Rule.BinCtl();
            this.binCtl1 = new Sorting_Rule.BinCtl();
            this.binCtl6 = new Sorting_Rule.BinCtl();
            this.binCtl2 = new Sorting_Rule.BinCtl();
            this.binCtl5 = new Sorting_Rule.BinCtl();
            this.binCtl3 = new Sorting_Rule.BinCtl();
            this.binCtl4 = new Sorting_Rule.BinCtl();
            this.pnlBinCtl.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.LightSteelBlue;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Location = new System.Drawing.Point(22, 178);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(775, 43);
            this.panel1.TabIndex = 1;
            // 
            // pnlBinCtl
            // 
            this.pnlBinCtl.Controls.Add(this.binCtl15);
            this.pnlBinCtl.Controls.Add(this.binCtl11);
            this.pnlBinCtl.Controls.Add(this.binCtl12);
            this.pnlBinCtl.Controls.Add(this.binCtl13);
            this.pnlBinCtl.Controls.Add(this.binCtl14);
            this.pnlBinCtl.Controls.Add(this.binCtl7);
            this.pnlBinCtl.Controls.Add(this.binCtl8);
            this.pnlBinCtl.Controls.Add(this.binCtl9);
            this.pnlBinCtl.Controls.Add(this.binCtl10);
            this.pnlBinCtl.Controls.Add(this.panel1);
            this.pnlBinCtl.Controls.Add(this.binCtl1);
            this.pnlBinCtl.Controls.Add(this.binCtl6);
            this.pnlBinCtl.Controls.Add(this.binCtl2);
            this.pnlBinCtl.Controls.Add(this.binCtl5);
            this.pnlBinCtl.Controls.Add(this.binCtl3);
            this.pnlBinCtl.Controls.Add(this.binCtl4);
            this.pnlBinCtl.Location = new System.Drawing.Point(12, 12);
            this.pnlBinCtl.Name = "pnlBinCtl";
            this.pnlBinCtl.Size = new System.Drawing.Size(1006, 409);
            this.pnlBinCtl.TabIndex = 9;
            // 
            // binCtl15
            // 
            this.binCtl15.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.binCtl15.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.binCtl15.DrawerLine = Sorting_Rule.DrawerLineType.Fifth;
            this.binCtl15.DrawerNumber = "14";
            this.binCtl15.DrawerPosition = Sorting_Rule.DrawerPositionType.First;
            this.binCtl15.DrawerSide = Sorting_Rule.DrawerSideType.Right;
            this.binCtl15.IsEditable = false;
            this.binCtl15.Location = new System.Drawing.Point(826, 176);
            this.binCtl15.Name = "binCtl15";
            this.binCtl15.ReportFieldName = "Other";
            this.binCtl15.SelectBinType = Sorting_Rule.BinTypeEnum.NG;
            this.binCtl15.Size = new System.Drawing.Size(144, 76);
            this.binCtl15.TabIndex = 16;
            // 
            // binCtl11
            // 
            this.binCtl11.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.binCtl11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.binCtl11.DrawerLine = Sorting_Rule.DrawerLineType.Second;
            this.binCtl11.DrawerNumber = "10";
            this.binCtl11.DrawerPosition = Sorting_Rule.DrawerPositionType.First;
            this.binCtl11.DrawerSide = Sorting_Rule.DrawerSideType.Right;
            this.binCtl11.IsEditable = true;
            this.binCtl11.Location = new System.Drawing.Point(618, 238);
            this.binCtl11.Name = "binCtl11";
            this.binCtl11.ReportFieldName = "";
            this.binCtl11.SelectBinType = Sorting_Rule.BinTypeEnum.OK;
            this.binCtl11.Size = new System.Drawing.Size(144, 76);
            this.binCtl11.TabIndex = 12;
            // 
            // binCtl12
            // 
            this.binCtl12.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.binCtl12.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.binCtl12.DrawerLine = Sorting_Rule.DrawerLineType.Second;
            this.binCtl12.DrawerNumber = "13";
            this.binCtl12.DrawerPosition = Sorting_Rule.DrawerPositionType.Second;
            this.binCtl12.DrawerSide = Sorting_Rule.DrawerSideType.Left;
            this.binCtl12.IsEditable = true;
            this.binCtl12.Location = new System.Drawing.Point(618, 14);
            this.binCtl12.Name = "binCtl12";
            this.binCtl12.ReportFieldName = "";
            this.binCtl12.SelectBinType = Sorting_Rule.BinTypeEnum.OK;
            this.binCtl12.Size = new System.Drawing.Size(144, 76);
            this.binCtl12.TabIndex = 15;
            // 
            // binCtl13
            // 
            this.binCtl13.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.binCtl13.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.binCtl13.DrawerLine = Sorting_Rule.DrawerLineType.Second;
            this.binCtl13.DrawerNumber = "11";
            this.binCtl13.DrawerPosition = Sorting_Rule.DrawerPositionType.Second;
            this.binCtl13.DrawerSide = Sorting_Rule.DrawerSideType.Right;
            this.binCtl13.IsEditable = true;
            this.binCtl13.Location = new System.Drawing.Point(618, 320);
            this.binCtl13.Name = "binCtl13";
            this.binCtl13.ReportFieldName = "";
            this.binCtl13.SelectBinType = Sorting_Rule.BinTypeEnum.OK;
            this.binCtl13.Size = new System.Drawing.Size(144, 76);
            this.binCtl13.TabIndex = 13;
            // 
            // binCtl14
            // 
            this.binCtl14.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.binCtl14.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.binCtl14.DrawerLine = Sorting_Rule.DrawerLineType.Second;
            this.binCtl14.DrawerNumber = "12";
            this.binCtl14.DrawerPosition = Sorting_Rule.DrawerPositionType.First;
            this.binCtl14.DrawerSide = Sorting_Rule.DrawerSideType.Left;
            this.binCtl14.IsEditable = true;
            this.binCtl14.Location = new System.Drawing.Point(618, 96);
            this.binCtl14.Name = "binCtl14";
            this.binCtl14.ReportFieldName = "";
            this.binCtl14.SelectBinType = Sorting_Rule.BinTypeEnum.OK;
            this.binCtl14.Size = new System.Drawing.Size(144, 76);
            this.binCtl14.TabIndex = 14;
            // 
            // binCtl7
            // 
            this.binCtl7.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.binCtl7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.binCtl7.DrawerLine = Sorting_Rule.DrawerLineType.Second;
            this.binCtl7.DrawerNumber = "06";
            this.binCtl7.DrawerPosition = Sorting_Rule.DrawerPositionType.First;
            this.binCtl7.DrawerSide = Sorting_Rule.DrawerSideType.Right;
            this.binCtl7.IsEditable = true;
            this.binCtl7.Location = new System.Drawing.Point(417, 238);
            this.binCtl7.Name = "binCtl7";
            this.binCtl7.ReportFieldName = "";
            this.binCtl7.SelectBinType = Sorting_Rule.BinTypeEnum.OK;
            this.binCtl7.Size = new System.Drawing.Size(144, 76);
            this.binCtl7.TabIndex = 8;
            // 
            // binCtl8
            // 
            this.binCtl8.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.binCtl8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.binCtl8.DrawerLine = Sorting_Rule.DrawerLineType.Second;
            this.binCtl8.DrawerNumber = "09";
            this.binCtl8.DrawerPosition = Sorting_Rule.DrawerPositionType.Second;
            this.binCtl8.DrawerSide = Sorting_Rule.DrawerSideType.Left;
            this.binCtl8.IsEditable = true;
            this.binCtl8.Location = new System.Drawing.Point(417, 14);
            this.binCtl8.Name = "binCtl8";
            this.binCtl8.ReportFieldName = "";
            this.binCtl8.SelectBinType = Sorting_Rule.BinTypeEnum.OK;
            this.binCtl8.Size = new System.Drawing.Size(144, 76);
            this.binCtl8.TabIndex = 11;
            // 
            // binCtl9
            // 
            this.binCtl9.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.binCtl9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.binCtl9.DrawerLine = Sorting_Rule.DrawerLineType.Second;
            this.binCtl9.DrawerNumber = "07";
            this.binCtl9.DrawerPosition = Sorting_Rule.DrawerPositionType.Second;
            this.binCtl9.DrawerSide = Sorting_Rule.DrawerSideType.Right;
            this.binCtl9.IsEditable = true;
            this.binCtl9.Location = new System.Drawing.Point(417, 320);
            this.binCtl9.Name = "binCtl9";
            this.binCtl9.ReportFieldName = "";
            this.binCtl9.SelectBinType = Sorting_Rule.BinTypeEnum.OK;
            this.binCtl9.Size = new System.Drawing.Size(144, 76);
            this.binCtl9.TabIndex = 9;
            // 
            // binCtl10
            // 
            this.binCtl10.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.binCtl10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.binCtl10.DrawerLine = Sorting_Rule.DrawerLineType.Second;
            this.binCtl10.DrawerNumber = "08";
            this.binCtl10.DrawerPosition = Sorting_Rule.DrawerPositionType.First;
            this.binCtl10.DrawerSide = Sorting_Rule.DrawerSideType.Left;
            this.binCtl10.IsEditable = true;
            this.binCtl10.Location = new System.Drawing.Point(417, 96);
            this.binCtl10.Name = "binCtl10";
            this.binCtl10.ReportFieldName = "";
            this.binCtl10.SelectBinType = Sorting_Rule.BinTypeEnum.OK;
            this.binCtl10.Size = new System.Drawing.Size(144, 76);
            this.binCtl10.TabIndex = 10;
            // 
            // binCtl1
            // 
            this.binCtl1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.binCtl1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.binCtl1.DrawerLine = Sorting_Rule.DrawerLineType.First;
            this.binCtl1.DrawerNumber = "01";
            this.binCtl1.DrawerPosition = Sorting_Rule.DrawerPositionType.First;
            this.binCtl1.DrawerSide = Sorting_Rule.DrawerSideType.Right;
            this.binCtl1.IsEditable = true;
            this.binCtl1.Location = new System.Drawing.Point(35, 238);
            this.binCtl1.Name = "binCtl1";
            this.binCtl1.ReportFieldName = "";
            this.binCtl1.SelectBinType = Sorting_Rule.BinTypeEnum.OK;
            this.binCtl1.Size = new System.Drawing.Size(144, 76);
            this.binCtl1.TabIndex = 2;
            // 
            // binCtl6
            // 
            this.binCtl6.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.binCtl6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.binCtl6.DrawerLine = Sorting_Rule.DrawerLineType.Fifth;
            this.binCtl6.DrawerNumber = "15";
            this.binCtl6.DrawerPosition = Sorting_Rule.DrawerPositionType.First;
            this.binCtl6.DrawerSide = Sorting_Rule.DrawerSideType.Right;
            this.binCtl6.IsEditable = false;
            this.binCtl6.Location = new System.Drawing.Point(826, 258);
            this.binCtl6.Name = "binCtl6";
            this.binCtl6.ReportFieldName = "Clear";
            this.binCtl6.SelectBinType = Sorting_Rule.BinTypeEnum.NG;
            this.binCtl6.Size = new System.Drawing.Size(144, 76);
            this.binCtl6.TabIndex = 7;
            // 
            // binCtl2
            // 
            this.binCtl2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.binCtl2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.binCtl2.DrawerLine = Sorting_Rule.DrawerLineType.Second;
            this.binCtl2.DrawerNumber = "02";
            this.binCtl2.DrawerPosition = Sorting_Rule.DrawerPositionType.First;
            this.binCtl2.DrawerSide = Sorting_Rule.DrawerSideType.Right;
            this.binCtl2.IsEditable = true;
            this.binCtl2.Location = new System.Drawing.Point(216, 238);
            this.binCtl2.Name = "binCtl2";
            this.binCtl2.ReportFieldName = "";
            this.binCtl2.SelectBinType = Sorting_Rule.BinTypeEnum.OK;
            this.binCtl2.Size = new System.Drawing.Size(144, 76);
            this.binCtl2.TabIndex = 3;
            // 
            // binCtl5
            // 
            this.binCtl5.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.binCtl5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.binCtl5.DrawerLine = Sorting_Rule.DrawerLineType.Second;
            this.binCtl5.DrawerNumber = "05";
            this.binCtl5.DrawerPosition = Sorting_Rule.DrawerPositionType.Second;
            this.binCtl5.DrawerSide = Sorting_Rule.DrawerSideType.Left;
            this.binCtl5.IsEditable = true;
            this.binCtl5.Location = new System.Drawing.Point(216, 14);
            this.binCtl5.Name = "binCtl5";
            this.binCtl5.ReportFieldName = null;
            this.binCtl5.SelectBinType = Sorting_Rule.BinTypeEnum.NG;
            this.binCtl5.Size = new System.Drawing.Size(144, 76);
            this.binCtl5.TabIndex = 6;
            // 
            // binCtl3
            // 
            this.binCtl3.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.binCtl3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.binCtl3.DrawerLine = Sorting_Rule.DrawerLineType.Second;
            this.binCtl3.DrawerNumber = "03";
            this.binCtl3.DrawerPosition = Sorting_Rule.DrawerPositionType.Second;
            this.binCtl3.DrawerSide = Sorting_Rule.DrawerSideType.Right;
            this.binCtl3.IsEditable = true;
            this.binCtl3.Location = new System.Drawing.Point(216, 320);
            this.binCtl3.Name = "binCtl3";
            this.binCtl3.ReportFieldName = "";
            this.binCtl3.SelectBinType = Sorting_Rule.BinTypeEnum.OK;
            this.binCtl3.Size = new System.Drawing.Size(144, 76);
            this.binCtl3.TabIndex = 4;
            // 
            // binCtl4
            // 
            this.binCtl4.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.binCtl4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.binCtl4.DrawerLine = Sorting_Rule.DrawerLineType.Second;
            this.binCtl4.DrawerNumber = "04";
            this.binCtl4.DrawerPosition = Sorting_Rule.DrawerPositionType.First;
            this.binCtl4.DrawerSide = Sorting_Rule.DrawerSideType.Left;
            this.binCtl4.IsEditable = true;
            this.binCtl4.Location = new System.Drawing.Point(216, 96);
            this.binCtl4.Name = "binCtl4";
            this.binCtl4.ReportFieldName = "";
            this.binCtl4.SelectBinType = Sorting_Rule.BinTypeEnum.OK;
            this.binCtl4.Size = new System.Drawing.Size(144, 76);
            this.binCtl4.TabIndex = 5;
            // 
            // DefineBinDataForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1049, 437);
            this.Controls.Add(this.pnlBinCtl);
            this.Name = "DefineBinDataForm";
            this.Text = "DefineBinDataForm";
            this.pnlBinCtl.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private Sorting_Rule.BinCtl binCtl1;
        private Sorting_Rule.BinCtl binCtl2;
        private Sorting_Rule.BinCtl binCtl3;
        private Sorting_Rule.BinCtl binCtl4;
        private Sorting_Rule.BinCtl binCtl5;
        private Sorting_Rule.BinCtl binCtl6;
        private System.Windows.Forms.Panel pnlBinCtl;
        private Sorting_Rule.BinCtl binCtl15;
        private Sorting_Rule.BinCtl binCtl11;
        private Sorting_Rule.BinCtl binCtl12;
        private Sorting_Rule.BinCtl binCtl13;
        private Sorting_Rule.BinCtl binCtl14;
        private Sorting_Rule.BinCtl binCtl7;
        private Sorting_Rule.BinCtl binCtl8;
        private Sorting_Rule.BinCtl binCtl9;
        private Sorting_Rule.BinCtl binCtl10;
    }
}
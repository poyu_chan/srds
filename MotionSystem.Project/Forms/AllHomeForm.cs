﻿using AOISystem.Utilities;
using AOISystem.Utilities.Flow;
using AOISystem.Utilities.Forms;
using AOISystem.Utilities.Modules.Syntek.EtherCAT.SlaveModules.Motion;
using AOISystem.Utilities.MultiLanguage;
using MotionSystem.Project.Common;
using MotionSystem.Project.Resources;
using System;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;

namespace MotionSystem.Project.Forms
{
    public partial class AllHomeForm : Form
    {
        CEtherCATMotion l122M2X4 = null;

        public AllHomeForm()
        {
            InitializeComponent();
        }

        private void GetMotor(object sender, EventArgs e)
        {
            RadioButton rad = (RadioButton)sender;
            string MotorName = rad.Name.Substring(3);

            if (string.IsNullOrEmpty(MotorName))
            {
                return;
            }

            PropertyInfo propertyInfo = typeof(Modules).GetProperty(MotorName);

            if (propertyInfo != null)
            {
                l122M2X4 = (CEtherCATMotion)propertyInfo.GetValue(AOI.Modules, null);
            }
        }

        private void AllHomeForm_Load(object sender, EventArgs e)
        {
            this.tabControl.Appearance = TabAppearance.Buttons;
            this.tabControl.SizeMode = TabSizeMode.Fixed;
            this.tabControl.ItemSize = new Size(0, 1);

            l122M2X4 = AOI.Modules.CV1;
            this.tmrScan.Start();

            if (CheckMotorEncode())
            {
                //需要手動先移動位置
                this.tabControl.SelectedTab = this.tpLoader;
            }
            else
            {
                //載入表單時直接整機復歸
                this.tabControl.SelectedTab = this.tpHome;
                FlowControl flowControl = AOI.FlowControlHelper.GetFlowControl("Home");
                flowControl.RestartAll();
            }
        }

        private void AllHomeForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.tmrScan.Stop();
        }

        #region 正轉
        private void btnJogP_MouseDown(object sender, MouseEventArgs e)
        {
            if (l122M2X4 != null)
            {
                l122M2X4.ContinuousMove(RotationDirection.CW, JogSpeed.Low);
            }
        }
        #endregion

        #region 反轉
        private void btnJogN_MouseDown(object sender, MouseEventArgs e)
        {
            if (l122M2X4 != null)
            {
                l122M2X4.ContinuousMove(RotationDirection.CCW, JogSpeed.Low);
            }
        }
        #endregion

        #region 停止
        private void btnStop_Click(object sender, EventArgs e)
        {
            if (l122M2X4 != null)
            {
                l122M2X4.Stop(StopType.Emergency);
            }
        }
        #endregion

        private void btnHome_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("請確認是否將Loader端馬達移置安全位置!!", "注意", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                this.tabControl.SelectedTab = this.tpHome;

                FlowControl flowControl = AOI.FlowControlHelper.GetFlowControl("Home");
                flowControl.RestartAll();
            }
            else
            {
                this.DialogResult = DialogResult.Cancel;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void tmrScan_Tick(object sender, EventArgs e)
        {
            if (l122M2X4 != null)
            {
                ldrAlarm.On = l122M2X4.IsAlarm;
                ldrLimitP.On = l122M2X4.IsLimitP;
                ldrLimitN.On = l122M2X4.IsLimitN;

                ntxbCommandCouter.Text = l122M2X4.Position.ToString();
                ntxbFeedBackCounter.Text = l122M2X4.Encoder.ToString();
            }
            //偵測哪些軸是否完成HOME
            foreach (Control control in flowLayoutPanel.Controls)
            {
                LedRectangle ledRectangle = control as LedRectangle;
                PropertyInfo propertyInfo = typeof(Modules).GetProperty(ledRectangle.Name.Substring(3));
                CEtherCATMotion iMotion = (CEtherCATMotion)propertyInfo.GetValue(AOI.Modules, null);
                if (iMotion != null)
                {
                    ledRectangle.On = iMotion.IsHome;   
                }
            }
            this.ledAOIHomeFlow.On = AOI.WorkData.IsAOIHomeCompleted;
            if (AOI.WorkData.IsAllHomeCompleted)
            {
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
        }

        private void ledIMotion_DoubleClick(object sender, EventArgs e)
        {
            LedRectangle ledRectangle = sender as LedRectangle;
            PropertyInfo propertyInfo = typeof(Modules).GetProperty(ledRectangle.Name.Substring(3));
            CEtherCATMotion iMotion = (CEtherCATMotion)propertyInfo.GetValue(AOI.Modules, null);
            iMotion.Home();
        }

        private void btnABORT_Click(object sender, EventArgs e)
        {
            FlowControl flowControl = AOI.FlowControlHelper.GetFlowControl("Home");
            flowControl.FailAll();
            AOI.Modules.AllMotorStop(StopType.Emergency);

            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void ledLoaderHomeFlow_DoubleClick(object sender, EventArgs e)
        {
            FlowControl flowControl = AOI.FlowControlHelper.GetFlowControl("Home");
            flowControl.Restart("LoaderHomeFlow");
        }

        private void ledAOIHomeFlow_DoubleClick(object sender, EventArgs e)
        {
            FlowControl flowControl = AOI.FlowControlHelper.GetFlowControl("Home");
            flowControl.Restart("AOIHomeFlow");
        }

        private void ledBinHomeFlow_DoubleClick(object sender, EventArgs e)
        {
            FlowControl flowControl = AOI.FlowControlHelper.GetFlowControl("Home");
            flowControl.Restart("BinHomeFlow");
        }

        private bool CheckMotorEncode()
        {
            return false;
        }
    }
}

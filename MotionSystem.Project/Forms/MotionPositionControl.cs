﻿using System;
using System.ComponentModel;
using System.Reflection;
using System.Windows.Forms;
using AOISystem.Utilities;
using AOISystem.Utilities.Modules.Syntek.EtherCAT.SlaveModules.Motion;
using MotionSystem.Project.Common;

namespace MotionSystem.Project.Forms
{
    public enum PositionMode { Command, Encoder }

    public enum MotorListEnum
    {
        LoaderLeftX,
        LoaderRightX,
        LoaderLeftZ,
        LoaderRightZ,
        CV1,
        CV2,
        CV3,
        CV4,
        CV5,
        CV6,
        CV7,
        CV8,
        CV9,
        RotateFrontT,
        RotateFrontZ,
        RotateFrontX,
        RotateRearT,
        RotateRearZ,
        RotateRearX,
        ReworkX,
        ReworkZ,
        AlignmentFront,
        AlignmentRear,
        BinClassifyX1,
        BinClassifyX2,
        BinClassifyX3,
        BinClassifyX4,
        BinClassifyY1,
        BinClassifyY2,
        BinClassifyY3,
        BinClassifyY4,
        BinClassifyZ1,
        BinClassifyZ2,
        BinClassifyZ3,
        BinClassifyZ4,
        BinLeftZ1,
        BinRightZ1,
        BinLeftZ2,
        BinRightZ2,
        BinLeftZ3,
        BinRightZ3,
        BinLeftZ4,
        BinRightZ4,
        BinCenterZ4
    }

    public partial class MotionPositionControl : UserControl
    {
        private CEtherCATMotion _iMotion;
        private PositionMode _positionMode;
        private Type _type;
        private object _instance;
        private string _positionPropertyName;
        private bool _useMotorFunction;

        public MotionPositionControl()
        {
            InitializeComponent();

            this.PropertyName = "PropertyName";
            this.LabelName = "Name";
            this.MotorName = MotorListEnum.LoaderLeftX;
            this.PositionMode = PositionMode.Encoder;
            this.ParameterType = ParameterType.Recipe;
            this.UseMotorFunction = true;
        }

        private void MotionPositionControl_Load(object sender, EventArgs e)
        {
            if (ProcessInfo.IsDesignMode())
            {
                return;
            }

            _iMotion = AOI.Modules.GetMotor(this.MotorName.ToString());

            if (_iMotion == null)
            {
                this.btnSet.Enabled = false;
                this.btnGo.Enabled = false;
            }

            _positionMode = PositionMode;
            _positionPropertyName = PropertyName;
            this.lblName.Text = this.LabelName;
            SetButtonVisible(this.UseMotorFunction);
            Restore();
        }

        public event EventHandler ValueChanged;

        [Category("Customer Property"), Description("選擇控制元件馬達名稱")]
        public MotorListEnum MotorName { get; set; }

        [Category("Customer Property"), Description("選擇儲存點位類型")]
        public PositionMode PositionMode { get; set; }

        [Category("Customer Property"), Description("選擇儲存參數類型")]
        public ParameterType ParameterType { get; set; }

        [Category("Customer Property"), Description("設定儲存參數名稱")]
        public string PropertyName
        {
            get { return _positionPropertyName; }
            set { _positionPropertyName = value; }
        }

        [Category("Customer Property"), Description("控制元件上顯式名稱")]
        public string LabelName
        {
            get { return this.lblName.Text; }
            set { this.lblName.Text = value; }
        }

        [Category("Customer Property"), Description("儲存點位數值")]
        public double Value
        {
            get { return double.Parse(this.txtPosition.Text); }
            set { this.txtPosition.Text = value.ToString(); }
        }

        [Category("Customer Property"), Description("是否使用馬達功能")]
        public bool UseMotorFunction
        {
            get { return _useMotorFunction; }
            set
            {
                _useMotorFunction = value;
                SetButtonVisible(_useMotorFunction);
            }
        }

        private void txtPosition_TextChanged(object sender, EventArgs e)
        {
            if (ValueChanged != null)
            {
                ValueChanged(this, e);
            }
        }

        private void btnSet_Click(object sender, EventArgs e)
        {
            _iMotion.ConfigurationSlimShow(Configuration_FormClosing);
        }

        private void Configuration_FormClosing(object sender, FormClosingEventArgs e)
        {
            double position = _positionMode == PositionMode.Command ? _iMotion.Position : _iMotion.Encoder;
            this.txtPosition.Text = position.ToString("F2");
        }

        private void btnGo_Click(object sender, EventArgs e)
        {
            double position = double.Parse(this.txtPosition.Text);
            _iMotion.AbsolueMove(position);
        }

        private void SetButtonVisible(bool IsVisible)
        {
            if (IsVisible)
            {
                this.btnGo.Visible = IsVisible;
                this.btnSet.Visible = IsVisible;
            }
            else
            {
                this.btnGo.Visible = IsVisible;
                this.btnSet.Visible = IsVisible;
            }
        }

        public void Save(object selectedInstance = null)
        {
            if (ParameterType == ParameterType.System)
            {
                _type = typeof(SystemData);

                if (selectedInstance != null)
                {
                    SystemData systemData = (SystemData)selectedInstance;
                    systemData.Load();
                    _instance = systemData;
                }
                else
                {
                    _instance = AOI.SystemData;
                }
            }
            else
            {
                _type = typeof(RecipeData);

                if (selectedInstance != null)
                {
                    RecipeData recipeData = (RecipeData)selectedInstance;
                    recipeData.Load();
                    _instance = recipeData;
                }
                else
                {
                    _instance = AOI.RecipeData;
                }
            }

            double position = double.Parse(this.txtPosition.Text);
            PropertyInfo propertyInfo = _type.GetProperty(_positionPropertyName);
            propertyInfo.SetValue(_instance, position, null);
        }

        public void Restore(object selectedInstance = null)
        {
            if (ParameterType == ParameterType.System)
            {
                _type = typeof(SystemData);

                if (selectedInstance != null)
                {
                    SystemData systemData = (SystemData)selectedInstance;
                    systemData.Load();
                    _instance = systemData;
                }
                else
                {
                    _instance = AOI.SystemData;
                }
            }
            else
            {
                _type = typeof(RecipeData);

                if (selectedInstance != null)
                {
                    RecipeData recipeData = (RecipeData)selectedInstance;
                    recipeData.Load();
                    _instance = recipeData;
                }
                else
                {
                    _instance = AOI.RecipeData;
                }
            }

            PropertyInfo propertyInfo = _type.GetProperty(_positionPropertyName);
            this.txtPosition.Text = propertyInfo.GetValue(_instance, null).ToString();
        }
    }
}

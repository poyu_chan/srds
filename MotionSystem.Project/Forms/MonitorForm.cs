﻿using AOISystem.Utilities;
using MotionSystem.Project.Common;
using System;
using System.Windows.Forms;

namespace MotionSystem.Project.Forms
{
    public partial class MonitorForm : Form
    {
        private static MonitorForm instance = null;
        private static readonly object synObject = new object();

        public MonitorForm()
        {
            InitializeComponent();
        }

        private void MonitorForm_Load(object sender, EventArgs e)
        {
        }

        private void MonitorForm_FormClosing(object sender, FormClosingEventArgs e)
        {
        }

        public static MonitorForm GetInstance()
        {
            if (instance == null || instance.IsDisposed)
            {
                lock (synObject)
                {
                    if (instance == null || instance.IsDisposed)
                    {
                        instance = new MonitorForm();
                    }
                }
            }
            return instance;
        }

        public void FormClose()
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new Action(FormClose));
                return;
            }
            this.Close();
        }

        private void btnAlarmReset_Click(object sender, EventArgs e)
        {
            AOI.SystemNotify(OperationStatus.AlarmReset);
        }

        private void btnClearOff_Click(object sender, EventArgs e)
        {
            AOI.SystemNotify(OperationStatus.ClearOff);
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void NotifyMessage(string msg)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new Action<string>(NotifyMessage), msg);
                return;
            }
            this.hLogger.SetCommandLine(msg);
        }

        public void ClearMessage()
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new Action(ClearMessage));
                return;
            }
            this.hLogger.ClearCommand();
        }
    }
}

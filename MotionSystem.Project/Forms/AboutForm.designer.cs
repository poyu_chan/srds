﻿namespace MotionSystem.Project.Forms
{
    partial class AboutForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOK = new System.Windows.Forms.Button();
            this.lblVesionInfo = new System.Windows.Forms.Label();
            this.txtVesionInfo = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(437, 284);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 0;
            this.btnOK.Text = "確定";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // lblVesionInfo
            // 
            this.lblVesionInfo.AutoSize = true;
            this.lblVesionInfo.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lblVesionInfo.ForeColor = System.Drawing.Color.Blue;
            this.lblVesionInfo.Location = new System.Drawing.Point(12, 9);
            this.lblVesionInfo.Name = "lblVesionInfo";
            this.lblVesionInfo.Size = new System.Drawing.Size(76, 16);
            this.lblVesionInfo.TabIndex = 1;
            this.lblVesionInfo.Text = "組件訊息";
            // 
            // txtVesionInfo
            // 
            this.txtVesionInfo.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.txtVesionInfo.Location = new System.Drawing.Point(12, 28);
            this.txtVesionInfo.Multiline = true;
            this.txtVesionInfo.Name = "txtVesionInfo";
            this.txtVesionInfo.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtVesionInfo.Size = new System.Drawing.Size(500, 250);
            this.txtVesionInfo.TabIndex = 2;
            // 
            // AboutForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(523, 314);
            this.Controls.Add(this.txtVesionInfo);
            this.Controls.Add(this.lblVesionInfo);
            this.Controls.Add(this.btnOK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "AboutForm";
            this.Text = "About Form";
            this.Load += new System.EventHandler(this.AboutForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Label lblVesionInfo;
        private System.Windows.Forms.TextBox txtVesionInfo;
    }
}
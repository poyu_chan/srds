﻿namespace MotionSystem.Project.Forms
{
    partial class AllHomeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AllHomeForm));
            this.btnJogP = new System.Windows.Forms.Button();
            this.btnJogN = new System.Windows.Forms.Button();
            this.btnHome = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.ntxbFeedBackCounter = new AOISystem.Utilities.Forms.NumTextBox();
            this.ntxbCommandCouter = new AOISystem.Utilities.Forms.NumTextBox();
            this.ldrLimitP = new AOISystem.Utilities.Forms.LedRectangle();
            this.ldrLimitN = new AOISystem.Utilities.Forms.LedRectangle();
            this.ldrAlarm = new AOISystem.Utilities.Forms.LedRectangle();
            this.btnCancel = new System.Windows.Forms.Button();
            this.tmrScan = new System.Windows.Forms.Timer(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.grpMotor = new System.Windows.Forms.GroupBox();
            this.radLoaderRightZ = new System.Windows.Forms.RadioButton();
            this.radLoaderRightX = new System.Windows.Forms.RadioButton();
            this.radLoaderLeftZ = new System.Windows.Forms.RadioButton();
            this.radLoaderLeftX = new System.Windows.Forms.RadioButton();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tpLoader = new System.Windows.Forms.TabPage();
            this.tpHome = new System.Windows.Forms.TabPage();
            this.flowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.ledCV1 = new AOISystem.Utilities.Forms.LedRectangle();
            this.btnABORT = new AOISystem.Utilities.Forms.DelayTimeButton();
            this.ledAOIHomeFlow = new AOISystem.Utilities.Forms.LedRectangle();
            this.panel1.SuspendLayout();
            this.grpMotor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tabControl.SuspendLayout();
            this.tpLoader.SuspendLayout();
            this.tpHome.SuspendLayout();
            this.flowLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnJogP
            // 
            resources.ApplyResources(this.btnJogP, "btnJogP");
            this.btnJogP.Name = "btnJogP";
            this.btnJogP.UseVisualStyleBackColor = true;
            this.btnJogP.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnJogP_MouseDown);
            this.btnJogP.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnStop_Click);
            // 
            // btnJogN
            // 
            resources.ApplyResources(this.btnJogN, "btnJogN");
            this.btnJogN.Name = "btnJogN";
            this.btnJogN.UseVisualStyleBackColor = true;
            this.btnJogN.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnJogN_MouseDown);
            this.btnJogN.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnStop_Click);
            // 
            // btnHome
            // 
            resources.ApplyResources(this.btnHome, "btnHome");
            this.btnHome.Name = "btnHome";
            this.btnHome.UseVisualStyleBackColor = true;
            this.btnHome.Click += new System.EventHandler(this.btnHome_Click);
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // ntxbFeedBackCounter
            // 
            resources.ApplyResources(this.ntxbFeedBackCounter, "ntxbFeedBackCounter");
            this.ntxbFeedBackCounter.FilterFlag = AOISystem.Utilities.Forms.FilterType.NumericPos;
            this.ntxbFeedBackCounter.Name = "ntxbFeedBackCounter";
            this.ntxbFeedBackCounter.ReadOnly = true;
            this.ntxbFeedBackCounter.TabStop = false;
            // 
            // ntxbCommandCouter
            // 
            resources.ApplyResources(this.ntxbCommandCouter, "ntxbCommandCouter");
            this.ntxbCommandCouter.FilterFlag = AOISystem.Utilities.Forms.FilterType.NumericPos;
            this.ntxbCommandCouter.Name = "ntxbCommandCouter";
            this.ntxbCommandCouter.ReadOnly = true;
            this.ntxbCommandCouter.TabStop = false;
            // 
            // ldrLimitP
            // 
            resources.ApplyResources(this.ldrLimitP, "ldrLimitP");
            this.ldrLimitP.Name = "ldrLimitP";
            this.ldrLimitP.On = false;
            this.ldrLimitP.TextColor = System.Drawing.Color.MediumVioletRed;
            this.ldrLimitP.TextFont = new System.Drawing.Font("微軟正黑體", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.ldrLimitP.TextVisible = true;
            // 
            // ldrLimitN
            // 
            resources.ApplyResources(this.ldrLimitN, "ldrLimitN");
            this.ldrLimitN.Name = "ldrLimitN";
            this.ldrLimitN.On = false;
            this.ldrLimitN.TextColor = System.Drawing.Color.MediumVioletRed;
            this.ldrLimitN.TextFont = new System.Drawing.Font("微軟正黑體", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.ldrLimitN.TextVisible = true;
            // 
            // ldrAlarm
            // 
            resources.ApplyResources(this.ldrAlarm, "ldrAlarm");
            this.ldrAlarm.Name = "ldrAlarm";
            this.ldrAlarm.On = false;
            this.ldrAlarm.TextColor = System.Drawing.Color.MediumVioletRed;
            this.ldrAlarm.TextFont = new System.Drawing.Font("微軟正黑體", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.ldrAlarm.TextVisible = true;
            // 
            // btnCancel
            // 
            resources.ApplyResources(this.btnCancel, "btnCancel");
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // tmrScan
            // 
            this.tmrScan.Interval = 50;
            this.tmrScan.Tick += new System.EventHandler(this.tmrScan_Tick);
            // 
            // panel1
            // 
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.grpMotor);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.ntxbFeedBackCounter);
            this.panel1.Controls.Add(this.ntxbCommandCouter);
            this.panel1.Controls.Add(this.ldrLimitP);
            this.panel1.Controls.Add(this.ldrLimitN);
            this.panel1.Controls.Add(this.ldrAlarm);
            this.panel1.Controls.Add(this.btnJogN);
            this.panel1.Controls.Add(this.btnJogP);
            this.panel1.Name = "panel1";
            // 
            // grpMotor
            // 
            resources.ApplyResources(this.grpMotor, "grpMotor");
            this.grpMotor.Controls.Add(this.radLoaderRightZ);
            this.grpMotor.Controls.Add(this.radLoaderRightX);
            this.grpMotor.Controls.Add(this.radLoaderLeftZ);
            this.grpMotor.Controls.Add(this.radLoaderLeftX);
            this.grpMotor.Name = "grpMotor";
            this.grpMotor.TabStop = false;
            // 
            // radLoaderRightZ
            // 
            resources.ApplyResources(this.radLoaderRightZ, "radLoaderRightZ");
            this.radLoaderRightZ.Name = "radLoaderRightZ";
            this.radLoaderRightZ.UseVisualStyleBackColor = true;
            this.radLoaderRightZ.CheckedChanged += new System.EventHandler(this.GetMotor);
            // 
            // radLoaderRightX
            // 
            resources.ApplyResources(this.radLoaderRightX, "radLoaderRightX");
            this.radLoaderRightX.Name = "radLoaderRightX";
            this.radLoaderRightX.UseVisualStyleBackColor = true;
            this.radLoaderRightX.CheckedChanged += new System.EventHandler(this.GetMotor);
            // 
            // radLoaderLeftZ
            // 
            resources.ApplyResources(this.radLoaderLeftZ, "radLoaderLeftZ");
            this.radLoaderLeftZ.Name = "radLoaderLeftZ";
            this.radLoaderLeftZ.UseVisualStyleBackColor = true;
            this.radLoaderLeftZ.CheckedChanged += new System.EventHandler(this.GetMotor);
            // 
            // radLoaderLeftX
            // 
            resources.ApplyResources(this.radLoaderLeftX, "radLoaderLeftX");
            this.radLoaderLeftX.Checked = true;
            this.radLoaderLeftX.Name = "radLoaderLeftX";
            this.radLoaderLeftX.TabStop = true;
            this.radLoaderLeftX.UseVisualStyleBackColor = true;
            this.radLoaderLeftX.CheckedChanged += new System.EventHandler(this.GetMotor);
            // 
            // pictureBox1
            // 
            resources.ApplyResources(this.pictureBox1, "pictureBox1");
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.TabStop = false;
            // 
            // tabControl
            // 
            resources.ApplyResources(this.tabControl, "tabControl");
            this.tabControl.Controls.Add(this.tpLoader);
            this.tabControl.Controls.Add(this.tpHome);
            this.tabControl.Multiline = true;
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            // 
            // tpLoader
            // 
            resources.ApplyResources(this.tpLoader, "tpLoader");
            this.tpLoader.Controls.Add(this.panel1);
            this.tpLoader.Controls.Add(this.btnHome);
            this.tpLoader.Controls.Add(this.pictureBox1);
            this.tpLoader.Controls.Add(this.btnCancel);
            this.tpLoader.Name = "tpLoader";
            this.tpLoader.UseVisualStyleBackColor = true;
            // 
            // tpHome
            // 
            resources.ApplyResources(this.tpHome, "tpHome");
            this.tpHome.Controls.Add(this.flowLayoutPanel);
            this.tpHome.Controls.Add(this.btnABORT);
            this.tpHome.Controls.Add(this.ledAOIHomeFlow);
            this.tpHome.Name = "tpHome";
            this.tpHome.UseVisualStyleBackColor = true;
            // 
            // flowLayoutPanel
            // 
            resources.ApplyResources(this.flowLayoutPanel, "flowLayoutPanel");
            this.flowLayoutPanel.Controls.Add(this.ledCV1);
            this.flowLayoutPanel.Name = "flowLayoutPanel";
            // 
            // ledCV1
            // 
            resources.ApplyResources(this.ledCV1, "ledCV1");
            this.ledCV1.Name = "ledCV1";
            this.ledCV1.On = false;
            this.ledCV1.Tag = "";
            this.ledCV1.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ledCV1.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold);
            this.ledCV1.TextVisible = true;
            this.ledCV1.DoubleClick += new System.EventHandler(this.ledIMotion_DoubleClick);
            // 
            // btnABORT
            // 
            resources.ApplyResources(this.btnABORT, "btnABORT");
            this.btnABORT.AccountLevel = AOISystem.Utilities.Account.AccountLevel.Engineer;
            this.btnABORT.BackColor = System.Drawing.Color.Red;
            this.btnABORT.DelayTime = 0D;
            this.btnABORT.ForeColor = System.Drawing.Color.White;
            this.btnABORT.Name = "btnABORT";
            this.btnABORT.TestPermission = false;
            this.btnABORT.UseVisualStyleBackColor = false;
            this.btnABORT.Click += new System.EventHandler(this.btnABORT_Click);
            // 
            // ledAOIHomeFlow
            // 
            resources.ApplyResources(this.ledAOIHomeFlow, "ledAOIHomeFlow");
            this.ledAOIHomeFlow.Color = System.Drawing.Color.Yellow;
            this.ledAOIHomeFlow.Name = "ledAOIHomeFlow";
            this.ledAOIHomeFlow.On = false;
            this.ledAOIHomeFlow.Tag = "";
            this.ledAOIHomeFlow.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ledAOIHomeFlow.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold);
            this.ledAOIHomeFlow.TextVisible = true;
            this.ledAOIHomeFlow.DoubleClick += new System.EventHandler(this.ledAOIHomeFlow_DoubleClick);
            // 
            // AllHomeForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tabControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AllHomeForm";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AllHomeForm_FormClosing);
            this.Load += new System.EventHandler(this.AllHomeForm_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.grpMotor.ResumeLayout(false);
            this.grpMotor.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tabControl.ResumeLayout(false);
            this.tpLoader.ResumeLayout(false);
            this.tpHome.ResumeLayout(false);
            this.flowLayoutPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnJogP;
        private System.Windows.Forms.Button btnJogN;
        private System.Windows.Forms.Button btnHome;
        private AOISystem.Utilities.Forms.LedRectangle ldrAlarm;
        private AOISystem.Utilities.Forms.LedRectangle ldrLimitP;
        private AOISystem.Utilities.Forms.LedRectangle ldrLimitN;
        private AOISystem.Utilities.Forms.NumTextBox ntxbCommandCouter;
        private AOISystem.Utilities.Forms.NumTextBox ntxbFeedBackCounter;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Timer tmrScan;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox grpMotor;
        private System.Windows.Forms.RadioButton radLoaderRightZ;
        private System.Windows.Forms.RadioButton radLoaderRightX;
        private System.Windows.Forms.RadioButton radLoaderLeftZ;
        private System.Windows.Forms.RadioButton radLoaderLeftX;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tpHome;
        private System.Windows.Forms.TabPage tpLoader;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel;
        private AOISystem.Utilities.Forms.LedRectangle ledCV1;
        private AOISystem.Utilities.Forms.DelayTimeButton btnABORT;
        private AOISystem.Utilities.Forms.LedRectangle ledAOIHomeFlow;
    }
}
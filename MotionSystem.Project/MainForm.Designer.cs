﻿namespace MotionSystem.Project
{
    partial class MainForm
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器
        /// 修改這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.splViewer = new AOISystem.Utilities.Forms.SplitContainerEx(this.components);
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tpMain = new System.Windows.Forms.TabPage();
            this.grpInformation = new System.Windows.Forms.GroupBox();
            this.txtVersion = new System.Windows.Forms.TextBox();
            this.lblVersionLabel = new System.Windows.Forms.Label();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.lblDescription = new System.Windows.Forms.Label();
            this.txtNowTime = new System.Windows.Forms.TextBox();
            this.txtUserID = new System.Windows.Forms.TextBox();
            this.lblUserID = new System.Windows.Forms.Label();
            this.lblNowTimeLabel = new System.Windows.Forms.Label();
            this.txtRecipeID = new System.Windows.Forms.TextBox();
            this.txtRecipeNo = new System.Windows.Forms.TextBox();
            this.lblRecipeNoLabel = new System.Windows.Forms.Label();
            this.tpIO = new System.Windows.Forms.TabPage();
            this.grpIOOutput = new System.Windows.Forms.GroupBox();
            this.flpIOOutput = new System.Windows.Forms.FlowLayoutPanel();
            this.grpIOInput = new System.Windows.Forms.GroupBox();
            this.flpIOInput = new System.Windows.Forms.FlowLayoutPanel();
            this.tpMotion = new System.Windows.Forms.TabPage();
            this.tabMotion = new System.Windows.Forms.TabControl();
            this.tpAOI = new System.Windows.Forms.TabPage();
            this.pnlAOISystemParam = new System.Windows.Forms.Panel();
            this.tabControl4 = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel19 = new System.Windows.Forms.FlowLayoutPanel();
            this.rncAvailableRAMMinFreeSpace = new AOISystem.Utilities.Recipe.RecipeNumericUpDownControl();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel20 = new System.Windows.Forms.FlowLayoutPanel();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.flowLayoutPanel25 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnSystemMotionPositionRestore = new System.Windows.Forms.Button();
            this.btnSystemMotionPositionSave = new System.Windows.Forms.Button();
            this.btnCV1 = new System.Windows.Forms.Button();
            this.tpDatabase = new System.Windows.Forms.TabPage();
            this.grpMeasureResult = new System.Windows.Forms.GroupBox();
            this.tpRecipe = new System.Windows.Forms.TabPage();
            this.tlpRecipe = new System.Windows.Forms.TableLayoutPanel();
            this.tabRecipe = new System.Windows.Forms.TabControl();
            this.tpOther = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanel10 = new System.Windows.Forms.FlowLayoutPanel();
            this.rtcExtraMaxDistance = new AOISystem.Utilities.Recipe.RecipeTextControl();
            this.rtcExtraSurvivalTime = new AOISystem.Utilities.Recipe.RecipeTextControl();
            this.rtcClearOffFlowContiunanceTime = new AOISystem.Utilities.Recipe.RecipeTextControl();
            this.flowLayoutPanel11 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel12 = new System.Windows.Forms.FlowLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.btnRecipeRestore = new System.Windows.Forms.Button();
            this.btnRecipeSave = new System.Windows.Forms.Button();
            this.recipeEditorControl = new AOISystem.Utilities.Recipe.RecipeEditorControl();
            this.tpSystem = new System.Windows.Forms.TabPage();
            this.btnSaveSystemData = new System.Windows.Forms.Button();
            this.flpSystem = new System.Windows.Forms.FlowLayoutPanel();
            this.recipeLongTextControl1 = new AOISystem.Utilities.Recipe.RecipeLongTextControl();
            this.recipeLongTextControl2 = new AOISystem.Utilities.Recipe.RecipeLongTextControl();
            this.recipeBooleanControl5 = new AOISystem.Utilities.Recipe.RecipeBooleanControl();
            this.tpTest = new System.Windows.Forms.TabPage();
            this.tabManualFlowControl = new System.Windows.Forms.TabControl();
            this.tpQuickStart = new System.Windows.Forms.TabPage();
            this.pnlQuickStart = new AOISystem.Utilities.Forms.ScrollablePanel();
            this.pnlQuickStartInner = new System.Windows.Forms.Panel();
            this.btnByPassConfiguration = new System.Windows.Forms.Button();
            this.btnSystemSetting = new System.Windows.Forms.Button();
            this.tpTools = new System.Windows.Forms.TabPage();
            this.grpDeveloperTools = new System.Windows.Forms.GroupBox();
            this.lblDeveloperTools = new System.Windows.Forms.Label();
            this.tpCommunication = new System.Windows.Forms.TabPage();
            this.tlpCommunication = new System.Windows.Forms.TableLayoutPanel();
            this.panel = new System.Windows.Forms.Panel();
            this.tpMutiLanguage = new System.Windows.Forms.TabPage();
            this.btnMutiLanguage = new System.Windows.Forms.Button();
            this.safeDoorCtl1 = new AOISystem.Utilities.Recipe.SafeDoorCtl();
            this.dataRecordControl1 = new AOISystem.Utilities.Recipe.DataRecordControl();
            this.nudMutiLanguage = new AOISystem.Utilities.Forms.NumericUpDownSlider();
            this.ledMutiLanguage = new AOISystem.Utilities.Forms.LedRectangle();
            this.tabMutiLanguage = new System.Windows.Forms.TabControl();
            this.tpEnglishItem1 = new System.Windows.Forms.TabPage();
            this.tpEnglishItem2 = new System.Windows.Forms.TabPage();
            this.treeMutiLanguage = new System.Windows.Forms.TreeView();
            this.rdoMutiLanguage = new System.Windows.Forms.RadioButton();
            this.listMutiLanguage = new System.Windows.Forms.ListBox();
            this.cboMutiLanguage = new System.Windows.Forms.ComboBox();
            this.chklistMutiLanguage = new System.Windows.Forms.CheckedListBox();
            this.chkMutiLanguage = new System.Windows.Forms.CheckBox();
            this.lblMutiLanguage = new System.Windows.Forms.Label();
            this.txtMutiLanguage = new System.Windows.Forms.TextBox();
            this.tabMessage = new System.Windows.Forms.TabControl();
            this.tpInfoformation = new System.Windows.Forms.TabPage();
            this.lsvErrorMessage = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tpLogging = new System.Windows.Forms.TabPage();
            this.hLogger = new AOISystem.Utilities.Logging.HLogger();
            this.msMenu = new System.Windows.Forms.MenuStrip();
            this.tsmiFile = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiSystemConfiguration = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiRecipeConfiguration = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiUserLogInOut = new System.Windows.Forms.ToolStripMenuItem();
            this.tspmiLogIn = new System.Windows.Forms.ToolStripMenuItem();
            this.tspmiLogOut = new System.Windows.Forms.ToolStripMenuItem();
            this.tspmiAccountEditor = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiRecipe = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiLanguage = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiTraditionalChinese = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiEnglish = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.tmrSystem = new System.Windows.Forms.Timer(this.components);
            this.tlpToolBar = new System.Windows.Forms.TableLayoutPanel();
            this.tlpSystemStatus = new System.Windows.Forms.TableLayoutPanel();
            this.ledDown = new AOISystem.Utilities.Forms.LedRectangle();
            this.ledWarning = new AOISystem.Utilities.Forms.LedRectangle();
            this.ledRun = new AOISystem.Utilities.Forms.LedRectangle();
            this.flpToolBarSub2 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnStartFlow = new AOISystem.Utilities.Forms.DelayTimeButton();
            this.btnPauseFlow = new AOISystem.Utilities.Forms.DelayTimeButton();
            this.btnAlarmReset = new AOISystem.Utilities.Forms.DelayTimeButton();
            this.btnMute = new AOISystem.Utilities.Forms.DelayTimeButton();
            this.btnStopFlow = new AOISystem.Utilities.Forms.DelayTimeButton();
            this.btnLoadRecipeFlow = new AOISystem.Utilities.Forms.DelayTimeButton();
            this.btnHome = new AOISystem.Utilities.Forms.DelayTimeButton();
            this.flpToolBarSub1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnMain = new System.Windows.Forms.Button();
            this.btnIO = new System.Windows.Forms.Button();
            this.btnMotion = new System.Windows.Forms.Button();
            this.btnDatabase = new System.Windows.Forms.Button();
            this.btnRecipe = new System.Windows.Forms.Button();
            this.btnSystem = new System.Windows.Forms.Button();
            this.btnTest = new System.Windows.Forms.Button();
            this.tlpConnecteStatus = new System.Windows.Forms.TableLayoutPanel();
            this.ledAOI1 = new AOISystem.Utilities.Forms.LedRectangle();
            this.ssStatusBar = new System.Windows.Forms.StatusStrip();
            this.lblRecipeNo = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblRecipeID = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblCPU = new System.Windows.Forms.ToolStripStatusLabel();
            this.prgCPU = new System.Windows.Forms.ToolStripProgressBar();
            this.lblRAM = new System.Windows.Forms.ToolStripStatusLabel();
            this.prgRAM = new System.Windows.Forms.ToolStripProgressBar();
            this.lblDISK = new System.Windows.Forms.ToolStripStatusLabel();
            this.prgDISK = new System.Windows.Forms.ToolStripProgressBar();
            this.lblSpare = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblAccountName = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblAccountLevel = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblVersion = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblNowTime = new System.Windows.Forms.ToolStripStatusLabel();
            this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
            this.tmrLogOut = new System.Windows.Forms.Timer(this.components);
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.modulesIMotionControl1 = new MotionSystem.Project.Forms.ModulesIMotionControl();
            ((System.ComponentModel.ISupportInitialize)(this.splViewer)).BeginInit();
            this.splViewer.Panel1.SuspendLayout();
            this.splViewer.Panel2.SuspendLayout();
            this.splViewer.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.tpMain.SuspendLayout();
            this.grpInformation.SuspendLayout();
            this.tpIO.SuspendLayout();
            this.grpIOOutput.SuspendLayout();
            this.grpIOInput.SuspendLayout();
            this.tpMotion.SuspendLayout();
            this.tabMotion.SuspendLayout();
            this.tpAOI.SuspendLayout();
            this.pnlAOISystemParam.SuspendLayout();
            this.tabControl4.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.flowLayoutPanel19.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tpDatabase.SuspendLayout();
            this.tpRecipe.SuspendLayout();
            this.tlpRecipe.SuspendLayout();
            this.tabRecipe.SuspendLayout();
            this.tpOther.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.flowLayoutPanel10.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tpSystem.SuspendLayout();
            this.flpSystem.SuspendLayout();
            this.tpTest.SuspendLayout();
            this.tabManualFlowControl.SuspendLayout();
            this.tpQuickStart.SuspendLayout();
            this.pnlQuickStart.SuspendLayout();
            this.pnlQuickStartInner.SuspendLayout();
            this.tpTools.SuspendLayout();
            this.tpCommunication.SuspendLayout();
            this.tlpCommunication.SuspendLayout();
            this.tpMutiLanguage.SuspendLayout();
            this.tabMutiLanguage.SuspendLayout();
            this.tabMessage.SuspendLayout();
            this.tpInfoformation.SuspendLayout();
            this.tpLogging.SuspendLayout();
            this.msMenu.SuspendLayout();
            this.tlpToolBar.SuspendLayout();
            this.tlpSystemStatus.SuspendLayout();
            this.flpToolBarSub2.SuspendLayout();
            this.flpToolBarSub1.SuspendLayout();
            this.tlpConnecteStatus.SuspendLayout();
            this.ssStatusBar.SuspendLayout();
            this.tlpMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // splViewer
            // 
            this.splViewer.BackColor = System.Drawing.SystemColors.Control;
            this.splViewer.CollpaseOrExpandSize = 160;
            this.splViewer.Cursor = System.Windows.Forms.Cursors.Default;
            resources.ApplyResources(this.splViewer, "splViewer");
            this.splViewer.IsCollpased = false;
            this.splViewer.IsCollpaseOrExpandFixed = false;
            this.splViewer.Name = "splViewer";
            // 
            // splViewer.Panel1
            // 
            this.splViewer.Panel1.Controls.Add(this.tabControl);
            // 
            // splViewer.Panel2
            // 
            this.splViewer.Panel2.Controls.Add(this.tabMessage);
            // 
            // tabControl
            // 
            resources.ApplyResources(this.tabControl, "tabControl");
            this.tabControl.Controls.Add(this.tpMain);
            this.tabControl.Controls.Add(this.tpIO);
            this.tabControl.Controls.Add(this.tpMotion);
            this.tabControl.Controls.Add(this.tpDatabase);
            this.tabControl.Controls.Add(this.tpRecipe);
            this.tabControl.Controls.Add(this.tpSystem);
            this.tabControl.Controls.Add(this.tpTest);
            this.tabControl.Multiline = true;
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            // 
            // tpMain
            // 
            this.tpMain.Controls.Add(this.grpInformation);
            resources.ApplyResources(this.tpMain, "tpMain");
            this.tpMain.Name = "tpMain";
            this.tpMain.UseVisualStyleBackColor = true;
            // 
            // grpInformation
            // 
            this.grpInformation.Controls.Add(this.txtVersion);
            this.grpInformation.Controls.Add(this.lblVersionLabel);
            this.grpInformation.Controls.Add(this.txtDescription);
            this.grpInformation.Controls.Add(this.lblDescription);
            this.grpInformation.Controls.Add(this.txtNowTime);
            this.grpInformation.Controls.Add(this.txtUserID);
            this.grpInformation.Controls.Add(this.lblUserID);
            this.grpInformation.Controls.Add(this.lblNowTimeLabel);
            this.grpInformation.Controls.Add(this.txtRecipeID);
            this.grpInformation.Controls.Add(this.txtRecipeNo);
            this.grpInformation.Controls.Add(this.lblRecipeNoLabel);
            resources.ApplyResources(this.grpInformation, "grpInformation");
            this.grpInformation.ForeColor = System.Drawing.Color.Blue;
            this.grpInformation.Name = "grpInformation";
            this.grpInformation.TabStop = false;
            // 
            // txtVersion
            // 
            this.txtVersion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtVersion, "txtVersion");
            this.txtVersion.ForeColor = System.Drawing.Color.Black;
            this.txtVersion.Name = "txtVersion";
            this.txtVersion.ReadOnly = true;
            // 
            // lblVersionLabel
            // 
            resources.ApplyResources(this.lblVersionLabel, "lblVersionLabel");
            this.lblVersionLabel.ForeColor = System.Drawing.Color.Black;
            this.lblVersionLabel.Name = "lblVersionLabel";
            // 
            // txtDescription
            // 
            this.txtDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtDescription, "txtDescription");
            this.txtDescription.ForeColor = System.Drawing.Color.Black;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.ReadOnly = true;
            // 
            // lblDescription
            // 
            resources.ApplyResources(this.lblDescription, "lblDescription");
            this.lblDescription.ForeColor = System.Drawing.Color.Black;
            this.lblDescription.Name = "lblDescription";
            // 
            // txtNowTime
            // 
            this.txtNowTime.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtNowTime, "txtNowTime");
            this.txtNowTime.ForeColor = System.Drawing.Color.Black;
            this.txtNowTime.Name = "txtNowTime";
            this.txtNowTime.ReadOnly = true;
            // 
            // txtUserID
            // 
            this.txtUserID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtUserID, "txtUserID");
            this.txtUserID.ForeColor = System.Drawing.Color.Black;
            this.txtUserID.Name = "txtUserID";
            this.txtUserID.ReadOnly = true;
            // 
            // lblUserID
            // 
            resources.ApplyResources(this.lblUserID, "lblUserID");
            this.lblUserID.ForeColor = System.Drawing.Color.Black;
            this.lblUserID.Name = "lblUserID";
            // 
            // lblNowTimeLabel
            // 
            resources.ApplyResources(this.lblNowTimeLabel, "lblNowTimeLabel");
            this.lblNowTimeLabel.ForeColor = System.Drawing.Color.Black;
            this.lblNowTimeLabel.Name = "lblNowTimeLabel";
            // 
            // txtRecipeID
            // 
            this.txtRecipeID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtRecipeID, "txtRecipeID");
            this.txtRecipeID.ForeColor = System.Drawing.Color.Black;
            this.txtRecipeID.Name = "txtRecipeID";
            this.txtRecipeID.ReadOnly = true;
            // 
            // txtRecipeNo
            // 
            this.txtRecipeNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtRecipeNo, "txtRecipeNo");
            this.txtRecipeNo.ForeColor = System.Drawing.Color.Black;
            this.txtRecipeNo.Name = "txtRecipeNo";
            this.txtRecipeNo.ReadOnly = true;
            // 
            // lblRecipeNoLabel
            // 
            resources.ApplyResources(this.lblRecipeNoLabel, "lblRecipeNoLabel");
            this.lblRecipeNoLabel.ForeColor = System.Drawing.Color.Black;
            this.lblRecipeNoLabel.Name = "lblRecipeNoLabel";
            // 
            // tpIO
            // 
            this.tpIO.Controls.Add(this.grpIOOutput);
            this.tpIO.Controls.Add(this.grpIOInput);
            resources.ApplyResources(this.tpIO, "tpIO");
            this.tpIO.Name = "tpIO";
            this.tpIO.UseVisualStyleBackColor = true;
            // 
            // grpIOOutput
            // 
            this.grpIOOutput.Controls.Add(this.flpIOOutput);
            resources.ApplyResources(this.grpIOOutput, "grpIOOutput");
            this.grpIOOutput.Name = "grpIOOutput";
            this.grpIOOutput.TabStop = false;
            // 
            // flpIOOutput
            // 
            resources.ApplyResources(this.flpIOOutput, "flpIOOutput");
            this.flpIOOutput.Name = "flpIOOutput";
            // 
            // grpIOInput
            // 
            this.grpIOInput.Controls.Add(this.flpIOInput);
            resources.ApplyResources(this.grpIOInput, "grpIOInput");
            this.grpIOInput.Name = "grpIOInput";
            this.grpIOInput.TabStop = false;
            // 
            // flpIOInput
            // 
            resources.ApplyResources(this.flpIOInput, "flpIOInput");
            this.flpIOInput.Name = "flpIOInput";
            // 
            // tpMotion
            // 
            this.tpMotion.Controls.Add(this.tabMotion);
            resources.ApplyResources(this.tpMotion, "tpMotion");
            this.tpMotion.Name = "tpMotion";
            this.tpMotion.UseVisualStyleBackColor = true;
            // 
            // tabMotion
            // 
            this.tabMotion.Controls.Add(this.tpAOI);
            resources.ApplyResources(this.tabMotion, "tabMotion");
            this.tabMotion.Name = "tabMotion";
            this.tabMotion.SelectedIndex = 0;
            // 
            // tpAOI
            // 
            this.tpAOI.Controls.Add(this.pnlAOISystemParam);
            this.tpAOI.Controls.Add(this.btnCV1);
            resources.ApplyResources(this.tpAOI, "tpAOI");
            this.tpAOI.Name = "tpAOI";
            this.tpAOI.UseVisualStyleBackColor = true;
            // 
            // pnlAOISystemParam
            // 
            this.pnlAOISystemParam.BackColor = System.Drawing.Color.DarkGray;
            this.pnlAOISystemParam.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlAOISystemParam.Controls.Add(this.tabControl4);
            this.pnlAOISystemParam.Controls.Add(this.btnSystemMotionPositionRestore);
            this.pnlAOISystemParam.Controls.Add(this.btnSystemMotionPositionSave);
            resources.ApplyResources(this.pnlAOISystemParam, "pnlAOISystemParam");
            this.pnlAOISystemParam.Name = "pnlAOISystemParam";
            // 
            // tabControl4
            // 
            resources.ApplyResources(this.tabControl4, "tabControl4");
            this.tabControl4.Controls.Add(this.tabPage3);
            this.tabControl4.Controls.Add(this.tabPage4);
            this.tabControl4.Name = "tabControl4";
            this.tabControl4.SelectedIndex = 0;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.groupBox6);
            this.tabPage3.Controls.Add(this.groupBox7);
            resources.ApplyResources(this.tabPage3, "tabPage3");
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.flowLayoutPanel19);
            resources.ApplyResources(this.groupBox6, "groupBox6");
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.TabStop = false;
            // 
            // flowLayoutPanel19
            // 
            resources.ApplyResources(this.flowLayoutPanel19, "flowLayoutPanel19");
            this.flowLayoutPanel19.BackColor = System.Drawing.Color.LightGray;
            this.flowLayoutPanel19.Controls.Add(this.rncAvailableRAMMinFreeSpace);
            this.flowLayoutPanel19.Name = "flowLayoutPanel19";
            // 
            // rncAvailableRAMMinFreeSpace
            // 
            this.rncAvailableRAMMinFreeSpace.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.rncAvailableRAMMinFreeSpace.DecimalPlaces = 2;
            resources.ApplyResources(this.rncAvailableRAMMinFreeSpace, "rncAvailableRAMMinFreeSpace");
            this.rncAvailableRAMMinFreeSpace.Increment = 1D;
            this.rncAvailableRAMMinFreeSpace.LabelFont = null;
            this.rncAvailableRAMMinFreeSpace.MaxNumber = 100D;
            this.rncAvailableRAMMinFreeSpace.MinNumber = 0D;
            this.rncAvailableRAMMinFreeSpace.Name = "rncAvailableRAMMinFreeSpace";
            this.rncAvailableRAMMinFreeSpace.Tag = "AvailableRAMMinFreeSpace";
            this.rncAvailableRAMMinFreeSpace.Value = 10D;
            this.rncAvailableRAMMinFreeSpace.ValueWidth = 63F;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.flowLayoutPanel20);
            resources.ApplyResources(this.groupBox7, "groupBox7");
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.TabStop = false;
            // 
            // flowLayoutPanel20
            // 
            resources.ApplyResources(this.flowLayoutPanel20, "flowLayoutPanel20");
            this.flowLayoutPanel20.BackColor = System.Drawing.Color.LightGray;
            this.flowLayoutPanel20.Name = "flowLayoutPanel20";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.flowLayoutPanel25);
            resources.ApplyResources(this.tabPage4, "tabPage4");
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // flowLayoutPanel25
            // 
            resources.ApplyResources(this.flowLayoutPanel25, "flowLayoutPanel25");
            this.flowLayoutPanel25.BackColor = System.Drawing.Color.LightGray;
            this.flowLayoutPanel25.Name = "flowLayoutPanel25";
            // 
            // btnSystemMotionPositionRestore
            // 
            this.btnSystemMotionPositionRestore.BackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.btnSystemMotionPositionRestore, "btnSystemMotionPositionRestore");
            this.btnSystemMotionPositionRestore.ForeColor = System.Drawing.Color.Black;
            this.btnSystemMotionPositionRestore.Name = "btnSystemMotionPositionRestore";
            this.btnSystemMotionPositionRestore.UseVisualStyleBackColor = false;
            this.btnSystemMotionPositionRestore.Click += new System.EventHandler(this.btnSystemMotionPositionRestore_Click);
            // 
            // btnSystemMotionPositionSave
            // 
            this.btnSystemMotionPositionSave.BackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.btnSystemMotionPositionSave, "btnSystemMotionPositionSave");
            this.btnSystemMotionPositionSave.ForeColor = System.Drawing.Color.Black;
            this.btnSystemMotionPositionSave.Name = "btnSystemMotionPositionSave";
            this.btnSystemMotionPositionSave.UseVisualStyleBackColor = false;
            this.btnSystemMotionPositionSave.Click += new System.EventHandler(this.btnSystemMotionPositionSave_Click);
            // 
            // btnCV1
            // 
            resources.ApplyResources(this.btnCV1, "btnCV1");
            this.btnCV1.Name = "btnCV1";
            this.btnCV1.UseVisualStyleBackColor = true;
            this.btnCV1.Click += new System.EventHandler(this.ShowMotorConfiguration_Click);
            // 
            // tpDatabase
            // 
            this.tpDatabase.Controls.Add(this.grpMeasureResult);
            resources.ApplyResources(this.tpDatabase, "tpDatabase");
            this.tpDatabase.Name = "tpDatabase";
            this.tpDatabase.UseVisualStyleBackColor = true;
            // 
            // grpMeasureResult
            // 
            resources.ApplyResources(this.grpMeasureResult, "grpMeasureResult");
            this.grpMeasureResult.ForeColor = System.Drawing.SystemColors.WindowText;
            this.grpMeasureResult.Name = "grpMeasureResult";
            this.grpMeasureResult.TabStop = false;
            // 
            // tpRecipe
            // 
            this.tpRecipe.Controls.Add(this.tlpRecipe);
            resources.ApplyResources(this.tpRecipe, "tpRecipe");
            this.tpRecipe.Name = "tpRecipe";
            this.tpRecipe.UseVisualStyleBackColor = true;
            // 
            // tlpRecipe
            // 
            resources.ApplyResources(this.tlpRecipe, "tlpRecipe");
            this.tlpRecipe.Controls.Add(this.tabRecipe, 1, 1);
            this.tlpRecipe.Controls.Add(this.tableLayoutPanel2, 1, 0);
            this.tlpRecipe.Controls.Add(this.recipeEditorControl, 0, 0);
            this.tlpRecipe.Name = "tlpRecipe";
            // 
            // tabRecipe
            // 
            this.tabRecipe.Controls.Add(this.tpOther);
            resources.ApplyResources(this.tabRecipe, "tabRecipe");
            this.tabRecipe.Name = "tabRecipe";
            this.tabRecipe.SelectedIndex = 0;
            // 
            // tpOther
            // 
            this.tpOther.Controls.Add(this.tableLayoutPanel6);
            resources.ApplyResources(this.tpOther, "tpOther");
            this.tpOther.Name = "tpOther";
            this.tpOther.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel6
            // 
            resources.ApplyResources(this.tableLayoutPanel6, "tableLayoutPanel6");
            this.tableLayoutPanel6.Controls.Add(this.flowLayoutPanel10, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.flowLayoutPanel11, 1, 0);
            this.tableLayoutPanel6.Controls.Add(this.flowLayoutPanel12, 2, 0);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            // 
            // flowLayoutPanel10
            // 
            this.flowLayoutPanel10.Controls.Add(this.rtcExtraMaxDistance);
            this.flowLayoutPanel10.Controls.Add(this.rtcExtraSurvivalTime);
            this.flowLayoutPanel10.Controls.Add(this.rtcClearOffFlowContiunanceTime);
            resources.ApplyResources(this.flowLayoutPanel10, "flowLayoutPanel10");
            this.flowLayoutPanel10.Name = "flowLayoutPanel10";
            // 
            // rtcExtraMaxDistance
            // 
            this.rtcExtraMaxDistance.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.rtcExtraMaxDistance, "rtcExtraMaxDistance");
            this.rtcExtraMaxDistance.LabelFont = new System.Drawing.Font("微軟正黑體", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.rtcExtraMaxDistance.Name = "rtcExtraMaxDistance";
            this.rtcExtraMaxDistance.Tag = "ExtraMaxDistance";
            this.rtcExtraMaxDistance.ToolTipDescription = "";
            this.rtcExtraMaxDistance.Value = 12345D;
            // 
            // rtcExtraSurvivalTime
            // 
            this.rtcExtraSurvivalTime.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.rtcExtraSurvivalTime, "rtcExtraSurvivalTime");
            this.rtcExtraSurvivalTime.LabelFont = new System.Drawing.Font("微軟正黑體", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.rtcExtraSurvivalTime.Name = "rtcExtraSurvivalTime";
            this.rtcExtraSurvivalTime.Tag = "ExtraSurvivalTime";
            this.rtcExtraSurvivalTime.ToolTipDescription = "";
            this.rtcExtraSurvivalTime.Value = 12345D;
            // 
            // rtcClearOffFlowContiunanceTime
            // 
            this.rtcClearOffFlowContiunanceTime.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.rtcClearOffFlowContiunanceTime, "rtcClearOffFlowContiunanceTime");
            this.rtcClearOffFlowContiunanceTime.LabelFont = new System.Drawing.Font("微軟正黑體", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.rtcClearOffFlowContiunanceTime.Name = "rtcClearOffFlowContiunanceTime";
            this.rtcClearOffFlowContiunanceTime.Tag = "ClearOffFlowContiunanceTime";
            this.rtcClearOffFlowContiunanceTime.ToolTipDescription = "";
            this.rtcClearOffFlowContiunanceTime.Value = 12345D;
            // 
            // flowLayoutPanel11
            // 
            resources.ApplyResources(this.flowLayoutPanel11, "flowLayoutPanel11");
            this.flowLayoutPanel11.Name = "flowLayoutPanel11";
            // 
            // flowLayoutPanel12
            // 
            resources.ApplyResources(this.flowLayoutPanel12, "flowLayoutPanel12");
            this.flowLayoutPanel12.Name = "flowLayoutPanel12";
            // 
            // tableLayoutPanel2
            // 
            resources.ApplyResources(this.tableLayoutPanel2, "tableLayoutPanel2");
            this.tableLayoutPanel2.Controls.Add(this.btnRecipeRestore, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnRecipeSave, 0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            // 
            // btnRecipeRestore
            // 
            resources.ApplyResources(this.btnRecipeRestore, "btnRecipeRestore");
            this.btnRecipeRestore.Name = "btnRecipeRestore";
            this.btnRecipeRestore.UseVisualStyleBackColor = true;
            this.btnRecipeRestore.Click += new System.EventHandler(this.btnRecipeRestore_Click);
            // 
            // btnRecipeSave
            // 
            resources.ApplyResources(this.btnRecipeSave, "btnRecipeSave");
            this.btnRecipeSave.Name = "btnRecipeSave";
            this.btnRecipeSave.UseVisualStyleBackColor = true;
            this.btnRecipeSave.Click += new System.EventHandler(this.btnRecipeSave_Click);
            // 
            // recipeEditorControl
            // 
            this.recipeEditorControl.ChangeRecipeTimeInterval = 10;
            resources.ApplyResources(this.recipeEditorControl, "recipeEditorControl");
            this.recipeEditorControl.IsShowChangeRecipeButton = false;
            this.recipeEditorControl.Name = "recipeEditorControl";
            this.tlpRecipe.SetRowSpan(this.recipeEditorControl, 2);
            this.recipeEditorControl.SelectedRecipeInfo = ((AOISystem.Utilities.Recipe.RecipeInfo)(resources.GetObject("recipeEditorControl.SelectedRecipeInfo")));
            // 
            // tpSystem
            // 
            this.tpSystem.Controls.Add(this.btnSaveSystemData);
            this.tpSystem.Controls.Add(this.flpSystem);
            resources.ApplyResources(this.tpSystem, "tpSystem");
            this.tpSystem.Name = "tpSystem";
            this.tpSystem.UseVisualStyleBackColor = true;
            // 
            // btnSaveSystemData
            // 
            resources.ApplyResources(this.btnSaveSystemData, "btnSaveSystemData");
            this.btnSaveSystemData.Name = "btnSaveSystemData";
            this.btnSaveSystemData.UseVisualStyleBackColor = true;
            this.btnSaveSystemData.Click += new System.EventHandler(this.btnSaveSystemData_Click);
            // 
            // flpSystem
            // 
            this.flpSystem.BackColor = System.Drawing.Color.LightGray;
            this.flpSystem.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flpSystem.Controls.Add(this.recipeLongTextControl1);
            this.flpSystem.Controls.Add(this.recipeLongTextControl2);
            this.flpSystem.Controls.Add(this.recipeBooleanControl5);
            resources.ApplyResources(this.flpSystem, "flpSystem");
            this.flpSystem.Name = "flpSystem";
            // 
            // recipeLongTextControl1
            // 
            this.recipeLongTextControl1.BackColor = System.Drawing.Color.CornflowerBlue;
            this.recipeLongTextControl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.recipeLongTextControl1, "recipeLongTextControl1");
            this.recipeLongTextControl1.LabelFont = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.recipeLongTextControl1.Name = "recipeLongTextControl1";
            this.recipeLongTextControl1.Tag = "FabName";
            this.recipeLongTextControl1.Value = "";
            // 
            // recipeLongTextControl2
            // 
            this.recipeLongTextControl2.BackColor = System.Drawing.Color.CornflowerBlue;
            this.recipeLongTextControl2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.recipeLongTextControl2, "recipeLongTextControl2");
            this.recipeLongTextControl2.LabelFont = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.recipeLongTextControl2.Name = "recipeLongTextControl2";
            this.recipeLongTextControl2.Tag = "MachineID";
            this.recipeLongTextControl2.Value = "";
            // 
            // recipeBooleanControl5
            // 
            this.recipeBooleanControl5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.recipeBooleanControl5, "recipeBooleanControl5");
            this.recipeBooleanControl5.Name = "recipeBooleanControl5";
            this.recipeBooleanControl5.Tag = "UseManualSelectRecipe";
            this.recipeBooleanControl5.Value = false;
            // 
            // tpTest
            // 
            this.tpTest.Controls.Add(this.tabManualFlowControl);
            resources.ApplyResources(this.tpTest, "tpTest");
            this.tpTest.Name = "tpTest";
            this.tpTest.UseVisualStyleBackColor = true;
            // 
            // tabManualFlowControl
            // 
            this.tabManualFlowControl.Controls.Add(this.tpQuickStart);
            this.tabManualFlowControl.Controls.Add(this.tpTools);
            this.tabManualFlowControl.Controls.Add(this.tpCommunication);
            this.tabManualFlowControl.Controls.Add(this.tpMutiLanguage);
            resources.ApplyResources(this.tabManualFlowControl, "tabManualFlowControl");
            this.tabManualFlowControl.Name = "tabManualFlowControl";
            this.tabManualFlowControl.SelectedIndex = 0;
            // 
            // tpQuickStart
            // 
            this.tpQuickStart.Controls.Add(this.pnlQuickStart);
            resources.ApplyResources(this.tpQuickStart, "tpQuickStart");
            this.tpQuickStart.Name = "tpQuickStart";
            this.tpQuickStart.UseVisualStyleBackColor = true;
            // 
            // pnlQuickStart
            // 
            resources.ApplyResources(this.pnlQuickStart, "pnlQuickStart");
            this.pnlQuickStart.AutoScrollHorizontalMaximum = 100;
            this.pnlQuickStart.AutoScrollHorizontalMinimum = 0;
            this.pnlQuickStart.AutoScrollHPos = 0;
            this.pnlQuickStart.AutoScrollVerticalMaximum = 100;
            this.pnlQuickStart.AutoScrollVerticalMinimum = 0;
            this.pnlQuickStart.AutoScrollVPos = 0;
            this.pnlQuickStart.Controls.Add(this.pnlQuickStartInner);
            this.pnlQuickStart.EnableAutoScrollHorizontal = true;
            this.pnlQuickStart.EnableAutoScrollVertical = true;
            this.pnlQuickStart.Name = "pnlQuickStart";
            this.pnlQuickStart.VisibleAutoScrollHorizontal = true;
            this.pnlQuickStart.VisibleAutoScrollVertical = true;
            // 
            // pnlQuickStartInner
            // 
            this.pnlQuickStartInner.Controls.Add(this.btnByPassConfiguration);
            this.pnlQuickStartInner.Controls.Add(this.btnSystemSetting);
            resources.ApplyResources(this.pnlQuickStartInner, "pnlQuickStartInner");
            this.pnlQuickStartInner.Name = "pnlQuickStartInner";
            // 
            // btnByPassConfiguration
            // 
            resources.ApplyResources(this.btnByPassConfiguration, "btnByPassConfiguration");
            this.btnByPassConfiguration.ForeColor = System.Drawing.Color.Black;
            this.btnByPassConfiguration.Name = "btnByPassConfiguration";
            this.btnByPassConfiguration.UseVisualStyleBackColor = true;
            this.btnByPassConfiguration.Click += new System.EventHandler(this.btnByPassConfiguration_Click);
            // 
            // btnSystemSetting
            // 
            resources.ApplyResources(this.btnSystemSetting, "btnSystemSetting");
            this.btnSystemSetting.ForeColor = System.Drawing.Color.Black;
            this.btnSystemSetting.Name = "btnSystemSetting";
            this.btnSystemSetting.UseVisualStyleBackColor = true;
            this.btnSystemSetting.Click += new System.EventHandler(this.btnSystemConfiguration_Click);
            // 
            // tpTools
            // 
            this.tpTools.Controls.Add(this.grpDeveloperTools);
            this.tpTools.Controls.Add(this.lblDeveloperTools);
            resources.ApplyResources(this.tpTools, "tpTools");
            this.tpTools.Name = "tpTools";
            this.tpTools.UseVisualStyleBackColor = true;
            // 
            // grpDeveloperTools
            // 
            resources.ApplyResources(this.grpDeveloperTools, "grpDeveloperTools");
            this.grpDeveloperTools.Name = "grpDeveloperTools";
            this.grpDeveloperTools.TabStop = false;
            // 
            // lblDeveloperTools
            // 
            resources.ApplyResources(this.lblDeveloperTools, "lblDeveloperTools");
            this.lblDeveloperTools.ForeColor = System.Drawing.Color.Red;
            this.lblDeveloperTools.Name = "lblDeveloperTools";
            // 
            // tpCommunication
            // 
            this.tpCommunication.Controls.Add(this.tlpCommunication);
            resources.ApplyResources(this.tpCommunication, "tpCommunication");
            this.tpCommunication.Name = "tpCommunication";
            this.tpCommunication.UseVisualStyleBackColor = true;
            // 
            // tlpCommunication
            // 
            resources.ApplyResources(this.tlpCommunication, "tlpCommunication");
            this.tlpCommunication.Controls.Add(this.panel, 0, 0);
            this.tlpCommunication.Name = "tlpCommunication";
            // 
            // panel
            // 
            resources.ApplyResources(this.panel, "panel");
            this.panel.Name = "panel";
            // 
            // tpMutiLanguage
            // 
            this.tpMutiLanguage.Controls.Add(this.btnMutiLanguage);
            this.tpMutiLanguage.Controls.Add(this.safeDoorCtl1);
            this.tpMutiLanguage.Controls.Add(this.dataRecordControl1);
            this.tpMutiLanguage.Controls.Add(this.nudMutiLanguage);
            this.tpMutiLanguage.Controls.Add(this.ledMutiLanguage);
            this.tpMutiLanguage.Controls.Add(this.tabMutiLanguage);
            this.tpMutiLanguage.Controls.Add(this.treeMutiLanguage);
            this.tpMutiLanguage.Controls.Add(this.rdoMutiLanguage);
            this.tpMutiLanguage.Controls.Add(this.listMutiLanguage);
            this.tpMutiLanguage.Controls.Add(this.cboMutiLanguage);
            this.tpMutiLanguage.Controls.Add(this.chklistMutiLanguage);
            this.tpMutiLanguage.Controls.Add(this.chkMutiLanguage);
            this.tpMutiLanguage.Controls.Add(this.lblMutiLanguage);
            this.tpMutiLanguage.Controls.Add(this.txtMutiLanguage);
            resources.ApplyResources(this.tpMutiLanguage, "tpMutiLanguage");
            this.tpMutiLanguage.Name = "tpMutiLanguage";
            this.tpMutiLanguage.UseVisualStyleBackColor = true;
            // 
            // btnMutiLanguage
            // 
            resources.ApplyResources(this.btnMutiLanguage, "btnMutiLanguage");
            this.btnMutiLanguage.Name = "btnMutiLanguage";
            this.btnMutiLanguage.UseVisualStyleBackColor = true;
            this.btnMutiLanguage.Click += new System.EventHandler(this.btnMutiLanguage_Click);
            // 
            // safeDoorCtl1
            // 
            this.safeDoorCtl1.Enable = false;
            this.safeDoorCtl1.ErrorCodeNum = 1;
            resources.ApplyResources(this.safeDoorCtl1, "safeDoorCtl1");
            this.safeDoorCtl1.Name = "safeDoorCtl1";
            this.safeDoorCtl1.SafeDoorNumber = 0;
            this.safeDoorCtl1.Side = AOISystem.Utilities.Recipe.SafeDoorSide.Right;
            this.safeDoorCtl1.Station = AOISystem.Utilities.Recipe.SafeDoorStation.Loader;
            // 
            // dataRecordControl1
            // 
            this.dataRecordControl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dataRecordControl1.DataType = AOISystem.Utilities.Recipe.DataTypeEnum.SystemData;
            this.dataRecordControl1.DataValue = "";
            this.dataRecordControl1.FieldType = AOISystem.Utilities.Recipe.FieldTypeEnum.DoubleType;
            resources.ApplyResources(this.dataRecordControl1, "dataRecordControl1");
            this.dataRecordControl1.LabelFont = null;
            this.dataRecordControl1.Name = "dataRecordControl1";
            this.dataRecordControl1.ToolTipDescription = "";
            this.dataRecordControl1.Value = -123456D;
            // 
            // nudMutiLanguage
            // 
            this.nudMutiLanguage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.nudMutiLanguage.DecimalPlaces = 0;
            this.nudMutiLanguage.Increment = new decimal(new int[] {
            1,
            0,
            0,
            0});
            resources.ApplyResources(this.nudMutiLanguage, "nudMutiLanguage");
            this.nudMutiLanguage.Maximum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.nudMutiLanguage.Minimum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.nudMutiLanguage.Name = "nudMutiLanguage";
            this.nudMutiLanguage.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudMutiLanguage.WaitMouseDownUp = false;
            // 
            // ledMutiLanguage
            // 
            resources.ApplyResources(this.ledMutiLanguage, "ledMutiLanguage");
            this.ledMutiLanguage.Name = "ledMutiLanguage";
            this.ledMutiLanguage.On = true;
            this.ledMutiLanguage.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ledMutiLanguage.TextFont = new System.Drawing.Font("Lucida Sans Unicode", 14F, System.Drawing.FontStyle.Bold);
            this.ledMutiLanguage.TextVisible = true;
            // 
            // tabMutiLanguage
            // 
            this.tabMutiLanguage.Controls.Add(this.tpEnglishItem1);
            this.tabMutiLanguage.Controls.Add(this.tpEnglishItem2);
            resources.ApplyResources(this.tabMutiLanguage, "tabMutiLanguage");
            this.tabMutiLanguage.Name = "tabMutiLanguage";
            this.tabMutiLanguage.SelectedIndex = 0;
            // 
            // tpEnglishItem1
            // 
            resources.ApplyResources(this.tpEnglishItem1, "tpEnglishItem1");
            this.tpEnglishItem1.Name = "tpEnglishItem1";
            this.tpEnglishItem1.UseVisualStyleBackColor = true;
            // 
            // tpEnglishItem2
            // 
            resources.ApplyResources(this.tpEnglishItem2, "tpEnglishItem2");
            this.tpEnglishItem2.Name = "tpEnglishItem2";
            this.tpEnglishItem2.UseVisualStyleBackColor = true;
            // 
            // treeMutiLanguage
            // 
            resources.ApplyResources(this.treeMutiLanguage, "treeMutiLanguage");
            this.treeMutiLanguage.Name = "treeMutiLanguage";
            this.treeMutiLanguage.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            ((System.Windows.Forms.TreeNode)(resources.GetObject("treeMutiLanguage.Nodes"))),
            ((System.Windows.Forms.TreeNode)(resources.GetObject("treeMutiLanguage.Nodes1")))});
            // 
            // rdoMutiLanguage
            // 
            resources.ApplyResources(this.rdoMutiLanguage, "rdoMutiLanguage");
            this.rdoMutiLanguage.Name = "rdoMutiLanguage";
            this.rdoMutiLanguage.TabStop = true;
            this.rdoMutiLanguage.UseVisualStyleBackColor = true;
            // 
            // listMutiLanguage
            // 
            this.listMutiLanguage.FormattingEnabled = true;
            resources.ApplyResources(this.listMutiLanguage, "listMutiLanguage");
            this.listMutiLanguage.Items.AddRange(new object[] {
            resources.GetString("listMutiLanguage.Items"),
            resources.GetString("listMutiLanguage.Items1")});
            this.listMutiLanguage.Name = "listMutiLanguage";
            // 
            // cboMutiLanguage
            // 
            this.cboMutiLanguage.FormattingEnabled = true;
            this.cboMutiLanguage.Items.AddRange(new object[] {
            resources.GetString("cboMutiLanguage.Items"),
            resources.GetString("cboMutiLanguage.Items1")});
            resources.ApplyResources(this.cboMutiLanguage, "cboMutiLanguage");
            this.cboMutiLanguage.Name = "cboMutiLanguage";
            // 
            // chklistMutiLanguage
            // 
            this.chklistMutiLanguage.FormattingEnabled = true;
            this.chklistMutiLanguage.Items.AddRange(new object[] {
            resources.GetString("chklistMutiLanguage.Items"),
            resources.GetString("chklistMutiLanguage.Items1")});
            resources.ApplyResources(this.chklistMutiLanguage, "chklistMutiLanguage");
            this.chklistMutiLanguage.Name = "chklistMutiLanguage";
            // 
            // chkMutiLanguage
            // 
            resources.ApplyResources(this.chkMutiLanguage, "chkMutiLanguage");
            this.chkMutiLanguage.Name = "chkMutiLanguage";
            this.chkMutiLanguage.UseVisualStyleBackColor = true;
            // 
            // lblMutiLanguage
            // 
            resources.ApplyResources(this.lblMutiLanguage, "lblMutiLanguage");
            this.lblMutiLanguage.Name = "lblMutiLanguage";
            // 
            // txtMutiLanguage
            // 
            resources.ApplyResources(this.txtMutiLanguage, "txtMutiLanguage");
            this.txtMutiLanguage.Name = "txtMutiLanguage";
            // 
            // tabMessage
            // 
            resources.ApplyResources(this.tabMessage, "tabMessage");
            this.tabMessage.Controls.Add(this.tpInfoformation);
            this.tabMessage.Controls.Add(this.tpLogging);
            this.tabMessage.Multiline = true;
            this.tabMessage.Name = "tabMessage";
            this.tabMessage.SelectedIndex = 0;
            // 
            // tpInfoformation
            // 
            this.tpInfoformation.Controls.Add(this.lsvErrorMessage);
            resources.ApplyResources(this.tpInfoformation, "tpInfoformation");
            this.tpInfoformation.Name = "tpInfoformation";
            this.tpInfoformation.UseVisualStyleBackColor = true;
            // 
            // lsvErrorMessage
            // 
            this.lsvErrorMessage.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6});
            resources.ApplyResources(this.lsvErrorMessage, "lsvErrorMessage");
            this.lsvErrorMessage.Name = "lsvErrorMessage";
            this.lsvErrorMessage.UseCompatibleStateImageBehavior = false;
            this.lsvErrorMessage.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            resources.ApplyResources(this.columnHeader1, "columnHeader1");
            // 
            // columnHeader2
            // 
            resources.ApplyResources(this.columnHeader2, "columnHeader2");
            // 
            // columnHeader3
            // 
            resources.ApplyResources(this.columnHeader3, "columnHeader3");
            // 
            // columnHeader4
            // 
            resources.ApplyResources(this.columnHeader4, "columnHeader4");
            // 
            // columnHeader5
            // 
            resources.ApplyResources(this.columnHeader5, "columnHeader5");
            // 
            // columnHeader6
            // 
            resources.ApplyResources(this.columnHeader6, "columnHeader6");
            // 
            // tpLogging
            // 
            this.tpLogging.Controls.Add(this.hLogger);
            resources.ApplyResources(this.tpLogging, "tpLogging");
            this.tpLogging.Name = "tpLogging";
            this.tpLogging.UseVisualStyleBackColor = true;
            // 
            // hLogger
            // 
            resources.ApplyResources(this.hLogger, "hLogger");
            this.hLogger.Name = "hLogger";
            // 
            // msMenu
            // 
            this.msMenu.GripStyle = System.Windows.Forms.ToolStripGripStyle.Visible;
            this.msMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiFile,
            this.tsmiUserLogInOut,
            this.tsmiRecipe,
            this.tsmiHelp});
            resources.ApplyResources(this.msMenu, "msMenu");
            this.msMenu.Name = "msMenu";
            // 
            // tsmiFile
            // 
            this.tsmiFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiSystemConfiguration,
            this.tsmiRecipeConfiguration});
            this.tsmiFile.Name = "tsmiFile";
            resources.ApplyResources(this.tsmiFile, "tsmiFile");
            this.tsmiFile.Tag = "";
            // 
            // tsmiSystemConfiguration
            // 
            this.tsmiSystemConfiguration.Name = "tsmiSystemConfiguration";
            resources.ApplyResources(this.tsmiSystemConfiguration, "tsmiSystemConfiguration");
            this.tsmiSystemConfiguration.Click += new System.EventHandler(this.tsmiSystemConfiguration_Click);
            // 
            // tsmiRecipeConfiguration
            // 
            this.tsmiRecipeConfiguration.Name = "tsmiRecipeConfiguration";
            resources.ApplyResources(this.tsmiRecipeConfiguration, "tsmiRecipeConfiguration");
            this.tsmiRecipeConfiguration.Click += new System.EventHandler(this.tsmiRecipeConfiguration_Click);
            // 
            // tsmiUserLogInOut
            // 
            this.tsmiUserLogInOut.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tspmiLogIn,
            this.tspmiLogOut,
            this.tspmiAccountEditor});
            this.tsmiUserLogInOut.Name = "tsmiUserLogInOut";
            resources.ApplyResources(this.tsmiUserLogInOut, "tsmiUserLogInOut");
            // 
            // tspmiLogIn
            // 
            this.tspmiLogIn.Name = "tspmiLogIn";
            resources.ApplyResources(this.tspmiLogIn, "tspmiLogIn");
            this.tspmiLogIn.Click += new System.EventHandler(this.tspmiLogIn_Click);
            // 
            // tspmiLogOut
            // 
            this.tspmiLogOut.Name = "tspmiLogOut";
            resources.ApplyResources(this.tspmiLogOut, "tspmiLogOut");
            this.tspmiLogOut.Click += new System.EventHandler(this.tspmiLogOut_Click);
            // 
            // tspmiAccountEditor
            // 
            this.tspmiAccountEditor.Name = "tspmiAccountEditor";
            resources.ApplyResources(this.tspmiAccountEditor, "tspmiAccountEditor");
            this.tspmiAccountEditor.Click += new System.EventHandler(this.tspmiAccountEditor_Click);
            // 
            // tsmiRecipe
            // 
            this.tsmiRecipe.Name = "tsmiRecipe";
            resources.ApplyResources(this.tsmiRecipe, "tsmiRecipe");
            this.tsmiRecipe.Click += new System.EventHandler(this.tsmiRecipe_Click);
            // 
            // tsmiHelp
            // 
            this.tsmiHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiLanguage,
            this.tsmiAbout});
            this.tsmiHelp.Name = "tsmiHelp";
            resources.ApplyResources(this.tsmiHelp, "tsmiHelp");
            // 
            // tsmiLanguage
            // 
            this.tsmiLanguage.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiTraditionalChinese,
            this.tsmiEnglish});
            this.tsmiLanguage.Name = "tsmiLanguage";
            resources.ApplyResources(this.tsmiLanguage, "tsmiLanguage");
            // 
            // tsmiTraditionalChinese
            // 
            this.tsmiTraditionalChinese.Name = "tsmiTraditionalChinese";
            resources.ApplyResources(this.tsmiTraditionalChinese, "tsmiTraditionalChinese");
            this.tsmiTraditionalChinese.Click += new System.EventHandler(this.tsmiTraditionalChinese_Click);
            // 
            // tsmiEnglish
            // 
            this.tsmiEnglish.Name = "tsmiEnglish";
            resources.ApplyResources(this.tsmiEnglish, "tsmiEnglish");
            this.tsmiEnglish.Click += new System.EventHandler(this.tsmiEnglish_Click);
            // 
            // tsmiAbout
            // 
            this.tsmiAbout.Name = "tsmiAbout";
            resources.ApplyResources(this.tsmiAbout, "tsmiAbout");
            this.tsmiAbout.Click += new System.EventHandler(this.tsmiAbout_Click);
            // 
            // tmrSystem
            // 
            this.tmrSystem.Tick += new System.EventHandler(this.tmrSystem_Tick);
            // 
            // tlpToolBar
            // 
            this.tlpToolBar.BackColor = System.Drawing.Color.SkyBlue;
            resources.ApplyResources(this.tlpToolBar, "tlpToolBar");
            this.tlpToolBar.Controls.Add(this.tlpSystemStatus, 2, 0);
            this.tlpToolBar.Controls.Add(this.flpToolBarSub2, 3, 0);
            this.tlpToolBar.Controls.Add(this.flpToolBarSub1, 0, 0);
            this.tlpToolBar.Controls.Add(this.tlpConnecteStatus, 1, 0);
            this.tlpToolBar.Name = "tlpToolBar";
            // 
            // tlpSystemStatus
            // 
            resources.ApplyResources(this.tlpSystemStatus, "tlpSystemStatus");
            this.tlpSystemStatus.Controls.Add(this.ledDown, 0, 2);
            this.tlpSystemStatus.Controls.Add(this.ledWarning, 0, 1);
            this.tlpSystemStatus.Controls.Add(this.ledRun, 0, 0);
            this.tlpSystemStatus.Name = "tlpSystemStatus";
            // 
            // ledDown
            // 
            this.ledDown.Color = System.Drawing.Color.Red;
            resources.ApplyResources(this.ledDown, "ledDown");
            this.ledDown.Name = "ledDown";
            this.ledDown.On = false;
            this.ledDown.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ledDown.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ledDown.TextVisible = true;
            // 
            // ledWarning
            // 
            this.ledWarning.Color = System.Drawing.Color.Yellow;
            resources.ApplyResources(this.ledWarning, "ledWarning");
            this.ledWarning.Name = "ledWarning";
            this.ledWarning.On = false;
            this.ledWarning.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ledWarning.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ledWarning.TextVisible = true;
            // 
            // ledRun
            // 
            resources.ApplyResources(this.ledRun, "ledRun");
            this.ledRun.Name = "ledRun";
            this.ledRun.On = false;
            this.ledRun.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ledRun.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ledRun.TextVisible = true;
            // 
            // flpToolBarSub2
            // 
            this.flpToolBarSub2.Controls.Add(this.btnStartFlow);
            this.flpToolBarSub2.Controls.Add(this.btnPauseFlow);
            this.flpToolBarSub2.Controls.Add(this.btnAlarmReset);
            this.flpToolBarSub2.Controls.Add(this.btnMute);
            this.flpToolBarSub2.Controls.Add(this.btnStopFlow);
            this.flpToolBarSub2.Controls.Add(this.btnLoadRecipeFlow);
            this.flpToolBarSub2.Controls.Add(this.btnHome);
            resources.ApplyResources(this.flpToolBarSub2, "flpToolBarSub2");
            this.flpToolBarSub2.Name = "flpToolBarSub2";
            // 
            // btnStartFlow
            // 
            this.btnStartFlow.AccountLevel = AOISystem.Utilities.Account.AccountLevel.Engineer;
            this.btnStartFlow.BackColor = System.Drawing.Color.Lime;
            this.btnStartFlow.DelayTime = 0D;
            resources.ApplyResources(this.btnStartFlow, "btnStartFlow");
            this.btnStartFlow.ForeColor = System.Drawing.Color.Black;
            this.btnStartFlow.Name = "btnStartFlow";
            this.btnStartFlow.TestPermission = false;
            this.btnStartFlow.UseVisualStyleBackColor = false;
            this.btnStartFlow.Click += new System.EventHandler(this.btnStartFlow_Click);
            // 
            // btnPauseFlow
            // 
            this.btnPauseFlow.AccountLevel = AOISystem.Utilities.Account.AccountLevel.Engineer;
            this.btnPauseFlow.BackColor = System.Drawing.Color.Orange;
            this.btnPauseFlow.DelayTime = 0D;
            resources.ApplyResources(this.btnPauseFlow, "btnPauseFlow");
            this.btnPauseFlow.ForeColor = System.Drawing.Color.Black;
            this.btnPauseFlow.Name = "btnPauseFlow";
            this.btnPauseFlow.TestPermission = false;
            this.btnPauseFlow.UseVisualStyleBackColor = false;
            this.btnPauseFlow.Click += new System.EventHandler(this.btnPauseFlow_Click);
            // 
            // btnAlarmReset
            // 
            this.btnAlarmReset.AccountLevel = AOISystem.Utilities.Account.AccountLevel.Engineer;
            this.btnAlarmReset.BackColor = System.Drawing.Color.Red;
            this.btnAlarmReset.DelayTime = 0D;
            resources.ApplyResources(this.btnAlarmReset, "btnAlarmReset");
            this.btnAlarmReset.ForeColor = System.Drawing.Color.Black;
            this.btnAlarmReset.Name = "btnAlarmReset";
            this.btnAlarmReset.TestPermission = false;
            this.btnAlarmReset.UseVisualStyleBackColor = false;
            this.btnAlarmReset.Click += new System.EventHandler(this.btnAlarmReset_Click);
            // 
            // btnMute
            // 
            this.btnMute.AccountLevel = AOISystem.Utilities.Account.AccountLevel.Engineer;
            this.btnMute.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnMute.DelayTime = 0D;
            resources.ApplyResources(this.btnMute, "btnMute");
            this.btnMute.ForeColor = System.Drawing.Color.Black;
            this.btnMute.Name = "btnMute";
            this.btnMute.TestPermission = false;
            this.btnMute.UseVisualStyleBackColor = false;
            this.btnMute.Click += new System.EventHandler(this.btnMute_Click);
            // 
            // btnStopFlow
            // 
            this.btnStopFlow.AccountLevel = AOISystem.Utilities.Account.AccountLevel.Engineer;
            this.btnStopFlow.BackColor = System.Drawing.SystemColors.Control;
            this.btnStopFlow.DelayTime = 0D;
            resources.ApplyResources(this.btnStopFlow, "btnStopFlow");
            this.btnStopFlow.ForeColor = System.Drawing.Color.Black;
            this.btnStopFlow.Name = "btnStopFlow";
            this.btnStopFlow.TestPermission = false;
            this.btnStopFlow.UseVisualStyleBackColor = false;
            this.btnStopFlow.Click += new System.EventHandler(this.btnStopFlow_Click);
            // 
            // btnLoadRecipeFlow
            // 
            this.btnLoadRecipeFlow.AccountLevel = AOISystem.Utilities.Account.AccountLevel.Engineer;
            this.btnLoadRecipeFlow.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnLoadRecipeFlow.DelayTime = 0D;
            resources.ApplyResources(this.btnLoadRecipeFlow, "btnLoadRecipeFlow");
            this.btnLoadRecipeFlow.ForeColor = System.Drawing.Color.Black;
            this.btnLoadRecipeFlow.Name = "btnLoadRecipeFlow";
            this.btnLoadRecipeFlow.TestPermission = false;
            this.btnLoadRecipeFlow.UseVisualStyleBackColor = false;
            this.btnLoadRecipeFlow.Click += new System.EventHandler(this.btnLoadRecipeFlow_Click);
            // 
            // btnHome
            // 
            this.btnHome.AccountLevel = AOISystem.Utilities.Account.AccountLevel.Engineer;
            this.btnHome.BackColor = System.Drawing.Color.Yellow;
            this.btnHome.DelayTime = 0D;
            resources.ApplyResources(this.btnHome, "btnHome");
            this.btnHome.ForeColor = System.Drawing.Color.Black;
            this.btnHome.Name = "btnHome";
            this.btnHome.TestPermission = false;
            this.btnHome.UseVisualStyleBackColor = false;
            this.btnHome.Click += new System.EventHandler(this.btnHome_Click);
            // 
            // flpToolBarSub1
            // 
            this.flpToolBarSub1.BackColor = System.Drawing.Color.SkyBlue;
            this.flpToolBarSub1.Controls.Add(this.btnMain);
            this.flpToolBarSub1.Controls.Add(this.btnIO);
            this.flpToolBarSub1.Controls.Add(this.btnMotion);
            this.flpToolBarSub1.Controls.Add(this.btnDatabase);
            this.flpToolBarSub1.Controls.Add(this.btnRecipe);
            this.flpToolBarSub1.Controls.Add(this.btnSystem);
            this.flpToolBarSub1.Controls.Add(this.btnTest);
            resources.ApplyResources(this.flpToolBarSub1, "flpToolBarSub1");
            this.flpToolBarSub1.Name = "flpToolBarSub1";
            // 
            // btnMain
            // 
            resources.ApplyResources(this.btnMain, "btnMain");
            this.btnMain.BackColor = System.Drawing.Color.Yellow;
            this.btnMain.ForeColor = System.Drawing.Color.DarkGreen;
            this.btnMain.Name = "btnMain";
            this.btnMain.UseVisualStyleBackColor = false;
            this.btnMain.Click += new System.EventHandler(this.btnMain_Click);
            // 
            // btnIO
            // 
            resources.ApplyResources(this.btnIO, "btnIO");
            this.btnIO.BackColor = System.Drawing.SystemColors.Control;
            this.btnIO.ForeColor = System.Drawing.Color.DarkGreen;
            this.btnIO.Name = "btnIO";
            this.btnIO.UseVisualStyleBackColor = false;
            this.btnIO.Click += new System.EventHandler(this.btnIO_Click);
            // 
            // btnMotion
            // 
            resources.ApplyResources(this.btnMotion, "btnMotion");
            this.btnMotion.BackColor = System.Drawing.SystemColors.Control;
            this.btnMotion.ForeColor = System.Drawing.Color.DarkGreen;
            this.btnMotion.Name = "btnMotion";
            this.btnMotion.UseVisualStyleBackColor = false;
            this.btnMotion.Click += new System.EventHandler(this.btnMotion_Click);
            // 
            // btnDatabase
            // 
            resources.ApplyResources(this.btnDatabase, "btnDatabase");
            this.btnDatabase.BackColor = System.Drawing.SystemColors.Control;
            this.btnDatabase.ForeColor = System.Drawing.Color.DarkGreen;
            this.btnDatabase.Name = "btnDatabase";
            this.btnDatabase.UseVisualStyleBackColor = false;
            this.btnDatabase.Click += new System.EventHandler(this.btnDatabase_Click);
            // 
            // btnRecipe
            // 
            resources.ApplyResources(this.btnRecipe, "btnRecipe");
            this.btnRecipe.BackColor = System.Drawing.SystemColors.Control;
            this.btnRecipe.ForeColor = System.Drawing.Color.DarkGreen;
            this.btnRecipe.Name = "btnRecipe";
            this.btnRecipe.UseVisualStyleBackColor = false;
            this.btnRecipe.Click += new System.EventHandler(this.btnRecipe_Click);
            // 
            // btnSystem
            // 
            resources.ApplyResources(this.btnSystem, "btnSystem");
            this.btnSystem.BackColor = System.Drawing.SystemColors.Control;
            this.btnSystem.ForeColor = System.Drawing.Color.DarkGreen;
            this.btnSystem.Name = "btnSystem";
            this.btnSystem.UseVisualStyleBackColor = false;
            this.btnSystem.Click += new System.EventHandler(this.btnSystem_Click);
            // 
            // btnTest
            // 
            resources.ApplyResources(this.btnTest, "btnTest");
            this.btnTest.BackColor = System.Drawing.SystemColors.Control;
            this.btnTest.ForeColor = System.Drawing.Color.DarkGreen;
            this.btnTest.Name = "btnTest";
            this.btnTest.UseVisualStyleBackColor = false;
            this.btnTest.Click += new System.EventHandler(this.btnTest_Click);
            // 
            // tlpConnecteStatus
            // 
            resources.ApplyResources(this.tlpConnecteStatus, "tlpConnecteStatus");
            this.tlpConnecteStatus.Controls.Add(this.ledAOI1, 0, 0);
            this.tlpConnecteStatus.Name = "tlpConnecteStatus";
            // 
            // ledAOI1
            // 
            resources.ApplyResources(this.ledAOI1, "ledAOI1");
            this.ledAOI1.Name = "ledAOI1";
            this.ledAOI1.On = false;
            this.ledAOI1.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ledAOI1.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ledAOI1.TextVisible = true;
            // 
            // ssStatusBar
            // 
            resources.ApplyResources(this.ssStatusBar, "ssStatusBar");
            this.ssStatusBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblRecipeNo,
            this.lblRecipeID,
            this.lblCPU,
            this.prgCPU,
            this.lblRAM,
            this.prgRAM,
            this.lblDISK,
            this.prgDISK,
            this.lblSpare,
            this.lblAccountName,
            this.lblAccountLevel,
            this.lblVersion,
            this.lblNowTime});
            this.ssStatusBar.Name = "ssStatusBar";
            // 
            // lblRecipeNo
            // 
            this.lblRecipeNo.Margin = new System.Windows.Forms.Padding(10, 3, 10, 3);
            this.lblRecipeNo.Name = "lblRecipeNo";
            resources.ApplyResources(this.lblRecipeNo, "lblRecipeNo");
            // 
            // lblRecipeID
            // 
            this.lblRecipeID.Margin = new System.Windows.Forms.Padding(10, 3, 10, 3);
            this.lblRecipeID.Name = "lblRecipeID";
            resources.ApplyResources(this.lblRecipeID, "lblRecipeID");
            // 
            // lblCPU
            // 
            this.lblCPU.Name = "lblCPU";
            resources.ApplyResources(this.lblCPU, "lblCPU");
            // 
            // prgCPU
            // 
            this.prgCPU.Name = "prgCPU";
            resources.ApplyResources(this.prgCPU, "prgCPU");
            // 
            // lblRAM
            // 
            this.lblRAM.Name = "lblRAM";
            resources.ApplyResources(this.lblRAM, "lblRAM");
            // 
            // prgRAM
            // 
            this.prgRAM.Name = "prgRAM";
            resources.ApplyResources(this.prgRAM, "prgRAM");
            // 
            // lblDISK
            // 
            this.lblDISK.Name = "lblDISK";
            resources.ApplyResources(this.lblDISK, "lblDISK");
            // 
            // prgDISK
            // 
            this.prgDISK.Name = "prgDISK";
            resources.ApplyResources(this.prgDISK, "prgDISK");
            // 
            // lblSpare
            // 
            this.lblSpare.Margin = new System.Windows.Forms.Padding(10, 3, 10, 3);
            this.lblSpare.Name = "lblSpare";
            resources.ApplyResources(this.lblSpare, "lblSpare");
            this.lblSpare.Spring = true;
            // 
            // lblAccountName
            // 
            this.lblAccountName.Margin = new System.Windows.Forms.Padding(10, 3, 10, 3);
            this.lblAccountName.Name = "lblAccountName";
            resources.ApplyResources(this.lblAccountName, "lblAccountName");
            // 
            // lblAccountLevel
            // 
            this.lblAccountLevel.Margin = new System.Windows.Forms.Padding(10, 3, 10, 3);
            this.lblAccountLevel.Name = "lblAccountLevel";
            resources.ApplyResources(this.lblAccountLevel, "lblAccountLevel");
            // 
            // lblVersion
            // 
            this.lblVersion.Margin = new System.Windows.Forms.Padding(10, 3, 10, 3);
            this.lblVersion.Name = "lblVersion";
            resources.ApplyResources(this.lblVersion, "lblVersion");
            // 
            // lblNowTime
            // 
            this.lblNowTime.Margin = new System.Windows.Forms.Padding(10, 3, 10, 3);
            this.lblNowTime.Name = "lblNowTime";
            resources.ApplyResources(this.lblNowTime, "lblNowTime");
            // 
            // tlpMain
            // 
            resources.ApplyResources(this.tlpMain, "tlpMain");
            this.tlpMain.Controls.Add(this.ssStatusBar, 0, 1);
            this.tlpMain.Controls.Add(this.splViewer, 0, 0);
            this.tlpMain.Name = "tlpMain";
            // 
            // tmrLogOut
            // 
            this.tmrLogOut.Interval = 1000;
            this.tmrLogOut.Tick += new System.EventHandler(this.tmrLogOut_Tick);
            // 
            // modulesIMotionControl1
            // 
            resources.ApplyResources(this.modulesIMotionControl1, "modulesIMotionControl1");
            this.modulesIMotionControl1.Name = "modulesIMotionControl1";
            // 
            // MainForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tlpMain);
            this.Controls.Add(this.tlpToolBar);
            this.Controls.Add(this.msMenu);
            this.DoubleBuffered = true;
            this.Name = "MainForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.splViewer.Panel1.ResumeLayout(false);
            this.splViewer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splViewer)).EndInit();
            this.splViewer.ResumeLayout(false);
            this.tabControl.ResumeLayout(false);
            this.tpMain.ResumeLayout(false);
            this.grpInformation.ResumeLayout(false);
            this.grpInformation.PerformLayout();
            this.tpIO.ResumeLayout(false);
            this.grpIOOutput.ResumeLayout(false);
            this.grpIOInput.ResumeLayout(false);
            this.tpMotion.ResumeLayout(false);
            this.tabMotion.ResumeLayout(false);
            this.tpAOI.ResumeLayout(false);
            this.pnlAOISystemParam.ResumeLayout(false);
            this.tabControl4.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.flowLayoutPanel19.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tpDatabase.ResumeLayout(false);
            this.tpRecipe.ResumeLayout(false);
            this.tlpRecipe.ResumeLayout(false);
            this.tabRecipe.ResumeLayout(false);
            this.tpOther.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.flowLayoutPanel10.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tpSystem.ResumeLayout(false);
            this.flpSystem.ResumeLayout(false);
            this.tpTest.ResumeLayout(false);
            this.tabManualFlowControl.ResumeLayout(false);
            this.tpQuickStart.ResumeLayout(false);
            this.pnlQuickStart.ResumeLayout(false);
            this.pnlQuickStartInner.ResumeLayout(false);
            this.tpTools.ResumeLayout(false);
            this.tpTools.PerformLayout();
            this.tpCommunication.ResumeLayout(false);
            this.tlpCommunication.ResumeLayout(false);
            this.tpMutiLanguage.ResumeLayout(false);
            this.tpMutiLanguage.PerformLayout();
            this.tabMutiLanguage.ResumeLayout(false);
            this.tabMessage.ResumeLayout(false);
            this.tpInfoformation.ResumeLayout(false);
            this.tpLogging.ResumeLayout(false);
            this.msMenu.ResumeLayout(false);
            this.msMenu.PerformLayout();
            this.tlpToolBar.ResumeLayout(false);
            this.tlpSystemStatus.ResumeLayout(false);
            this.flpToolBarSub2.ResumeLayout(false);
            this.flpToolBarSub1.ResumeLayout(false);
            this.tlpConnecteStatus.ResumeLayout(false);
            this.ssStatusBar.ResumeLayout(false);
            this.ssStatusBar.PerformLayout();
            this.tlpMain.ResumeLayout(false);
            this.tlpMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip msMenu;
        private System.Windows.Forms.ToolStripMenuItem tsmiFile;
        private System.Windows.Forms.TabControl tabManualFlowControl;
        private System.Windows.Forms.TabPage tpTools;
        private System.Windows.Forms.Timer tmrSystem;
        private System.Windows.Forms.TabPage tpQuickStart;
        private AOISystem.Utilities.Forms.ScrollablePanel pnlQuickStart;
        private System.Windows.Forms.Panel pnlQuickStartInner;
        private System.Windows.Forms.ToolStripMenuItem tsmiUserLogInOut;
        private System.Windows.Forms.ToolStripMenuItem tspmiLogIn;
        private System.Windows.Forms.ToolStripMenuItem tspmiLogOut;
        private System.Windows.Forms.ToolStripMenuItem tspmiAccountEditor;
        private System.Windows.Forms.Label lblDeveloperTools;
        private System.Windows.Forms.ToolStripMenuItem tsmiSystemConfiguration;
        private AOISystem.Utilities.Forms.DelayTimeButton btnStopFlow;
        private AOISystem.Utilities.Forms.DelayTimeButton btnStartFlow;
        private System.Windows.Forms.Button btnSystemSetting;
        private System.Windows.Forms.ToolStripMenuItem tsmiRecipe;
        private System.Windows.Forms.ToolStripMenuItem tsmiHelp;
        private System.Windows.Forms.ToolStripMenuItem tsmiLanguage;
        private System.Windows.Forms.ToolStripMenuItem tsmiTraditionalChinese;
        private System.Windows.Forms.ToolStripMenuItem tsmiEnglish;
        private AOISystem.Utilities.Forms.SplitContainerEx splViewer;
        private System.Windows.Forms.TabPage tpCommunication;
        private System.Windows.Forms.TableLayoutPanel tlpCommunication;
        private System.Windows.Forms.Panel panel;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tpMain;
        private System.Windows.Forms.TabPage tpIO;
        private System.Windows.Forms.GroupBox grpIOInput;
        private System.Windows.Forms.FlowLayoutPanel flpIOInput;
        private System.Windows.Forms.GroupBox grpIOOutput;
        private System.Windows.Forms.FlowLayoutPanel flpIOOutput;
        private System.Windows.Forms.TabPage tpTest;
        private System.Windows.Forms.TableLayoutPanel tlpToolBar;
        private System.Windows.Forms.FlowLayoutPanel flpToolBarSub2;
        private System.Windows.Forms.FlowLayoutPanel flpToolBarSub1;
        private System.Windows.Forms.Button btnMain;
        private System.Windows.Forms.Button btnIO;
        private System.Windows.Forms.Button btnTest;
        private System.Windows.Forms.TabPage tpMotion;
        private System.Windows.Forms.TabPage tpDatabase;
        private System.Windows.Forms.Button btnMotion;
        private System.Windows.Forms.Button btnDatabase;
        private System.Windows.Forms.TabControl tabMessage;
        private System.Windows.Forms.TabPage tpLogging;
        private AOISystem.Utilities.Logging.HLogger hLogger;
        private System.Windows.Forms.TabPage tpInfoformation;
        private System.Windows.Forms.Button btnByPassConfiguration;
        private System.Windows.Forms.TableLayoutPanel tlpConnecteStatus;
        private AOISystem.Utilities.Forms.LedRectangle ledAOI1;
        private System.Windows.Forms.TabControl tabMotion;
        private System.Windows.Forms.TabPage tpAOI;
        private System.Windows.Forms.Button btnCV1;
        private AOISystem.Utilities.Forms.DelayTimeButton btnMute;
        private AOISystem.Utilities.Forms.DelayTimeButton btnHome;
        private System.Windows.Forms.TableLayoutPanel tlpSystemStatus;
        private AOISystem.Utilities.Forms.LedRectangle ledDown;
        private AOISystem.Utilities.Forms.LedRectangle ledWarning;
        private AOISystem.Utilities.Forms.LedRectangle ledRun;
        private AOISystem.Utilities.Forms.DelayTimeButton btnPauseFlow;
        private AOISystem.Utilities.Forms.DelayTimeButton btnAlarmReset;
        private System.Windows.Forms.ToolStripMenuItem tsmiRecipeConfiguration;
        private System.Windows.Forms.ListView lsvErrorMessage;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private Forms.ModulesIMotionControl modulesIMotionControl1;
        private System.Windows.Forms.Button btnRecipe;
        private System.Windows.Forms.TabPage tpRecipe;
        private System.Windows.Forms.TableLayoutPanel tlpRecipe;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button btnRecipeRestore;
        private System.Windows.Forms.Button btnRecipeSave;
        private AOISystem.Utilities.Forms.DelayTimeButton btnLoadRecipeFlow;
        private System.Windows.Forms.TabControl tabRecipe;
        private System.Windows.Forms.TabPage tpOther;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel10;
        private AOISystem.Utilities.Recipe.RecipeTextControl rtcExtraMaxDistance;
        private AOISystem.Utilities.Recipe.RecipeTextControl rtcExtraSurvivalTime;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel11;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel12;
        private AOISystem.Utilities.Recipe.RecipeEditorControl recipeEditorControl;
        private System.Windows.Forms.GroupBox grpInformation;
        private System.Windows.Forms.TextBox txtUserID;
        private System.Windows.Forms.Label lblUserID;
        private System.Windows.Forms.TextBox txtRecipeID;
        private System.Windows.Forms.TextBox txtRecipeNo;
        private System.Windows.Forms.Label lblRecipeNoLabel;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.TextBox txtVersion;
        private System.Windows.Forms.Label lblVersionLabel;
        private System.Windows.Forms.TextBox txtNowTime;
        private System.Windows.Forms.Label lblNowTimeLabel;
        private System.Windows.Forms.GroupBox grpMeasureResult;
        private System.Windows.Forms.Button btnSystem;
        private System.Windows.Forms.TabPage tpSystem;
        private System.Windows.Forms.Button btnSaveSystemData;
        private System.Windows.Forms.FlowLayoutPanel flpSystem;
        private AOISystem.Utilities.Recipe.RecipeLongTextControl recipeLongTextControl1;
        private AOISystem.Utilities.Recipe.RecipeLongTextControl recipeLongTextControl2;
        private System.Windows.Forms.StatusStrip ssStatusBar;
        private System.Windows.Forms.ToolStripStatusLabel lblRecipeNo;
        private System.Windows.Forms.ToolStripStatusLabel lblRecipeID;
        private System.Windows.Forms.ToolStripStatusLabel lblDISK;
        private System.Windows.Forms.ToolStripProgressBar prgDISK;
        private System.Windows.Forms.ToolStripStatusLabel lblSpare;
        private System.Windows.Forms.ToolStripStatusLabel lblAccountName;
        private System.Windows.Forms.ToolStripStatusLabel lblAccountLevel;
        private System.Windows.Forms.ToolStripStatusLabel lblVersion;
        private System.Windows.Forms.ToolStripStatusLabel lblNowTime;
        private System.Windows.Forms.ToolStripProgressBar prgCPU;
        private AOISystem.Utilities.Recipe.RecipeBooleanControl recipeBooleanControl5;
        private System.Windows.Forms.Panel pnlAOISystemParam;
        private System.Windows.Forms.TabControl tabControl4;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel19;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel20;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel25;
        private System.Windows.Forms.Button btnSystemMotionPositionRestore;
        private System.Windows.Forms.Button btnSystemMotionPositionSave;
        private AOISystem.Utilities.Recipe.RecipeTextControl rtcClearOffFlowContiunanceTime;
        private System.Windows.Forms.ToolStripStatusLabel lblCPU;
        private System.Windows.Forms.ToolStripStatusLabel lblRAM;
        private System.Windows.Forms.ToolStripProgressBar prgRAM;
        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.ToolStripMenuItem tsmiAbout;
        private System.Windows.Forms.Timer tmrLogOut;
        private System.Windows.Forms.GroupBox grpDeveloperTools;
        private AOISystem.Utilities.Recipe.RecipeNumericUpDownControl rncAvailableRAMMinFreeSpace;
        private System.Windows.Forms.TabPage tpMutiLanguage;
        private AOISystem.Utilities.Forms.LedRectangle ledMutiLanguage;
        private System.Windows.Forms.TabControl tabMutiLanguage;
        private System.Windows.Forms.TabPage tpEnglishItem1;
        private System.Windows.Forms.TabPage tpEnglishItem2;
        private System.Windows.Forms.TreeView treeMutiLanguage;
        private System.Windows.Forms.RadioButton rdoMutiLanguage;
        private System.Windows.Forms.ListBox listMutiLanguage;
        private System.Windows.Forms.ComboBox cboMutiLanguage;
        private System.Windows.Forms.CheckedListBox chklistMutiLanguage;
        private System.Windows.Forms.CheckBox chkMutiLanguage;
        private System.Windows.Forms.Label lblMutiLanguage;
        private System.Windows.Forms.TextBox txtMutiLanguage;
        private System.Windows.Forms.ToolTip toolTip;
        private AOISystem.Utilities.Forms.NumericUpDownSlider nudMutiLanguage;
        private AOISystem.Utilities.Recipe.DataRecordControl dataRecordControl1;
        private AOISystem.Utilities.Recipe.SafeDoorCtl safeDoorCtl1;
        private System.Windows.Forms.Button btnMutiLanguage;
        private System.Windows.Forms.ColumnHeader columnHeader6;
    }
}


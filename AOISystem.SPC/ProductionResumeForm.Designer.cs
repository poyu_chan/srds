﻿namespace AOISystem.SPC
{
    partial class ProductionResumeForm
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器
        /// 修改這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.dgvHourResume = new System.Windows.Forms.DataGridView();
            this.HourTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HourPass = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HourFail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HourTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HourYield = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblHourResume = new System.Windows.Forms.Label();
            this.lblDayResume = new System.Windows.Forms.Label();
            this.dgvDayResume = new System.Windows.Forms.DataGridView();
            this.DayTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DayPass = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DayFail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DayTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DayYield = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tableLayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHourResume)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDayResume)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.ColumnCount = 2;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.Controls.Add(this.dgvHourResume, 1, 1);
            this.tableLayoutPanel.Controls.Add(this.lblHourResume, 1, 0);
            this.tableLayoutPanel.Controls.Add(this.lblDayResume, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.dgvDayResume, 0, 1);
            this.tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 2;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(1084, 732);
            this.tableLayoutPanel.TabIndex = 0;
            // 
            // dgvHourResume
            // 
            this.dgvHourResume.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvHourResume.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.HourTime,
            this.HourPass,
            this.HourFail,
            this.HourTotal,
            this.HourYield});
            this.dgvHourResume.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvHourResume.Location = new System.Drawing.Point(555, 48);
            this.dgvHourResume.Margin = new System.Windows.Forms.Padding(13);
            this.dgvHourResume.Name = "dgvHourResume";
            this.dgvHourResume.RowHeadersVisible = false;
            this.dgvHourResume.RowTemplate.Height = 24;
            this.dgvHourResume.Size = new System.Drawing.Size(516, 671);
            this.dgvHourResume.TabIndex = 3;
            // 
            // HourTime
            // 
            this.HourTime.HeaderText = "Time";
            this.HourTime.Name = "HourTime";
            this.HourTime.ReadOnly = true;
            // 
            // HourPass
            // 
            this.HourPass.HeaderText = "Pass";
            this.HourPass.Name = "HourPass";
            this.HourPass.ReadOnly = true;
            // 
            // HourFail
            // 
            this.HourFail.HeaderText = "Fail";
            this.HourFail.Name = "HourFail";
            this.HourFail.ReadOnly = true;
            // 
            // HourTotal
            // 
            this.HourTotal.HeaderText = "Total";
            this.HourTotal.Name = "HourTotal";
            this.HourTotal.ReadOnly = true;
            // 
            // HourYield
            // 
            this.HourYield.HeaderText = "Yield";
            this.HourYield.Name = "HourYield";
            this.HourYield.ReadOnly = true;
            // 
            // lblHourResume
            // 
            this.lblHourResume.AutoSize = true;
            this.lblHourResume.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblHourResume.Font = new System.Drawing.Font("微軟正黑體", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lblHourResume.Location = new System.Drawing.Point(545, 0);
            this.lblHourResume.Name = "lblHourResume";
            this.lblHourResume.Size = new System.Drawing.Size(536, 35);
            this.lblHourResume.TabIndex = 1;
            this.lblHourResume.Text = "Hour Resume";
            this.lblHourResume.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDayResume
            // 
            this.lblDayResume.AutoSize = true;
            this.lblDayResume.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDayResume.Font = new System.Drawing.Font("微軟正黑體", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lblDayResume.Location = new System.Drawing.Point(3, 0);
            this.lblDayResume.Name = "lblDayResume";
            this.lblDayResume.Size = new System.Drawing.Size(536, 35);
            this.lblDayResume.TabIndex = 0;
            this.lblDayResume.Text = "Day Resume";
            this.lblDayResume.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dgvDayResume
            // 
            this.dgvDayResume.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDayResume.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DayTime,
            this.DayPass,
            this.DayFail,
            this.DayTotal,
            this.DayYield});
            this.dgvDayResume.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvDayResume.Location = new System.Drawing.Point(13, 48);
            this.dgvDayResume.Margin = new System.Windows.Forms.Padding(13);
            this.dgvDayResume.Name = "dgvDayResume";
            this.dgvDayResume.RowHeadersVisible = false;
            this.dgvDayResume.RowTemplate.Height = 24;
            this.dgvDayResume.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvDayResume.Size = new System.Drawing.Size(516, 671);
            this.dgvDayResume.TabIndex = 2;
            this.dgvDayResume.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvDayResume_CellMouseDoubleClick);
            // 
            // DayTime
            // 
            this.DayTime.HeaderText = "Time";
            this.DayTime.Name = "DayTime";
            this.DayTime.ReadOnly = true;
            // 
            // DayPass
            // 
            this.DayPass.HeaderText = "Pass";
            this.DayPass.Name = "DayPass";
            this.DayPass.ReadOnly = true;
            // 
            // DayFail
            // 
            this.DayFail.HeaderText = "Fail";
            this.DayFail.Name = "DayFail";
            this.DayFail.ReadOnly = true;
            // 
            // DayTotal
            // 
            this.DayTotal.HeaderText = "Total";
            this.DayTotal.Name = "DayTotal";
            this.DayTotal.ReadOnly = true;
            // 
            // DayYield
            // 
            this.DayYield.HeaderText = "Yield";
            this.DayYield.Name = "DayYield";
            this.DayYield.ReadOnly = true;
            // 
            // ProductionResumeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1084, 732);
            this.Controls.Add(this.tableLayoutPanel);
            this.Name = "ProductionResumeForm";
            this.Text = "Production Resume";
            this.Load += new System.EventHandler(this.ProductionResumeForm_Load);
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHourResume)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDayResume)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private System.Windows.Forms.Label lblDayResume;
        private System.Windows.Forms.DataGridView dgvHourResume;
        private System.Windows.Forms.DataGridViewTextBoxColumn HourTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn HourPass;
        private System.Windows.Forms.DataGridViewTextBoxColumn HourFail;
        private System.Windows.Forms.DataGridViewTextBoxColumn HourTotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn HourYield;
        private System.Windows.Forms.Label lblHourResume;
        private System.Windows.Forms.DataGridView dgvDayResume;
        private System.Windows.Forms.DataGridViewTextBoxColumn DayTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn DayPass;
        private System.Windows.Forms.DataGridViewTextBoxColumn DayFail;
        private System.Windows.Forms.DataGridViewTextBoxColumn DayTotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn DayYield;
    }
}
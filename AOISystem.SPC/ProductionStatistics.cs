﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using AOISystem.Utilities.IO;

namespace AOISystem.SPC
{
    public class ProductionStatistics
    {
        private string _folderPath;
        private string _fileName;
        private string _timeFormat = "08:00";
        private int _timeHour = 8;
        private int _timeMinute = 0;
        private int _reserverDay;

        public ProductionStatistics(string folderPath, string fileName, string timeFormat, int reserverDay)
        {
            _folderPath = folderPath;
            _fileName = fileName;
            _reserverDay = reserverDay;
            this.TimeFormat = timeFormat;
            IsRequireChangeTime(this.NowTime);
            CheckOverDueDay();
            this.ProductionInfoCollection = GetInitializeProductionInfoCollection();

            ReadFromIni();
        }

        public string TimeFormat
        {
            get { return _timeFormat; }
            set 
            {
                string[] subs = value.Split(':');
                _timeHour = int.Parse(subs[0]) % 12;
                _timeMinute = int.Parse(subs[1]);
            }
        }

        public DateTime NowTime { get { return DateTime.Now; } }

        public DateTime DayTime { get; private set; }

        public DateTime NightTime { get; private set; }

        public ProductionInfo[] ProductionInfoCollection { get; private set; }

        public TimeMode GetTimeMode(DateTime dateTime)
        {
            DateTime dayTime, nightTime;
            return GetTimeMode(dateTime, out dayTime, out nightTime);
        }

        public TimeMode GetTimeMode(DateTime dateTime, out  DateTime dayTime, out DateTime nightTime)
        {
            TimeMode productionTimeMode = TimeMode.DayTime;
            DateTime zeroTime = dateTime.Date;
            dayTime = GetDayTime(dateTime);
            nightTime = GetNightTime(dateTime);
            if (zeroTime <= dateTime && dateTime < dayTime)
            {
                // 00:00 ~ 07:30
                dayTime = dayTime.AddDays(-1);
                nightTime = nightTime.AddDays(-1);
                productionTimeMode = TimeMode.NightTime;
            }
            else if (dayTime <= dateTime && dateTime < nightTime)
            {
                // 07:30 ~ 19:30
                productionTimeMode = TimeMode.DayTime;
            }
            else
            {
                // 19:30 ~ 24:00
                productionTimeMode = TimeMode.NightTime;
            }
            return productionTimeMode;
        }

        public void ReadFromIni()
        {
            this.ProductionInfoCollection = ReadProductionInfoCollectionFromIni(this.DayTime);
        }

        private ProductionInfo[] ReadProductionInfoCollectionFromIni(DateTime dateTime)
        {
            IniFile iniFile = new IniFile(_folderPath, _fileName);
            ProductionInfo[] productionInfos = GetInitializeProductionInfoCollection();
            for (int i = 0; i < productionInfos.Length; i++)
            {
                string value = iniFile.GetString(
                    dateTime.ToString("yyyyMMdd"),
                    i.ToString("00"),
                    "0,0");
                string[] values = value.Split(',');
                productionInfos[i].Pass = int.Parse(values[0]);
                productionInfos[i].Fail = int.Parse(values[1]);
            }
            return productionInfos;
        }

        public void WriteToIni()
        {
            IniFile iniFile = new IniFile(_folderPath, _fileName);
            for (int i = 0; i < this.ProductionInfoCollection.Length; i++)
            {
                ProductionInfo productionInfo = this.ProductionInfoCollection[i];
                iniFile.WriteValue(
                    this.DayTime.ToString("yyyyMMdd"),
                    i.ToString("00"),
                    string.Format("{0},{1}", productionInfo.Pass, productionInfo.Fail));
            }
        }

        public ProductionInfo GetTotalProductionInfo(DateTime dateTime)
        {
            if (IsRequireChangeTime(dateTime))
            {
                CheckOverDueDay();
                this.ProductionInfoCollection = GetInitializeProductionInfoCollection();
            }
            TimeMode timeMode = GetTimeMode(dateTime);
            return this.ProductionInfoCollection[(int)timeMode];
        }

        public ProductionInfo GetProductionInfo(DateTime dateTime)
        {
            if (IsRequireChangeTime(dateTime))
            {
                CheckOverDueDay();
                this.ProductionInfoCollection = GetInitializeProductionInfoCollection();
            }
            return this.ProductionInfoCollection[(dateTime - this.DayTime).Hours + 2];
        }

        public void PassIncrease(DateTime dateTime)
        {
            ProductionInfo productionInfo = GetProductionInfo(dateTime);
            productionInfo.Pass++;
            ProductionInfo productionInfoAll = GetTotalProductionInfo(dateTime);
            productionInfoAll.Pass++;
            WriteToIni();
        }

        public void FailIncrease(DateTime dateTime)
        {
            ProductionInfo productionInfo = GetProductionInfo(dateTime);
            productionInfo.Fail++;
            ProductionInfo productionInfoAll = GetTotalProductionInfo(dateTime);
            productionInfoAll.Fail++;
            WriteToIni();
        }

        public List<ProductionInfo> GetAllDayTotalProductionInfo()
        {
            IniFile iniFile = new IniFile(_folderPath, _fileName);
            string[] sections = iniFile.GetSectionNames();
            List<ProductionInfo> productionInfos = new List<ProductionInfo>();
            for (int i = 0; i < sections.Length; i++)
            {
                DateTime dayDate = DateTime.ParseExact(sections[i], "yyyyMMdd", CultureInfo.CurrentUICulture, DateTimeStyles.AllowWhiteSpaces);

                ProductionInfo productionInfoDay = new ProductionInfo();
                productionInfoDay.DateTime = dayDate;
                string value = iniFile.GetString(
                    dayDate.ToString("yyyyMMdd"),
                    "00",
                    "0,0");
                string[] values = value.Split(',');
                productionInfoDay.Pass = int.Parse(values[0]);
                productionInfoDay.Fail = int.Parse(values[1]);
                productionInfos.Add(productionInfoDay);

                ProductionInfo productionInfoNight = new ProductionInfo();
                productionInfoNight.DateTime = dayDate;
                value = iniFile.GetString(
                    dayDate.ToString("yyyyMMdd"),
                    "01",
                    "0,0");
                values = value.Split(',');
                productionInfoNight.Pass = int.Parse(values[0]);
                productionInfoNight.Fail = int.Parse(values[1]);
                productionInfos.Add(productionInfoNight);
            }
            return productionInfos;
        }

        public List<ProductionInfo> GetOneDayProductionInfo(DateTime dateTime)
        {
            ProductionInfo[] productionInfoCollection = ReadProductionInfoCollectionFromIni(dateTime);
            return productionInfoCollection.ToList();
        }

        private ProductionInfo[] GetInitializeProductionInfoCollection()
        {
            ProductionInfo[] productionInfos = new ProductionInfo[26];
            for (int i = 0; i < productionInfos.Length; i++)
            {
                productionInfos[i] = new ProductionInfo();
            }
            return productionInfos;
        }

        private bool IsRequireChangeTime(DateTime dateTime)
        {
            DateTime dayTime, nightTime;
            GetTimeMode(dateTime, out dayTime, out nightTime);
            TimeSpan TimeSpan = dayTime - this.DayTime;
            if ((dayTime - this.DayTime).TotalHours >= 24)
            {
                this.DayTime = dayTime;
                this.NightTime = nightTime;
                return true;
            }
            else
            {
                return false;
            }
        }

        private void CheckOverDueDay()
        {
            IniFile iniFile = new IniFile(_folderPath, _fileName);
            List<string> sections = iniFile.GetSectionNames().ToList();
            for (int i = 0; i < _reserverDay; i++)
            {
                DateTime targetDateTime = this.DayTime.AddDays(-i);
                string section = targetDateTime.ToString("yyyyMMdd");
                if (sections.Contains(section))
                {
                    sections.Remove(section);
                }
            }
            foreach (string section in sections)
            {
                iniFile.DeleteSection(section);
            }
        }

        private DateTime GetDayTime(DateTime dateTime)
        {
            return new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, _timeHour, _timeMinute, 0);
        }

        private DateTime GetNightTime(DateTime dateTime)
        {
            return new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, _timeHour + 12, _timeMinute, 0);
        }
    }
}

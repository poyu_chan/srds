﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

namespace AOISystem.SPC
{
    public partial class ProductionResumeForm : Form
    {
        private ProductionStatistics _productionStatistics;

        public ProductionResumeForm(ProductionStatistics productionStatistics)
        {
            InitializeComponent();

            _productionStatistics = productionStatistics;
        }

        private void ProductionResumeForm_Load(object sender, EventArgs e)
        {
            ReadDayResume();
            dgvDayResume_CellMouseDoubleClick(
                this.dgvDayResume, 
                new DataGridViewCellMouseEventArgs(0, 0, 0, 0, new MouseEventArgs(MouseButtons.Left, 2, 0, 0, 0)));
        }

        private void ReadDayResume()
        {
            List<ProductionInfo> productionInfos = _productionStatistics.GetAllDayTotalProductionInfo();
            for (int i = 0; i < productionInfos.Count; i++)
            {
                this.dgvDayResume.Rows.Add(
                    string.Format("{0: yyyy.MM.dd} {1}", productionInfos[i].DateTime, i % 2 == 0 ? "Day" : "Night"),
                    productionInfos[i].Pass,
                    productionInfos[i].Fail,
                    productionInfos[i].Total,
                    productionInfos[i].Yield.ToString("0.## %"));
            }
        }

        private void ReadHourResume(DateTime dateTime)
        {
            List<ProductionInfo> productionInfos = _productionStatistics.GetOneDayProductionInfo(dateTime);
            ProductionInfo productionInfoAll = null;
            DateTime dayTime = _productionStatistics.DayTime;
            this.dgvHourResume.Rows.Clear();
            for (int i = 0; i < 24; i++)
            {
                if (i % 12 == 0)
                {
                    productionInfoAll = new ProductionInfo();
                }
                ProductionInfo productionInfo = productionInfos[i + 2];
                productionInfoAll.Pass += productionInfo.Pass;
                productionInfoAll.Fail += productionInfo.Fail;
                string sss = dayTime.AddHours(i).ToString("HH:mm");
                this.dgvHourResume.Rows.Add(
                    string.Format("{0} ~ {1}", dayTime.AddHours(i).ToString("HH:mm"), dayTime.AddHours(i + 1).ToString("HH:mm")),
                    productionInfo.Pass,
                    productionInfo.Fail,
                    productionInfo.Total,
                    productionInfo.Yield.ToString("0.## %"));
                if ((i + 1) % 12 == 0)
                {
                    int a = (i + 1) / 12;
                    int index = this.dgvHourResume.Rows.Add(
                        (i + 1) / 12 == 1 ? "DayTime" : "NightTime",
                        productionInfoAll.Pass,
                        productionInfoAll.Fail,
                        productionInfoAll.Total,
                        productionInfoAll.Yield.ToString("0.## %"));
                    this.dgvHourResume.Rows[index].DefaultCellStyle.BackColor = Color.Yellow;
                }
            }
        }

        private void dgvDayResume_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            DataGridViewRow rowData = this.dgvDayResume.Rows[e.RowIndex];
            if (rowData.Cells[0].Value != null)
            {
                string dateString = rowData.Cells[0].Value.ToString();
                dateString = dateString.Trim().Substring(0, 10);
                DateTime dateTime = DateTime.ParseExact(dateString, "yyyy.MM.dd", CultureInfo.CurrentUICulture, DateTimeStyles.AllowWhiteSpaces);
                ReadHourResume(dateTime);
            }
        }
    }
}

﻿using System;

namespace AOISystem.SPC
{
    public class SPCHelper
    {
        public SPCHelper()
        {
            DateTime dateTime = new DateTime(2014, 2, 24, 8, 00, 00);
            DateTime dayTime, nightTime;
            TimeMode productionTimeMode = GetProductionTime(dateTime, out dayTime, out nightTime);
            Console.WriteLine("Now is {0} {1 : yyyy.MM.dd HH:mm:ss}", productionTimeMode, dateTime);
        }

        public static TimeMode GetProductionTime(DateTime dateTime, out  DateTime dayTime, out DateTime nightTime)
        {
            TimeMode productionTimeMode = TimeMode.DayTime;
            DateTime zeroTime = dateTime.Date;
            dayTime = GetDayTime(dateTime);
            nightTime = GetNightTime(dateTime);
            if (zeroTime <= dateTime && dateTime < dayTime)
            {
                // 00:00 ~ 07:30
                dayTime = dayTime.AddDays(-1);
                nightTime = nightTime.AddDays(-1);
                productionTimeMode = TimeMode.NightTime;
            }
            else if (dayTime <= dateTime && dateTime < nightTime)
            {
                // 07:30 ~ 19:30
                productionTimeMode = TimeMode.DayTime;
            }
            else
            {
                // 19:30 ~ 24:00
                productionTimeMode = TimeMode.NightTime;
            }
            return productionTimeMode;
        }

        private static DateTime GetDayTime(DateTime dateTime)
        {
            return new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, 8, 0, 0);
        }

        private static DateTime GetNightTime(DateTime dateTime)
        {
            return new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, 20, 00, 0);
        }
    }
}

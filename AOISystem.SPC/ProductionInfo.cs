﻿using System;

namespace AOISystem.SPC
{
    public class ProductionInfo
    {
        public ProductionInfo()
        {

        }

        public DateTime DateTime { get; set; }

        public int Total
        {
            get
            {
                return this.Pass + this.Fail;
            }
        }

        public int Pass { get; set; }

        public int Fail { get; set; }

        public double Yield
        {
            get
            {
                return this.Total == 0 ? 0 : (double)this.Pass / this.Total;
            }
        }
    }
}
